import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { environment } from '@env/environment';
import { AuthenticationService } from '../authentication.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [MessageService],
})
export class RegisterComponent implements OnInit, OnDestroy {
  version: string | null = environment.version;

  error: string | undefined;
  registerForm!: FormGroup;
  isLoading = false;
  submitted = false;

  get f() {
    return this.registerForm.controls;
  }

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private messageService: MessageService
  ) {
    this.Form();
  }

  private Form() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      remember: true,
    });
  }

  ngOnInit() {}

  ngOnDestroy() {}

  register() {
    this.isLoading = true;
    this.submitted = true;

    if (this.registerForm.invalid) {
      if (this.f.email.errors) {
        if (this.f.email.errors.email) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Email format wrong. Ex:youremail@email.com',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Email can not empty',
          });
        }
      }
      if (this.f.password.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Password can not empty',
        });
      }
      return;
    } else {
      this.submitted = false;
    }

    this.authenticationService.register(this.registerForm.value).then((res) => {
      if (res) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: res,
        });
      } else {
        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Success register. Check your email for verification!',
        });
      }
    });
  }
}
