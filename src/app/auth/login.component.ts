import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { environment } from '@env/environment';
import { Logger, untilDestroyed } from '@core';
import { AuthenticationService } from './authentication.service';
import { CredentialsService } from './credentials.service';
import { MessageService } from 'primeng/api';

const log = new Logger('Login');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService],
})
export class LoginComponent implements OnInit, OnDestroy {
  version: string | null = environment.version;

  error: string | undefined;
  loginForm!: FormGroup;
  isLoading = false;
  submitted = false;

  get f() {
    return this.loginForm.controls;
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private messageService: MessageService
  ) {
    this.Form();
  }

  private Form() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      remember: true,
    });
  }

  ngOnInit() {
    this.logout();
  }

  ngOnDestroy() {}

  logout() {
    this.authenticationService.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
  }

  logIn() {
    this.isLoading = true;
    this.submitted = true;

    if (this.loginForm.invalid) {
      if (this.f.email.errors) {
        if (this.f.email.errors.email) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Email format wrong. Ex:youremail@email.com',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Email can not empty',
          });
        }
      }
      if (this.f.password.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Password can not empty',
        });
      }
      return;
    } else {
      this.submitted = false;
    }

    this.authenticationService.login(this.loginForm.value).then((res) => {
      if (!res.uid) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: res.message,
        });
      }

      // setTimeout(() => {
      //   if (!this.authenticationService.isEmailVerified) {
      //     this.messageService.add({
      //       key: 'error',
      //       severity: 'error',
      //       summary: 'Error Message',
      //       detail: 'Email not verified',
      //     });
      //   } else {
      //     this.router.navigate([this.route.snapshot.queryParams.redirect || '/'], { replaceUrl: true });
      //   }
      // }, 500);
    });
  }
}
