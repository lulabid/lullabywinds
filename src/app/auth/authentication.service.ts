import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

import { Credentials, CredentialsService } from './credentials.service';
import { auth } from 'firebase/app';

export interface LoginContext {
  email: string;
  password: string;
  remember?: boolean;
}

export interface RegisterContext {
  email: string;
  password: string;
}

const credentialsKey = 'credentials';

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(
    private credentialsService: CredentialsService,
    private _http: HttpClient,
    public router: Router,
    private route: ActivatedRoute,
    public firestore: AngularFirestore,
    public fireauth: AngularFireAuth,
    public ngZone: NgZone
  ) {
    // this.fireauth.authState.subscribe(user => {
    //   if (user) {
    //     localStorage.setItem('user', JSON.stringify(user.getIdTokenResult));
    //     JSON.parse(localStorage.getItem('user'));
    //   } else {
    //     localStorage.setItem('user', null);
    //     JSON.parse(localStorage.getItem('user'));
    //   }
    // })
  }

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */

  // Login in with email/password
  async login(context: LoginContext): Promise<any> {
    console.log('context ' + context.email, context.password);
    try {
      await this.fireauth.signInWithEmailAndPassword(context.email, context.password);
      // console.log('res login auth ' + JSON.stringify(res));

      const user = this.fireauth.currentUser;
      if (await user) {
        console.log('contexuserusert ' + JSON.stringify((await user).emailVerified));

        (await user).getIdToken().then((idToken) => {
          // console.log('idToken ' + JSON.stringify(idToken));

          this.fireauth.authState.subscribe((user) => {
            if (user) {
              const data: any = {
                email: context.email,
                token: idToken,
                emailVerified: user.emailVerified,
              };
              this.credentialsService.setCredentials(data, context.remember);
              this.router.navigate([this.route.snapshot.queryParams.redirect || '/'], { replaceUrl: true });
            }
          });
        });
        return await user;
      } else {
        console.log('contexuserusert ' + JSON.stringify((await user).emailVerified));

        (await user).getIdToken().then((idToken) => {
          // console.log('idToken ' + JSON.stringify(idToken));

          this.fireauth.authState.subscribe((user) => {
            if (user) {
              this.credentialsService.setCredentials();
              //this.router.navigate([this.route.snapshot.queryParams.redirect || '/'], { replaceUrl: true });
            }
          });
        });
        return await user;
      }
    } catch (error) {
      if (error.code === 'auth/wrong-password') {
        console.log('Incorrect Password');
      }
      if (error.code === 'auth/user-not-found') {
        console.log('User not found');
      }
      if (error.code === 'auth/email-already-in-use') {
        console.log('User already use');
      }
      if (error.code === 'auth/argument-error') {
        console.log('Argument error');
      }
      if (error.code === 'auth/invalid-email') {
        console.log('Invalid email');
      } else {
        console.log('Something went wrong try later');
      }
      console.log(error);
      return await error;
    }
  }

  // Login in with gmail
  async loginGmail() {
    try {
      const res = await this.fireauth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
      console.log('res ' + res);
      this.router.navigateByUrl('/');
    } catch (error) {
      console.log(error);
      if (error.code === 'auth/wrong-password') {
        console.log('Incorrect Password');
      }
      if (error.code === 'auth/user-not-found') {
        console.log('User dont found');
      }
      if (error.code === 'auth/email-already-in-use') {
        console.log('User already use');
      }
      if (error.code === 'auth/argument-error') {
        console.log('Argument error');
      }
      if (error.code === 'auth/invalid-email') {
        console.log('Invalid email');
      } else {
        console.log('Something went wrong try later');
      }
      console.log(error);
    }
  }

  // Register user with email/password
  async register(context: RegisterContext): Promise<any> {
    console.log('context ' + context.email, context.password);
    try {
      const res = await this.fireauth.createUserWithEmailAndPassword(context.email, context.password);
      // console.log('res register auth ' + JSON.stringify(res));

      if (res) {
        this.SendVerificationMail();
      }
    } catch (error) {
      if (error.code === 'auth/wrong-password') {
        console.log('Incorrect Password');
      }
      if (error.code === 'auth/user-not-found') {
        console.log('User not found');
      }
      if (error.code === 'auth/email-already-in-use') {
        console.log('User already use');
      }
      if (error.code === 'auth/argument-error') {
        console.log('Argument error');
      }
      if (error.code === 'auth/invalid-email') {
        console.log('Invalid email');
      } else {
        console.log('Something went wrong try later');
      }
      console.log(error);
      return await error;
    }
  }

  // Register user with email/password
  RegisterUser(context: LoginContext) {
    return this.fireauth.createUserWithEmailAndPassword(context.email, context.password);
  }

  // Email verification when new user register
  async SendVerificationMail() {
    return (await this.fireauth.currentUser).sendEmailVerification().then(() => {
      console.log('Verification has been send to your email. Please verifiy');
    });
  }

  // Recover password
  PasswordRecover(passwordResetEmail: any) {
    return this.fireauth
      .sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Password reset email has been sent, please check your inbox.');
      })
      .catch((error) => {
        window.alert(error);
      });
  }

  // Returns true when user is looged in
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem(credentialsKey));
    return user !== null && user.emailVerified !== false ? true : false;
  }

  // Returns true when user's email is verified
  get isEmailVerified(): boolean {
    const getCredentials =
      JSON.parse(sessionStorage.getItem(credentialsKey)) || JSON.parse(localStorage.getItem(credentialsKey));
    if (getCredentials) {
      return getCredentials.emailVerified !== false ? true : false;
    }
  }

  // Sign in with Gmail
  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  }

  // Auth providers
  AuthLogin(provider: any) {
    return this.fireauth
      .signInWithPopup(provider)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['/']);
        });
        this.SetUserData(result.user);
      })
      .catch((error) => {
        window.alert(error);
      });
  }

  // Store user in localStorage
  SetUserData(user: any) {
    const userRef: AngularFirestoreDocument<any> = this.firestore.doc(`users/${user.uid}`);
    const userData = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
    };
    return userRef.set(userData, {
      merge: true,
    });
  }

  // Sign-out
  SignOut() {
    return this.fireauth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['login']);
    });
  }

  handleError(err: any) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      // if error is client-side error
      errorMessage = `Error: Can not access server. Check your connection!`;
    } else {
      // if error is server-side error
      errorMessage = `Error Code: ${err}\nMessage: ${err}`;
    }
    return throwError(errorMessage);
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.credentialsService.setCredentials();
    return of(true);
  }

  /**
   * Provides a base for get token that used header CRUD.
   */
  private initAuthHeaders(): HttpHeaders {
    const token = this.credentialsService.getCredentials();
    // console.log('token ' + JSON.stringify(token));

    if (token === null) {
      this.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
    }

    const getToken = token.token;

    const headers: HttpHeaders = new HttpHeaders({
      'x-access-token': getToken,
      // 'Content-Type': 'multipart/form-data'
    });
    // set('Access-Control-Allow-Origin', '* ').
    // set('Content-Type', 'application/json').
    // set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')

    return headers;
  }

  /**
   * Provides a base for CRUD.
   */
  authGet(url: string) {
    const headers = this.initAuthHeaders();
    return this._http.get(url, { headers }).pipe(map((response) => response));
  }

  authPost(url: string, body: any): any {
    const headers = this.initAuthHeaders();
    return this._http.post(url, body, { headers }).pipe(map((response) => response));
  }

  authPostProgress(url: string, body: any): any {
    const headers = this.initAuthHeaders();
    return this._http
      .post(url, body, { headers, reportProgress: true, observe: 'events' })
      .pipe(map((response) => response));
  }

  authPut(url: string, body: any): any {
    const headers = this.initAuthHeaders();
    return this._http.put(url, body, { headers }).pipe(map((response) => response));
  }

  authPutProgress(url: string, body: any): any {
    const headers = this.initAuthHeaders();
    return this._http
      .put(url, body, { headers, reportProgress: true, observe: 'events' })
      .pipe(map((response) => response));
  }

  authPatch(url: string, body: any): any {
    const headers = this.initAuthHeaders();
    return this._http.patch(url, body, { headers }).pipe(map((response) => response));
  }

  authDelete(url: string): any {
    const headers = this.initAuthHeaders();
    return this._http.delete(url, { headers }).pipe(map((response) => response));
  }
}
