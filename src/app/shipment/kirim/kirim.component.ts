import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { KirimService } from './kirim.service';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { StartService } from '../../transaction/start/start.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-kirim',
  templateUrl: './kirim.component.html',
  styleUrls: ['./kirim.component.scss'],
  providers: [MessageService, DatePipe],
})
export class KirimComponent implements OnInit {
  isLoading = false;
  error: string | undefined;

  modalAdd: boolean = false;
  modalDetail: boolean = false;
  modalKirim: boolean = false;
  modalListAddress: boolean = false;

  transactionForm!: FormGroup;
  shipmentForm!: FormGroup;
  idCustom: any | undefined;
  alterBy: any | undefined;

  idShipment: any | undefined;
  idGoods: any | undefined;
  idBelanja: any | undefined;
  idCustomer: any | undefined;
  idx: any | undefined;
  readData: any | undefined;
  dataLength: any | undefined;

  readDataTrans: any | undefined;
  readDataShip: any | undefined;

  readAddressCustomer: any | undefined;
  readAddress: any | undefined;
  countryID: any | undefined;
  countryCustomer: any | undefined;
  provinceCustomer: any | undefined;
  cityCustomer: any | undefined;
  postCustomer: any | undefined;
  otherCustomer: any | undefined;
  telpCustomer: any | undefined;
  geoCustomer: any | undefined;

  searchValue: string | undefined;
  searchValueAddress: string | undefined;
  currentUser: any | undefined;
  currentUserCustomer: any | undefined;

  statusID: any | undefined;
  readAdmin: any | undefined;

  selectedShipment: any | undefined;

  get tf() {
    return this.shipmentForm.controls;
  }

  _selectedColumns: any[];
  cols: any[];
  _selectedColumnsAddress: any[];
  colsAddress: any[];

  
  submitted = false;

  country = [
    {
      id: 81,
      name: 'Jepang',
    },
    {
      id: 62,
      name: 'Indonesia',
    },
  ];

  province = [
    {
      id: 1,
      name: 'Aceh',
    },
    {
      id: 2,
      name: 'Sumatera Utara',
    },
    {
      id: 3,
      name: 'Sumatera Barat',
    },
    {
      id: 4,
      name: 'Riau',
    },
    {
      id: 5,
      name: 'Jambi',
    },
    {
      id: 6,
      name: 'Sumatera Selatan',
    },
    {
      id: 7,
      name: 'Bengkulu',
    },
    {
      id: 8,
      name: 'Lampung',
    },
    {
      id: 9,
      name: 'Kepulauan Bangka Belitung',
    },
    {
      id: 10,
      name: 'Kepulauan Riau',
    },
    {
      id: 11,
      name: 'Dki Jakarta',
    },
    {
      id: 12,
      name: 'Jawa Barat',
    },
    {
      id: 13,
      name: 'Jawa Tengah',
    },
    {
      id: 14,
      name: 'Di Yogyakarta',
    },
    {
      id: 15,
      name: 'Jawa Timur',
    },
    {
      id: 16,
      name: 'Banten',
    },
    {
      id: 17,
      name: 'Bali',
    },
    {
      id: 18,
      name: 'Nusa Tenggara Barat',
    },
    {
      id: 19,
      name: 'Nusa Tenggara Timur',
    },
    {
      id: 20,
      name: 'Kalimantan Barat',
    },
    {
      id: 21,
      name: 'Kalimantan Tengah',
    },
    {
      id: 22,
      name: 'Kalimantan Selatan',
    },
    {
      id: 23,
      name: 'Kalimantan Timur',
    },
    {
      id: 24,
      name: 'Kalimantan Utara',
    },
    {
      id: 25,
      name: 'Sulawesi Utara',
    },
    {
      id: 26,
      name: 'Sulawesi Tengah',
    },
    {
      id: 27,
      name: 'Sulawesi Selatan',
    },
    {
      id: 28,
      name: 'Sulawesi Tenggara',
    },
    {
      id: 29,
      name: 'Gorontalo',
    },
    {
      id: 30,
      name: 'Sulawesi Barat',
    },
    {
      id: 31,
      name: 'Maluku',
    },
    {
      id: 32,
      name: 'Maluku Utara',
    },
    {
      id: 33,
      name: 'Papua Barat',
    },
    {
      id: 34,
      name: 'Papua',
    },
  ];

  kurir = [
    {
      id: 0,
      name: 'Default',
    },
    {
      id: 1,
      name: 'Ambil Sendiri',
    },
    {
      id: 2,
      name: 'Anter Aja',
    },
    {
      id: 3,
      name: 'GO-SEND',
    },
    {
      id: 4,
      name: 'GOJEK',
    },
    {
      id: 5,
      name: 'GRAB',
    },
    {
      id: 6,
      name: 'JNE',
    },
    {
      id: 7,
      name: 'JNE JTR',
    },
    {
      id: 8,
      name: 'JNE OK',
    },
    {
      id: 9,
      name: 'JNE YES',
    },
    {
      id: 10,
      name: 'J&T',
    },
    {
      id: 11,
      name: 'Kurir',
    },
    {
      id: 12,
      name: 'MEX',
    },
    {
      id: 13,
      name: 'Pos Express',
    },
    {
      id: 14,
      name: 'Pos Kilat Khusus',
    },
    {
      id: 15,
      name: 'REX',
    },
    {
      id: 16,
      name: 'Si Cepat',
    },
    {
      id: 17,
      name: 'TIKI',
    },
    {
      id: 18,
      name: 'TIKI ECO',
    },
    {
      id: 19,
      name: 'TIKI ONS',
    },
    {
      id: 20,
      name: 'TIKI TCR',
    },
  ];

  constructor(
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private fireauth: AngularFireAuth,
    private firestore: AngularFirestore,
    private service: KirimService,
    private messageService: MessageService,
    private startService: StartService,
    private router: Router,
    private datePipe: DatePipe
  ) {
    this.Form();
  }

  ngOnInit() {
    this.getShipment();
    this.getTransaction();
    this.getAdmin();

     // set item column on p-table
     this.cols = [
       // { field: 'hi204paypric', header: 'Payment Price' },
      // { field: 'hi011postnum', header: 'Post Code' },
      { field: 'hi200kemasid', header: 'KmasID' },
      { field: 'hi000custoid', header: 'CustID' },
      { field: 'hi006fulname', header: 'Pmbeli' },
      { field: 'hi203onkiprc', header: 'Onkr' },
      { field: 'hi009namalen', header: 'Ptrima' },
      { field: 'hi012prefect', header: 'Prov' },
      { field: 'hi013citynam', header: 'Kota' },
      { field: 'hi014lainnya', header: 'Jln' },
      { field: 'hi015telpnum', header: 'Telp' },
      { field: 'hi205viakuri', header: 'Via' },
      { field: 'hi206resinum', header: 'ResiNo' },
      { field: 'hi205catatan', header: 'CatS' },
    ];
    this._selectedColumns = this.cols;

    this.colsAddress = [
      { field: 'ak009namalen', header: 'Ptrima' },
      { field: 'ak011postnum', header: 'Kdpos' },
      { field: 'ak012prefect', header: 'Prov' },
      { field: 'ak013citynam', header: 'Kota' },
      { field: 'ak014lainnya', header: 'Jln' },
      { field: 'ak015telpnum', header: 'Telp' },
    ];
    this._selectedColumnsAddress = this.colsAddress;
  }

  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }
  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col: any) => val.includes(col));
  }

  @Input() get selectedColumnsAddress(): any[] {
    return this._selectedColumnsAddress;
  }
  set selectedColumnsAddress(val: any[]) {
    //restore original order
    this._selectedColumnsAddress = this.colsAddress.filter((col: any) => val.includes(col));
  }

  printInvoice() {
    const url = this.router.serializeUrl(this.router.createUrlTree([`print-invoice`, this.selectedShipment.hi200kemasid]));

    window.open(url, '_blank');
  }

  listTransaction(event: any) {
    this.idShipment = event.hi200kemasid;
    this.router.navigate(['transaction-shipment', this.idShipment]);
  }

  addModal() {
    this.modalAdd = true;
    this.renderer.addClass(document.body, 'modal-open');

    this.shipmentForm.reset();
  }

  noKemas(event: any) {
    console.log('event ' + JSON.stringify(event));
  }

  removeModalTrans() {
    this.modalAdd = false;
    this.modalDetail = false;
    this.renderer.removeClass(document.body, 'modal-open');
  }

  removeModalAddress() {
    this.modalListAddress = false;
    this.lulaId(this.selectedShipment);
    this.currentUser = "";
    this.currentUserCustomer = "";
    this.searchValueAddress = "";

    this.renderer.removeClass(document.body, 'modal-open');
  }

  kurirChange(event: any) {
    console.log('kurir select ' + parseInt(event.target.value));
    this.shipmentForm.controls['hi205viakuri'].setValue(parseInt(event.target.value));
  }

  private Form() {
    this.transactionForm = this.formBuilder.group({
      idBelanja: [''],
      idGoods: [''],
      dn312alterby: [''],
      dn309statudt: [''],
      dn310sstatus: [''],
    });

    this.shipmentForm = this.formBuilder.group({
      idx: [''],
      idCustomer: [''],
      hi200kemasid: ['', [Validators.required]],
      hi205viakuri: [0],
      hi206resinum: [''],
      hi203onkiprc: [0],
      hi204paypric: [-999999999999],
      hi205catatan: [''],
      hi212alterby: [''],
      hi209statudt: [''],
      hi210statuss: [''],
      hi000custoid: [''],
      hi006fulname: [''],
      hi009namalen: [''],
      hi010country: [''],
      hi011postnum: [''],
      hi012prefect: [''],
      hi013citynam: [''],
      hi014lainnya: [''],
      hi015telpnum: [''],
      hi016geopoin: [''],
    });
  }

  closed(event: any) {
    console.log('event ' + event.target.className);
    if(event.target.className === "modal fade show") {
      this.modalDetail = false;
      this.renderer.removeClass(document.body, 'modal-open');
    } 
  }

  closedAddress(event: any) {
    if(event.target.className === "modal fade show") {
      this.removeModalAddress();
    } 
  }

  async lulaId(event: any) {
    console.log('event ' + JSON.stringify(event));

    // get collection address by id customer
    // this.getAddress();

    this.selectedShipment = event;

    this.modalDetail = true;
    this.renderer.addClass(document.body, 'modal-open');

    const getCountry = event.hi010country;
    const filterCountry = this.country.filter((data: any) => data.id == getCountry);
    this.countryCustomer = filterCountry[0];
    // console.log('this.countryCustomer ' + JSON.stringify(this.countryCustomer));

    const getProvince = event.hi012prefect;
    const filterProvince = this.province.filter((data: any) => data.id == getProvince);
    this.provinceCustomer = filterProvince[0];
    // console.log('this.provinceCustomer ' + JSON.stringify(this.provinceCustomer));

    const statusDate = moment(new Date(event.hi209statudt?.seconds * 1000)).format('MM/DD/YYYY hh:mm:ss A');

    this.idCustomer = event.idCustomer;
    this.cityCustomer = event.hi013citynam;
    this.postCustomer = event.hi011postnum;
    this.otherCustomer = event.hi014lainnya;
    this.telpCustomer = event.hi015telpnum;
    this.geoCustomer = event.hi016geopoin;

    this.shipmentForm.patchValue({
      idx: event.id,
      idCustomer: event.idCustomer,
      hi200kemasid: event.hi200kemasid,
      hi000custoid: event.hi000custoid,
      hi006fulname: event.hi006fulname,
      hi009namalen: event.hi009namalen,
      hi010country: this.countryCustomer.name,
      hi011postnum: event.hi011postnum,
      hi012prefect: this.provinceCustomer.name,
      hi013citynam: event.hi013citynam,
      hi014lainnya: event.hi014lainnya,
      hi015telpnum: event.hi015telpnum,
      hi016geopoin: event.hi016geopoin.latitude + ',' + event.hi016geopoin.longitude,
      hi205viakuri: event.hi205viakuri,
      hi206resinum: event.hi206resinum,
      hi203onkiprc: event.hi203onkiprc,
      hi204paypric: event.hi204paypric,
      hi205catatan: event.hi205catatan,
      hi212alterby: event.hi212alterby,
      hi209statudt: statusDate,
      hi210statuss: event.hi210statuss,
    });
  }

  getCustomer() {
    this.firestore.collection('cl001members').doc(this.idCustomer).ref.get().then((doc => {
      if (doc.exists) {
        this.readAddressCustomer = doc.data();
        console.log('this.readAddressCustomer ' + JSON.stringify(this.readAddressCustomer.dk014lainnya));
      } else {
        console.log('No Data Customer Ref');
        this.readAddressCustomer= [];
      }
    }));
  }

  async getAddress() {
    this.isLoading = true;

    (await this.service.readAddress()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          // Get all data address
          // console.log('this.idCustomer ' + JSON.stringify(this.idCustomer));

          this.readAddress = this.firestore
            .collection(`cl001members/${this.idCustomer}/cl007address`, (ref) => ref.orderBy('ak009namalen'))
            .snapshotChanges()
            .pipe(
              map((actions) =>
                actions.map((a) => {
                  const data = a.payload.doc.data() as any;
                  const id = a.payload.doc.id;
                  const idCustomer = a.payload.doc.ref.parent.parent.id;
                  const meta = a.payload.doc.metadata;

                  // console.log('idCustomer ' + JSON.stringify(idCustomer));
                  return { id, meta, idCustomer, ...data };
                })
              )
            );
        } else {
          this.readAddress = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach address from the server!',
        });
      }
    });
  }

  changeAddress() {
    // console.log('change customer ' + JSON.stringify(event.idCustomer));
    // console.log('change id ' + JSON.stringify(event.id));
    const getDataShipment = this.selectedShipment;

    console.log('getDataShipment ' + JSON.stringify(getDataShipment));

    this.idCustomer = getDataShipment.idCustomer;
    this.idx = getDataShipment.id;

    this.modalListAddress = true;
    this.getAddress();
    this.getCustomer();
  }

  async addressDefault(event: any, item: any) {
    console.log('item address lainnya ' + JSON.stringify(item));
    // console.log('item customer ' + JSON.stringify(this.idCustomer));
    // console.log('item idx ' + JSON.stringify(this.idx));
    this.currentUser = item.ak009namalen;
    this.currentUserCustomer = item.dk006fulname;

    console.log('item idx default' + JSON.stringify(this.idx));
    console.log('item idCustomer default' + JSON.stringify(this.idCustomer));

    this.shipmentForm.patchValue({
      idx: this.idx,
      idCustomer: this.idCustomer,
      hi009namalen: item.dk006fulname,
      hi010country: item.dk010country,
      hi011postnum: item.dk011postnum,
      hi012prefect: item.dk012prefect,
      hi013citynam: item.dk013citynam,
      hi014lainnya: item.dk014lainnya,
      hi015telpnum: item.dk015telpnum,
      hi016geopoin: item.dk016geopoin,
    });
  }

  async addressId(event: any, item: any) {
    console.log('item idx ' + JSON.stringify(item));
    // console.log('item customer ' + JSON.stringify(this.idCustomer));
    // console.log('item idx ' + JSON.stringify(this.idx));
    this.currentUser = item.ak009namalen;
    this.currentUserCustomer = item.dk006fulname;

    this.shipmentForm.patchValue({
      idx: this.idx,
      idCustomer: this.idCustomer,
      hi009namalen: item.ak009namalen,
      hi010country: item.ak010country,
      hi011postnum: item.ak011postnum,
      hi012prefect: item.ak012prefect,
      hi013citynam: item.ak013citynam,
      hi014lainnya: item.ak014lainnya,
      hi015telpnum: item.ak015telpnum,
      hi016geopoin: item.ak016geopoin,
    });
  }

  async updateAddress() {
    this.isLoading = true;
    this.submitted = true;

    console.log('idcustomer ' + JSON.stringify(this.idCustomer));
    console.log('idx ' + JSON.stringify(this.idx));

    this.service.updateAddress(this.shipmentForm.value).then((res: any) => {
      this.isLoading = false;

      if (res) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not update address customer!',
        });
      } else {
        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully update address customer',
        });

        this.removeModalAddress();
      }
    });
  }

  selectAddress(event: any) {
    // console.log('get id cstmer ' + JSON.stringify(event.target.value));
    const selectID = event.target.value;

    this.readAddress.forEach((ev: any) => {
      // get country from address customer
      const customerSelect = ev.filter((data: any) => data.ak009namalen === selectID);

      const getCountry = customerSelect[0].ak008address[0].ak010country;
      const filterCountry = this.country.filter((data: any) => data.id == getCountry);
      this.countryCustomer = filterCountry[0];
      // console.log('this.countryCustomer ' + JSON.stringify(this.countryCustomer));

      // get province from address customer
      const getProvince = customerSelect[0].ak008address[0].ak012prefect;
      //console.log('getProvince '  + JSON.stringify(getProvince));
      const filterProvince = this.province.filter((data: any) => data.id == getProvince);
      //console.log('filterProvince '  + JSON.stringify(filterProvince[0]));
      this.provinceCustomer = filterProvince[0];

      // get city from address customer
      const citySelect = customerSelect[0].ak008address[0].ak013citynam;
      this.cityCustomer = citySelect;
      // console.log('citySelect ' + JSON.stringify(citySelect));

      // get post code from address customer
      const postSelect = customerSelect[0].ak008address[0].ak011postnum;
      this.postCustomer = postSelect;

      // get others from address customer
      const otherSelect = customerSelect[0].ak008address[0].ak014lainnya;
      this.otherCustomer = otherSelect;

      // get telp from address customer
      const telpSelect = customerSelect[0].ak015telpnum;
      this.telpCustomer = telpSelect;

      // get geo from address customer
      const geoSelect = customerSelect[0].ak016geopoin;
      const latitude = parseFloat(geoSelect.latitude);
      const longitude = parseFloat(geoSelect.longitude);
      const locationData = new firebase.firestore.GeoPoint(latitude, longitude);
      this.geoCustomer = locationData;

      // set value into field transaction
      this.shipmentForm.patchValue({
        hi009namalen: selectID,
        hi010country: this.countryCustomer.id,
        hi012prefect: this.provinceCustomer.id,
        hi013citynam: this.cityCustomer,
        hi011postnum: this.postCustomer,
        hi014lainnya: this.otherCustomer,
        hi015telpnum: this.telpCustomer,
        hi016geopoin: this.geoCustomer,
      });
    });
  }

  selectCountry(event: any) {
    this.shipmentForm.patchValue({
      hi010country: parseInt(event.target.value),
    });
  }

  selectProvince(event: any) {
    this.shipmentForm.patchValue({
      hi012prefect: parseInt(event.target.value),
    });
  }

  selectCity(event: any) {
    this.shipmentForm.patchValue({
      hi013citynam: event.target.value,
    });
  }

  selectPost(event: any) {
    this.shipmentForm.patchValue({
      hi011postnum: event.target.value,
    });
  }

  selectOther(event: any) {
    this.shipmentForm.patchValue({
      hi014lainnya: event.target.value,
    });
  }

  selectTelp(event: any) {
    this.shipmentForm.patchValue({
      hi015telpnum: event.target.value,
    });
  }

  selectGeo(event: any) {
    this.shipmentForm.patchValue({
      hi016geopoin: event.target.value,
    });
  }

  async moveStatus(event: any) {
    const user = await this.fireauth.currentUser;

    if (user) {
      // get id Customer and input into idGoods
      this.readDataShip.forEach(async (data: any) => {
        setTimeout(() => {
          if (data.hi200kemasid === event.hi200kemasid) {
            // create new field shipment lunas
            this.shipmentForm.patchValue({
              idx: data?.id,
              idCustomer: data?.idCustomer,
              hi212alterby: this.readAdmin,
              hi209statudt: firebase.firestore.Timestamp.fromDate(new Date()),
              hi210statuss: 8,
            });

            // set create new data status collection shipment
            this.service.updateStatus(this.shipmentForm.value).then((res: any) => {
              this.isLoading = false;

              if (res) {
                this.messageService.add({
                  key: 'error',
                  severity: 'error',
                  summary: 'Error Message',
                  detail: 'Can not update status shipment to bungkus!',
                });
              } else {
                this.messageService.add({
                  key: 'success',
                  severity: 'success',
                  summary: 'Success Message',
                  detail: 'Succesfully update status shipment to bungkus',
                });

                this.removeModalTrans();
              }
            });
          }
        }, 0);
      });

      this.readDataTrans.forEach(async (dataTrans: any) => {
        setTimeout(() => {
          if (dataTrans.dn200kemasid === event.hi200kemasid) {
            // set update status transaction tagih
            this.transactionForm.patchValue({
              idBelanja: dataTrans?.id,
              idGoods: dataTrans?.idGoods,
              dn312alterby: this.readAdmin,
              dn309statudt: firebase.firestore.Timestamp.fromDate(new Date()),
              dn310sstatus: 8,
            });

            this.service.updateStatusTrans(this.transactionForm.value).then((res: any) => {
              this.isLoading = false;

              if (res) {
                this.messageService.add({
                  key: 'error',
                  severity: 'error',
                  summary: 'Error Message',
                  detail: 'Can not update status transaction to bungkus!',
                });
              } else {
                this.messageService.add({
                  key: 'success',
                  severity: 'success',
                  summary: 'Success Message',
                  detail: 'Succesfully move transaction to status lunas',
                });

                this.removeModalTrans();
              }
            });
          }
        }, 1000);
      });
    } else {
      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: 'Can not move to status selesai. Dont have authorization!',
      });
    }
  }

  async getAdmin() {
    (await this.startService.getAdmin()).subscribe((res: any) => {
      if (res) {
        const status = res;
        if (status) {
          this.readAdmin = res[0].di004admname;
        } else {
          this.readAdmin = [];
        }
      }
    });
  }

  async getTransaction() {
    this.isLoading = true;

    (await this.service.getTrans()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          this.readDataTrans = res.filter((data: any) => data.dn310sstatus == 7);
        } else {
          this.readDataTrans = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach the server!',
        });
      }
    });
  }

  async getShipment() {
    this.isLoading = true;

    (await this.service.getShip()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          const filterStatus = res.filter((data: any) => data.hi210statuss == 7);
          this.readDataShip = filterStatus;

          let matches = new Set();
          const filterKemas = filterStatus.filter((elem: any) => {
            const { hi200kemasid } = elem;
            if (matches.has(hi200kemasid)) {
              return false;
            } else {
              matches.add(hi200kemasid);
              return true;
            }
          })

          this.readData = filterKemas;
          console.log('output ' + JSON.stringify(filterKemas));
        } else {
          this.readData = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach the server!',
        });
      }
    });
  }

  async updateShipment() {
    this.isLoading = true;
    this.submitted = true;

    const user = await this.fireauth.currentUser;

    // if (this.shipmentForm.invalid) {
    //   this.isLoading = false;

    //   if (this.tf.dn302qantity.errors) {
    //     this.messageService.add({
    //       key: 'error',
    //       severity: 'error',
    //       summary: 'Error Message',
    //       detail: 'Quantity can not empty!',
    //     });
    //   }

    //   if (this.tf.dn303dealprc.errors) {
    //     this.messageService.add({
    //       key: 'error',
    //       severity: 'error',
    //       summary: 'Error Message',
    //       detail: 'Deal price can not empty!',
    //     });
    //   }

    //   if (this.tf.dn304paypric.errors) {
    //     this.messageService.add({
    //       key: 'error',
    //       severity: 'error',
    //       summary: 'Error Message',
    //       detail: 'Payment price can not empty!',
    //     });
    //   }

    //   return;
    // } else {
    //   this.submitted = false;
    // }

    this.readDataShip.forEach(async (data: any) => {
      if (data.hi200kemasid === this.selectedShipment.hi200kemasid) {
        // create new field shipment lunas
        this.shipmentForm.patchValue({
          idx: data?.id,
          idCustomer: data?.idCustomer,
          hi209statudt: firebase.firestore.Timestamp.fromDate(new Date()),
          hi212alterby: this.selectedShipment.hi212alterby,
        });

        // set create new data status collection shipment
        this.service.updateShipment(this.shipmentForm.value).then((res: any) => {
          this.isLoading = false;

          if (res) {
            this.messageService.add({
              key: 'error',
              severity: 'error',
              summary: 'Error Message',
              detail: 'Can not update shipment - kirim!',
            });
          } else {
            this.messageService.add({
              key: 'success',
              severity: 'success',
              summary: 'Success Message',
              detail: 'Succesfully update shipment kirim',
            });

            this.removeModalTrans();
          }
        });
      }
    });
  }

  async deleteShipment() {
    this.isLoading = true;

    this.service.deleteShip(this.shipmentForm.value).then((res: any) => {
      if (res) {
        console.log('Error delete data shipment');
      } else {
        console.log('Your shipment success delete');

        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully delete shipment',
        });

        this.removeModalTrans();

        setTimeout(() => {
          this.service.deleteTrans(this.transactionForm.value).then((res: any) => {
            if (res) {
              console.log('Error delete data transaction');
            } else {
              console.log('Your transaction success delete');

              this.messageService.add({
                key: 'success',
                severity: 'success',
                summary: 'Success Message',
                detail: 'Succesfully delete transaction',
              });
            }
          });
        }, 0);
      }
    });
  }

  async moveTrash() {
    const user = await this.fireauth.currentUser;

    if (user) {
      this.readDataShip.forEach(async (data: any) => {
        setTimeout(() => {
          if (data.hi200kemasid === this.selectedShipment.hi200kemasid) {
            // create new field shipment lunas
            this.shipmentForm.patchValue({
              idx: data?.id,
              idCustomer: data?.idCustomer,
              hi212alterby: this.readAdmin,
              hi209statudt: firebase.firestore.Timestamp.fromDate(new Date()),
              hi210statuss: 9,
            });

            // set create new data status collection shipment
            this.service.updateStatus(this.shipmentForm.value).then((res: any) => {
              this.isLoading = false;

              if (res) {
                this.messageService.add({
                  key: 'error',
                  severity: 'error',
                  summary: 'Error Message',
                  detail: 'Can not update status shipment to sampah!',
                });
              } else {
                this.messageService.add({
                  key: 'success',
                  severity: 'success',
                  summary: 'Success Message',
                  detail: 'Succesfully update status shipment to sampah',
                });

                this.removeModalTrans();
              }
            });
          }
        }, 0);
      });
      
      this.readDataTrans.forEach(async (dataTrans: any) => {
        setTimeout(() => {
          if (dataTrans.dn200kemasid === this.selectedShipment.hi200kemasid) {
            // set update status transaction tagih
            this.transactionForm.patchValue({
              idBelanja: dataTrans?.id,
              idGoods: dataTrans?.idGoods,
              dn312alterby: this.readAdmin,
              dn309statudt: firebase.firestore.Timestamp.fromDate(new Date()),
              dn310sstatus: 9,
            });

            this.service.updateStatusTrans(this.transactionForm.value).then((res: any) => {
              this.isLoading = false;

              if (res) {
                this.messageService.add({
                  key: 'error',
                  severity: 'error',
                  summary: 'Error Message',
                  detail: 'Can not update status transaction to sampah!',
                });
              } else {
                this.messageService.add({
                  key: 'success',
                  severity: 'success',
                  summary: 'Success Message',
                  detail: 'Succesfully move transaction to status sampah',
                });

                this.removeModalTrans();
              }
            });
          }
        }, 1000);
      });
    }
  }
}
