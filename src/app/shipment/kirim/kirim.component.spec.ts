import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KirimComponent } from './kirim.component';

describe('KirimComponent', () => {
  let component: KirimComponent;
  let fixture: ComponentFixture<KirimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KirimComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KirimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
