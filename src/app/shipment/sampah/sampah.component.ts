import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SampahService } from './sampah.service';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { map } from 'rxjs/operators';
import { StartService } from '@app/transaction/start/start.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sampah',
  templateUrl: './sampah.component.html',
  styleUrls: ['./sampah.component.scss'],
  providers: [MessageService, DatePipe],
})
export class SampahComponent implements OnInit {
  isLoading = false;
  error: string | undefined;

  modalAdd: boolean = false;
  modalDetail: boolean = false;
  modalKirim: boolean = false;
  modalListAddress: boolean = false;

  transactionForm!: FormGroup;
  shipmentForm!: FormGroup;
  idCustom: any | undefined;
  alterBy: any | undefined;

  idShipment: any | undefined;
  idGoods: any | undefined;
  idBelanja: any | undefined;
  idCustomer: any | undefined;
  idx: any | undefined;
  readData: any | undefined;
  dataLength: any | undefined;
  
  readDataTrans: any | undefined;
  readDataShip: any | undefined;

  readAddressCustomer: any | undefined;
  readAddress: any | undefined;
  countryID: any | undefined;
  countryCustomer: any | undefined;
  provinceCustomer: any | undefined;
  cityCustomer: any | undefined;
  postCustomer: any | undefined;
  otherCustomer: any | undefined;
  telpCustomer: any | undefined;
  geoCustomer: any | undefined;

  searchValue: string | undefined;
  searchValueAddress: string | undefined;
  currentUser: any | undefined;
  currentUserCustomer: any | undefined;

  statusID: any | undefined;
  readAdmin: any | undefined;

  selectedShipment: any | undefined;

  get tf() {
    return this.shipmentForm.controls;
  }

  _selectedColumns: any[];
  cols: any[];

  submitted = false;

  country = [
    {
      id: 81,
      name: 'Jepang',
    },
    {
      id: 62,
      name: 'Indonesia',
    },
  ];

  province = [
    {
      id: 1,
      name: 'Aceh',
    },
    {
      id: 2,
      name: 'Sumatera Utara',
    },
    {
      id: 3,
      name: 'Sumatera Barat',
    },
    {
      id: 4,
      name: 'Riau',
    },
    {
      id: 5,
      name: 'Jambi',
    },
    {
      id: 6,
      name: 'Sumatera Selatan',
    },
    {
      id: 7,
      name: 'Bengkulu',
    },
    {
      id: 8,
      name: 'Lampung',
    },
    {
      id: 9,
      name: 'Kepulauan Bangka Belitung',
    },
    {
      id: 10,
      name: 'Kepulauan Riau',
    },
    {
      id: 11,
      name: 'Dki Jakarta',
    },
    {
      id: 12,
      name: 'Jawa Barat',
    },
    {
      id: 13,
      name: 'Jawa Tengah',
    },
    {
      id: 14,
      name: 'Di Yogyakarta',
    },
    {
      id: 15,
      name: 'Jawa Timur',
    },
    {
      id: 16,
      name: 'Banten',
    },
    {
      id: 17,
      name: 'Bali',
    },
    {
      id: 18,
      name: 'Nusa Tenggara Barat',
    },
    {
      id: 19,
      name: 'Nusa Tenggara Timur',
    },
    {
      id: 20,
      name: 'Kalimantan Barat',
    },
    {
      id: 21,
      name: 'Kalimantan Tengah',
    },
    {
      id: 22,
      name: 'Kalimantan Selatan',
    },
    {
      id: 23,
      name: 'Kalimantan Timur',
    },
    {
      id: 24,
      name: 'Kalimantan Utara',
    },
    {
      id: 25,
      name: 'Sulawesi Utara',
    },
    {
      id: 26,
      name: 'Sulawesi Tengah',
    },
    {
      id: 27,
      name: 'Sulawesi Selatan',
    },
    {
      id: 28,
      name: 'Sulawesi Tenggara',
    },
    {
      id: 29,
      name: 'Gorontalo',
    },
    {
      id: 30,
      name: 'Sulawesi Barat',
    },
    {
      id: 31,
      name: 'Maluku',
    },
    {
      id: 32,
      name: 'Maluku Utara',
    },
    {
      id: 33,
      name: 'Papua Barat',
    },
    {
      id: 34,
      name: 'Papua',
    },
  ];

  kurir = [
    {
      id: 0,
      name: 'Default',
    },
    {
      id: 1,
      name: 'Ambil Sendiri',
    },
    {
      id: 2,
      name: 'Anter Aja',
    },
    {
      id: 3,
      name: 'GO-SEND',
    },
    {
      id: 4,
      name: 'GOJEK',
    },
    {
      id: 5,
      name: 'GRAB',
    },
    {
      id: 6,
      name: 'JNE',
    },
    {
      id: 7,
      name: 'JNE JTR',
    },
    {
      id: 8,
      name: 'JNE OK',
    },
    {
      id: 9,
      name: 'JNE YES',
    },
    {
      id: 10,
      name: 'J&T',
    },
    {
      id: 11,
      name: 'Kurir',
    },
    {
      id: 12,
      name: 'MEX',
    },
    {
      id: 13,
      name: 'Pos Express',
    },
    {
      id: 14,
      name: 'Pos Kilat Khusus',
    },
    {
      id: 15,
      name: 'REX',
    },
    {
      id: 16,
      name: 'Si Cepat',
    },
    {
      id: 17,
      name: 'TIKI',
    },
    {
      id: 18,
      name: 'TIKI ECO',
    },
    {
      id: 19,
      name: 'TIKI ONS',
    },
    {
      id: 20,
      name: 'TIKI TCR',
    },
  ];

  constructor(
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private fireauth: AngularFireAuth,
    private firestore: AngularFirestore,
    private service: SampahService,
    private messageService: MessageService,
    private startService: StartService,
    private router: Router,
    private datePipe: DatePipe
  ) {
    this.Form();
  }

  ngOnInit() {
    this.getShipment();
    this.getTransaction();

    // set item column on p-table
    this.cols = [
       // { field: 'hi204paypric', header: 'Payment Price' },
      // { field: 'hi011postnum', header: 'Post Code' },
      { field: 'hi200kemasid', header: 'KmasID' },
      { field: 'hi000custoid', header: 'CustID' },
      { field: 'hi006fulname', header: 'Pmbeli' },
      { field: 'hi203onkiprc', header: 'Onkr' },
      { field: 'hi009namalen', header: 'Ptrima' },
      { field: 'hi012prefect', header: 'Prov' },
      { field: 'hi013citynam', header: 'Kota' },
      { field: 'hi014lainnya', header: 'Jln' },
      { field: 'hi015telpnum', header: 'Telp' },
      { field: 'hi205viakuri', header: 'Via' },
      { field: 'hi206resinum', header: 'ResiNo' },
      { field: 'hi205catatan', header: 'CatS' },
    ];

    this._selectedColumns = this.cols;
  }

  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col: any) => val.includes(col));
  }
  
  addModal() {
    this.modalAdd = true;
    this.renderer.addClass(document.body, 'modal-open');

    this.shipmentForm.reset();
  }
  
  listTransaction(event: any) {
    this.idShipment = event.hi200kemasid;
    this.router.navigate(['transaction-shipment', this.idShipment]);
  }

  removeModalTrans() {
    this.modalAdd = false;
    this.modalDetail = false;
    this.renderer.removeClass(document.body, 'modal-open');
  }

  removeModalAddress() {
    this.modalListAddress = false;
    this.lulaId(this.selectedShipment);
    this.currentUser = "";
    this.currentUserCustomer = "";
    this.searchValueAddress = "";

    this.renderer.removeClass(document.body, 'modal-open');
  }

  private Form() {
    this.transactionForm = this.formBuilder.group({
      idBelanja: [''],
      idGoods: [''],
      dn312alterby: [''],
      dn309statudt: [''],
      dn310sstatus: [''],
    });

    this.shipmentForm = this.formBuilder.group({
      idx: [''],
      idCustomer: [''],
      hi200kemasid: ['', [Validators.required]],
      hi205viakuri: [0],
      hi206resinum: [''],
      hi203onkiprc: [0],
      hi204paypric: [-999999999999],
      hi205catatan: [''],
      hi212alterby: [''],
      hi209statudt: [''],
      hi210statuss: [''],
      hi000custoid: [''],
      hi006fulname: [''],
      hi009namalen: [''],
      hi010country: [''],
      hi011postnum: [''],
      hi012prefect: [''],
      hi013citynam: [''],
      hi014lainnya: [''],
      hi015telpnum: [''],
      hi016geopoin: [''],
    });
  }

  closed(event: any) {
    console.log('event ' + event.target.className);
    if(event.target.className === "modal fade show") {
      this.modalDetail = false;
      this.renderer.removeClass(document.body, 'modal-open');
    } 
  }

  closedAddress(event: any) {
    if(event.target.className === "modal fade show") {
      this.removeModalAddress();
    } 
  }

  lulaId(event: any) {
    console.log('event ' + JSON.stringify(event));

    // get collection address by id customer
    // this.getAddress();

    this.selectedShipment = event;

    this.modalDetail = true;
    this.renderer.addClass(document.body, 'modal-open');

    const getCountry = event.hi010country;
    const filterCountry = this.country.filter((data: any) => data.id == getCountry);
    this.countryCustomer = filterCountry[0];
    // console.log('this.countryCustomer ' + JSON.stringify(this.countryCustomer));

    const getProvince = event.hi012prefect;
    const filterProvince = this.province.filter((data: any) => data.id == getProvince);
    this.provinceCustomer = filterProvince[0];
    // console.log('this.provinceCustomer ' + JSON.stringify(this.provinceCustomer));

    const statusDate = moment(new Date(event.hi209statudt?.seconds * 1000)).format('MM/DD/YYYY hh:mm:ss A');

    this.idCustomer = event.idCustomer;
    this.cityCustomer = event.hi013citynam;
    this.postCustomer = event.hi011postnum;
    this.otherCustomer = event.hi014lainnya;
    this.telpCustomer = event.hi015telpnum;
    this.geoCustomer = event.hi016geopoin;

    this.shipmentForm.patchValue({
      idx: event.id,
      idCustomer: event.idCustomer,
      hi200kemasid: event.hi200kemasid,
      hi000custoid: event.hi000custoid,
      hi006fulname: event.hi006fulname,
      hi009namalen: event.hi009namalen,
      hi010country: this.countryCustomer.name,
      hi011postnum: event.hi011postnum,
      hi012prefect: this.provinceCustomer.name,
      hi013citynam: event.hi013citynam,
      hi014lainnya: event.hi014lainnya,
      hi015telpnum: event.hi015telpnum,
      hi016geopoin: event.hi016geopoin.latitude + ',' + event.hi016geopoin.longitude,
      hi205viakuri: event.hi205viakuri,
      hi206resinum: event.hi206resinum,
      hi203onkiprc: event.hi203onkiprc,
      hi204paypric: event.hi204paypric,
      hi205catatan: event.hi205catatan,
      hi212alterby: event.hi212alterby,
      hi209statudt: statusDate,
      hi210statuss: event.hi210statuss,
    });
  }

  getCustomer() {
    this.firestore.collection('cl001members').doc(this.idCustomer).ref.get().then((doc => {
      if (doc.exists) {
        this.readAddressCustomer = doc.data();
        console.log('this.readAddressCustomer ' + JSON.stringify(this.readAddressCustomer.dk014lainnya));
      } else {
        console.log('No Data Customer Ref');
        this.readAddressCustomer= [];
      }
    }));
  }

  async getAddress() {
    this.isLoading = true;

    (await this.service.readAddress()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          // Get all data address
          // console.log('this.idCustomer ' + JSON.stringify(this.idCustomer));

          this.readAddress = this.firestore
            .collection(`cl001members/${this.idCustomer}/cl007address`, (ref) => ref.orderBy('ak009namalen'))
            .snapshotChanges()
            .pipe(
              map((actions) =>
                actions.map((a) => {
                  const data = a.payload.doc.data() as any;
                  const id = a.payload.doc.id;
                  const idCustomer = a.payload.doc.ref.parent.parent.id;
                  const meta = a.payload.doc.metadata;

                  // console.log('idCustomer ' + JSON.stringify(idCustomer));
                  return { id, meta, idCustomer, ...data };
                })
              )
            );
        } else {
          this.readAddress = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach address from the server!',
        });
      }
    });
  }

  changeAddress() {
    // console.log('change customer ' + JSON.stringify(event.idCustomer));
    // console.log('change id ' + JSON.stringify(event.id));
    const getDataShipment = this.selectedShipment;

    console.log('getDataShipment ' + JSON.stringify(getDataShipment));

    this.idCustomer = getDataShipment.idCustomer;
    this.idx = getDataShipment.id;

    this.modalListAddress = true;
    this.getAddress();
    this.getCustomer();
  }

  async addressId(event: any, item: any) {
    console.log('item idx ' + JSON.stringify(item));
    // console.log('item customer ' + JSON.stringify(this.idCustomer));
    // console.log('item idx ' + JSON.stringify(this.idx));
    this.currentUser = item.ak009namalen;
    this.currentUserCustomer = item.dk006fulname;

    this.shipmentForm.patchValue({
      idx: this.idx,
      idCustomer: this.idCustomer,
      hi009namalen: item.ak009namalen,
      hi010country: item.ak010country,
      hi011postnum: item.ak011postnum,
      hi012prefect: item.ak012prefect,
      hi013citynam: item.ak013citynam,
      hi014lainnya: item.ak014lainnya,
      hi015telpnum: item.ak015telpnum,
      hi016geopoin: item.ak016geopoin,
    });
  }

  async getAdmin() {
    (await this.startService.getAdmin()).subscribe((res: any) => {
      if (res) {
        const status = res;
        if (status) {
          this.readAdmin = res[0].di004admname;
        } else {
          this.readAdmin = [];
        }
      }
    });
  }

  async getTransaction() {
    this.isLoading = true;

    (await this.service.getTrans()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          this.readDataTrans = res.filter((data: any) => data.dn310sstatus == 9);
        } else {
          this.readDataTrans = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach the server!',
        });
      }
    });
  }

  async getShipment() {
    this.isLoading = true;

    (await this.service.getShip()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          const filterStatus = res.filter((data: any) => data.hi210statuss == 9);
          this.readDataShip = filterStatus;

          let matches = new Set();
          const filterKemas = filterStatus.filter((elem: any) => {
            const { hi200kemasid } = elem;
            if (matches.has(hi200kemasid)) {
              return false;
            } else {
              matches.add(hi200kemasid);
              return true;
            }
          })

          this.readData = filterKemas;
          console.log('output ' + JSON.stringify(filterKemas));
        } else {
          this.readData = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach the server!',
        });
      }
    });
  }

  async updateShipment() {
    this.isLoading = true;
    this.submitted = true;

    const user = await this.fireauth.currentUser;

    this.readDataShip.forEach(async (data: any) => {
      if (data.hi200kemasid === this.selectedShipment.hi200kemasid) {
        // create new field shipment lunas
        this.shipmentForm.patchValue({
          idx: data?.id,
          idCustomer: data?.idCustomer,
          hi209statudt: firebase.firestore.Timestamp.fromDate(new Date()),
          hi212alterby: this.selectedShipment.hi212alterby,
        });

        // set create new data status collection shipment
        this.service.updateShipment(this.shipmentForm.value).then((res: any) => {
          this.isLoading = false;

          if (res) {
            this.messageService.add({
              key: 'error',
              severity: 'error',
              summary: 'Error Message',
              detail: 'Can not update shipment - sampah!',
            });
          } else {
            this.messageService.add({
              key: 'success',
              severity: 'success',
              summary: 'Success Message',
              detail: 'Succesfully update shipment sampah',
            });

            this.removeModalTrans();
          }
        });
      }
    });
  }
}
