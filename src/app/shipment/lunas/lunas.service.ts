import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

const routes = {
  read: () => `/cl005shipmnt`,
  customer: () => '/cl001members',
  address: () => 'cl007address',
};

export interface dataContext {
  // The quote's category: 'dev', 'explicit'...
  category: string;
}

export interface idCount {
  idCount: number;
}

export interface transactionContext {
  idBelanja: any;
  idGoods: any;
  dn200kemasid: string;
  dn312alterby: string;
  dn309statudt: string;
  dn310sstatus: number;
}

export interface shipmentContext {
  idx: any;
  idCustomer: any;
  hi200kemasid: string;
  hi205viakuri: number;
  hi206resinum: string;
  hi203onkiprc: number;
  hi204paypric: number;
  hi205catatan: string;
  hi212alterby: string;
  hi209statudt: string,
  hi210statuss: number;
  hi000custoid: string;
  hi006fulname: string;
  hi009namalen: string;
  hi010country: number;
  hi011postnum: string;
  hi012prefect: number;
  hi013citynam: string;
  hi014lainnya: string;
  hi015telpnum: string;
  hi016geopoin: string;
}

export interface addressContext {
  idCustomer: any;
  idAddress: any;
  ak009namalen: string;
  ak010country: string;
  ak011postnum: string;
  ak012prefect: number;
  ak013citynam: string;
  ak014lainnya: string;
  ak015telpnum: number;
  ak016geopoin: any;
}

@Injectable({
  providedIn: 'root',
})
export class LunasService {
  idCustom: any;
  value = new Subject<string>();

  constructor(private firestore: AngularFirestore, private fireauth: AngularFireAuth) {}

  async readShip() {
    return this.firestore.collectionGroup('cl005shipmnt').valueChanges() as Observable<shipmentContext[]>;
  }

  async getShip() {
    return this.firestore
      .collectionGroup(`cl005shipmnt`, (ref) => ref.orderBy('hi000custoid'))
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as any;
            const id = a.payload.doc.id;
            const idCustomer = a.payload.doc.ref.parent.parent.id;
            const meta = a.payload.doc.metadata;

            return { id, meta, idCustomer, ...data };
          })
        )
      );
  }

  async updateAddress(context: shipmentContext) {
    let user = await this.fireauth.currentUser;
    console.log('get id customer ' + context.idCustomer);
    console.log('get id shipment ' + context.idx);

    if (user) {
      this.firestore
        .doc(`cl001members/${context.idCustomer}`)
        .collection('cl005shipmnt')
        .doc(context.idx)
        .update({
          hi009namalen: context.hi009namalen,
          hi010country: context.hi010country,
          hi011postnum: context.hi011postnum,
          hi012prefect: context.hi012prefect,
          hi013citynam: context.hi013citynam,
          hi014lainnya: context.hi014lainnya,
          hi015telpnum: context.hi015telpnum,
          hi016geopoin: context.hi016geopoin,
        });
    }
  }

  async updateShipment(context: shipmentContext) {
    let user = await this.fireauth.currentUser;
    console.log('get id customer ' + context.idCustomer);
    console.log('get id shipment ' + context.idx);

    if (user) {
      this.firestore
        .doc(`cl001members/${context.idCustomer}`)
        .collection('cl005shipmnt')
        .doc(context.idx)
        .update({
          hi200kemasid: context.hi200kemasid,
          hi205viakuri: context.hi205viakuri,
          hi206resinum: context.hi206resinum,
          hi203onkiprc: context.hi203onkiprc,
          hi204paypric: context.hi204paypric,
          hi205catatan: context.hi205catatan,
          hi212alterby: context.hi212alterby,
          hi209statudt: context.hi209statudt,
          hi210statuss: context.hi210statuss,
          // hi000custoid: context.hi000custoid,
          // hi006fulname: context.hi006fulname,
          // hi009namalen: context.hi009namalen,
          // hi010country: context.hi010country,
          // hi011postnum: context.hi011postnum,
          // hi012prefect: context.hi012prefect,
          // hi013citynam: context.hi013citynam,
          // hi014lainnya: context.hi014lainnya,
          // hi015telpnum: context.hi015telpnum,
          // hi016geopoin: context.hi016geopoin,
        });
    }
  }

  async updateStatus(context: shipmentContext) {
    let user = await this.fireauth.currentUser;
    console.log('context.idx ' + context.idx);
    console.log('context.idCustomer ' + context.idCustomer);

    if (user) {
      // firebase.firestore().doc(`cl001members/${context.idCustomer}`).collection('cl005shipmnt').where('hi200kemasid', 'in', [context.hi200kemasid]).get().then(function (querySnapshot) {
      //   querySnapshot.forEach(function (doc) {
      //     doc.ref.update({
      //       hi212alterby: context.hi212alterby,
      //       hi209statudt: context.hi209statudt,
      //       hi210statuss: context.hi210statuss,
      //     });
      //   });
      // });

      this.firestore
        .doc(`cl001members/${context.idCustomer}`)
        .collection('cl005shipmnt')
        .doc(context.idx)
        .update({
          hi212alterby: context.hi212alterby,
          hi209statudt: context.hi209statudt,
          hi210statuss: context.hi210statuss,
        });
    }
  }

  async deleteTrans(context: shipmentContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      this.firestore.collection(routes.read()).doc(context.idx).delete();
    }
  }

  async readAddress() {
    return this.firestore.collectionGroup(routes.address()).valueChanges() as Observable<addressContext[]>;
  }

  async readTrans() {
    return this.firestore.collectionGroup('cl004belanja').valueChanges() as Observable<transactionContext[]>;
  }

  async updateStatusTrans(context: transactionContext) {
    let user = await this.fireauth.currentUser;

    console.log('context.idBelanja ' + context.idBelanja);
    console.log('context.idGoods ' + context.idGoods);

    if (user) {
      // firebase.firestore().doc(`cl003merchan/${context.idGoods}`).collection('cl004belanja').where('dn200kemasid', 'in', [context.dn200kemasid]).get().then(function (querySnapshot) {
      //   querySnapshot.forEach(function (doc) {
      //     doc.ref.update({
      //       dn312alterby: context.dn312alterby,
      //       dn309statudt: context.dn309statudt,
      //       dn310sstatus: context.dn310sstatus,
      //     });
      //   });
      // });

      this.firestore
        .doc(`cl003merchan/${context.idGoods}`)
        .collection('cl004belanja')
        .doc(context.idBelanja)
        .update({
          dn312alterby: context.dn312alterby,
          dn309statudt: context.dn309statudt,
          dn310sstatus: context.dn310sstatus,
        });
    }
  }

  async getTrans() {
    return this.firestore
      .collectionGroup<transactionContext>('cl004belanja')
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as transactionContext;
            const id = a.payload.doc.id;
            const meta = a.payload.doc.metadata;
            const idGoods = a.payload.doc.ref.parent.parent.id;
            return { id, meta, idGoods, ...data };
          })
        )
      );
  }
}
