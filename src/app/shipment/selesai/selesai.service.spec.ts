import { TestBed } from '@angular/core/testing';

import { SelesaiService } from './selesai.service';

describe('SelesaiService', () => {
  let service: SelesaiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SelesaiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
