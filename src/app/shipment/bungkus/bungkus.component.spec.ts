import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BungkusComponent } from './bungkus.component';

describe('BungkusComponent', () => {
  let component: BungkusComponent;
  let fixture: ComponentFixture<BungkusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BungkusComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BungkusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
