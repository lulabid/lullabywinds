import { TestBed } from '@angular/core/testing';

import { BungkusService } from './bungkus.service';

describe('BungkusService', () => {
  let service: BungkusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BungkusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
