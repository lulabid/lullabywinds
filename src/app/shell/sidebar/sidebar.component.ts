import { Component, OnInit } from '@angular/core';

import { StartService } from '../../transaction/start/start.service';
import { LunasService } from '../../shipment/lunas/lunas.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  show: boolean = false;
  expanded: boolean = false;

  show1: boolean = false;
  expanded1: boolean = false;

  package: boolean = false;
  expandedPackage: boolean = false;

  start: any | undefined;
  kotak: any | undefined;
  otewe: any | undefined;
  setok: any | undefined;
  tagih: any | undefined;
  lunas: any | undefined;
  selesai: any | undefined;
  sampah: any | undefined;

  lunasx: any | undefined;
  bungkusx: any | undefined;
  kirimx: any | undefined;
  selesaix: any | undefined;
  sampahx: any | undefined;

  constructor(private tranService: StartService, private shipService: LunasService) {}

  ngOnInit() {
    this.getTransaction();
    this.getShipment();
  }

  getShow() {
    this.show = !this.show;
    this.expanded = !this.expanded;
  }

  getShow1() {
    this.show1 = !this.show1;
    this.expanded1 = !this.expanded1;
  }

  getPackage() {
    this.package = !this.package;
    this.expandedPackage = !this.expandedPackage;
  }

  async getTransaction() {
    (await this.tranService.getTrans()).subscribe((res: any) => {
      if (res) {
        const status = res;
        if (status) {
          const start = res.filter((data: any) => data.dn310sstatus == 1);
          this.start = start.length;

          const kotak = res.filter((data: any) => data.dn310sstatus == 2);
          this.kotak = kotak.length;

          const otewe = res.filter((data: any) => data.dn310sstatus == 3);
          this.otewe = otewe.length;

          const setok = res.filter(
            (data: any) => (data.dn310sstatus == 4 && data.dn000custoid == '0000000000086') || (data.dn310sstatus == 4 && data.dn000custoid == '0000000000093')
          );
          this.setok = setok.length;

          const tagih = res.filter(
            (data: any) => (data.dn310sstatus == 4 && data.dn000custoid != '0000000000086') && (data.dn310sstatus == 4 && data.dn000custoid != '0000000000093')
          );
          this.tagih = tagih.length;

          const lunas = res.filter(
            (data: any) =>
              data.dn310sstatus == 5 ||
              data.dn310sstatus == 6 ||
              data.dn310sstatus == 7
          );
          this.lunas = lunas.length;

          const selesai = res.filter((data: any) => data.dn310sstatus == 8);
          this.selesai = selesai.length;

          const sampah = res.filter((data: any) => data.dn310sstatus == 9);
          this.sampah = sampah.length;
        } else {
          this.start = [];
          this.kotak = [];
          this.otewe = [];
          this.setok = [];
          this.tagih = [];
          this.lunas = [];
          this.selesai = [];
          this.sampah = [];
        }
      }
    });
  }

  async getShipment() {
    (await this.shipService.getShip()).subscribe((res: any) => {
      if (res) {
        const status = res;
        if (status) {
          const lunasx = res.filter((data: any) => data.hi210statuss == 5);
          let matchesLunas = new Set();
          const filterLunas = lunasx.filter((elem: any) => {
            const { hi200kemasid } = elem;
            if (matchesLunas.has(hi200kemasid)) {
              return false;
            } else {
              matchesLunas.add(hi200kemasid);
              return true;
            }
          })
          this.lunasx = filterLunas.length;

          const bungkusx = res.filter((data: any) => data.hi210statuss == 6);
          let matchesBungkus = new Set();
          const filterBunkgkus = bungkusx.filter((elem: any) => {
            const { hi200kemasid } = elem;
            if (matchesBungkus.has(hi200kemasid)) {
              return false;
            } else {
              matchesBungkus.add(hi200kemasid);
              return true;
            }
          })
          this.bungkusx = filterBunkgkus.length;

          const kirimx = res.filter((data: any) => data.hi210statuss == 7);
          let matchesKirim = new Set();
          const filterKirim = kirimx.filter((elem: any) => {
            const { hi200kemasid } = elem;
            if (matchesKirim.has(hi200kemasid)) {
              return false;
            } else {
              matchesKirim.add(hi200kemasid);
              return true;
            }
          })
          this.kirimx = filterKirim.length;

          const selesaix = res.filter((data: any) => data.hi210statuss == 8);
          let matchesSelesai = new Set();
          const filterSelsai = selesaix.filter((elem: any) => {
            const { hi200kemasid } = elem;
            if (matchesSelesai.has(hi200kemasid)) {
              return false;
            } else {
              matchesSelesai.add(hi200kemasid);
              return true;
            }
          })
          this.selesaix = filterSelsai.length;

          const sampahx = res.filter((data: any) => data.hi210statuss == 9);
          let matchesSampah = new Set();
          const filterSampah = sampahx.filter((elem: any) => {
            const { hi200kemasid } = elem;
            if (matchesSampah.has(hi200kemasid)) {
              return false;
            } else {
              matchesSampah.add(hi200kemasid);
              return true;
            }
          })
          this.sampahx = filterSampah.length;
        } else {
          this.lunasx = [];
          this.bungkusx = [];
          this.kirimx = [];
          this.selesaix = [];
          this.sampahx = [];
        }
      }
    });
  }
}
