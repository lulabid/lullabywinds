import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const routes = {
  read: () => `/cl004belanja`,
  customer: () => '/cl001members',
  merchant: () => '/cl003merchan',
  shipment: () => 'cl005shipmnt',
};

export interface transactionContext {
  idx: any;
  idGoods: any;
  dn300transid: string;
  dn307expordt: string;
  dn308whatbox: number;
  dn302qantity: number;
  dn101bymaker: string;
  dn102bseries: string;
  dn103handler: string;
  dn104release: string;
  dn111fotobrg: string;
  dn107barcoid: string;
  dn000custoid: string;
  dn006fulname: string;
  dn200kemasid: string;
  dn303dealprc: number;
  dn304paypric: number;
  dn305catatan: string;
  dn312alterby: string;
  dn309statudt: string;
  dn310sstatus: number;
}

export interface memberContext {
  idCustomer: any;
  dk000custoid: string;
  dk001emaddre: string;
  dk006fulname: string;
  dk010country: number;
  dk011postnum: string;
  dk012prefect: number;
  dk013citynam: string;
  dk014lainnya: string;
  dk015telpnum: number;
  dk016geopoin: any;
  dk005catatan: string;
  dk007created: string;
}

export interface goodsContext {
  idx: any;
  dm100merchid: string;
  dm107barcoid: string;
  dm101bymaker: string;
  dm102bseries: string;
  dm103handler: string;
  dm111fotobrg: string;
  dm104release: string;
  dm105catatan: string;
  dm109created: string;
}

export interface shipmentContext {
  idx: any;
  idCustomer: any;
  hi200kemasid: string;
  hi205viakuri: number;
  hi206resinum: string;
  hi203onkiprc: number;
  hi204paypric: number;
  hi205catatan: string;
  hi212alterby: string;
  hi209statudt: string,
  hi210statuss: number;
  hi000custoid: string;
  hi006fulname: string;
  hi009namalen: string;
  hi010country: number;
  hi011postnum: string;
  hi012prefect: number;
  hi013citynam: string;
  hi014lainnya: string;
  hi015telpnum: string;
  hi016geopoin: string;
}

@Injectable({
  providedIn: 'root'
})
export class TransactionShipmentService {
  constructor(private firestore: AngularFirestore, private fireauth: AngularFireAuth) {}

  async getTrans() {
    return this.firestore
      .collectionGroup(`cl004belanja`)
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as any;
            const id = a.payload.doc.id;
            const idGood = a.payload.doc.ref.parent.parent.id;
            const meta = a.payload.doc.metadata;
  
            return { id, meta, idGood, ...data };
          })
        )
      ) as Observable<transactionContext[]>;
  }

  async getShipment() {
    return this.firestore
      .collectionGroup(`cl005shipmnt`)
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as any;
            const id = a.payload.doc.id;
            const idCustomer = a.payload.doc.ref.parent.parent.id;
            const meta = a.payload.doc.metadata;
  
            return { id, meta, idCustomer, ...data };
          })
        )
      ) as Observable<transactionContext[]>;
  }
  
  async readCustomer() {
    return this.firestore.collection(routes.customer()).valueChanges() as Observable<memberContext[]>;
  }

  async readMerchant() {
    return this.firestore.collection(routes.merchant()).valueChanges() as Observable<goodsContext[]>;
  }

  async readShipment() {
    return this.firestore.collectionGroup(routes.shipment()).valueChanges() as Observable<shipmentContext[]>;
  }
}
