import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionShipmentComponent } from './transaction-shipment.component';

describe('TransactionShipmentComponent', () => {
  let component: TransactionShipmentComponent;
  let fixture: ComponentFixture<TransactionShipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionShipmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionShipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
