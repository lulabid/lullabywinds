import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TransactionShipmentService } from './transaction-shipment.service';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-transaction-shipment',
  templateUrl: './transaction-shipment.component.html',
  styleUrls: ['./transaction-shipment.component.scss'],
  providers: [MessageService, DatePipe],
})
export class TransactionShipmentComponent implements OnInit {
  isLoading = false;
  error: string | undefined;

  modalAdd: boolean = false;
  modalDetail: boolean = false;
  modalKirim: boolean = false;
  modalListAddress: boolean = false;
  modalPhoto: boolean = false;

  transactionForm!: FormGroup;
  idCustom: any | undefined;
  alterBy: any | undefined;

  idx: any | undefined;
  idGoods: any | undefined;
  idCustomer: any | undefined;
  dataLength: any | undefined;
  searchValue: string | undefined;

  readData: any | undefined;
  readDataFilter: any | undefined;
  readAllCustomer: any | undefined;
  readDataMerchant: any | undefined;
  readDataCustomer: any | undefined;
  readDataShipment: any | undefined;

  selectedData: any;
  docCustomer: any | undefined;

  statusID: any | undefined;

  noImage: any = 'assets/no-image.jpg';
  pictureFile: any = 'assets/no-image.jpg';

  ifUser: boolean = false;
  ifMerchant: boolean = false;
  ifShipment: boolean = false;

  _selectedColumns: any[];
  cols: any[];

  constructor(
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private fireauth: AngularFireAuth,
    private firestore: AngularFirestore,
    private service: TransactionShipmentService,
    private messageService: MessageService,
    private routeActive: ActivatedRoute
  ) {
    this.Form();
  }

  ngOnInit(): void {
    this.getAllCustomer();
    this.getShipment();
    this.getTransaction();

    // history data prev route
    this.routeActive.params.subscribe((params) => {
      this.idCustomer = params['id'];
    });

    // set item column on p-table
    this.cols = [
      // { field: 'dn304paypric', header: 'Payment Price' },
      // { field: 'dn111fotobrg', header: 'Photo' },
      { field: 'dn107barcoid', header: 'BarcID' },
      { field: 'dn103handler', header: 'Baran' },
      { field: 'dn101bymaker', header: 'Mker' },
      { field: 'dn102bseries', header: 'Seri' },
      { field: 'dn302qantity', header: 'Q' },
      { field: 'dn000custoid', header: 'CustID' },
      { field: 'dn006fulname', header: 'Pmbeli' },
      { field: 'dn303dealprc', header: 'Sisa' },
      { field: 'dn200kemasid', header: 'KmasID' },
      { field: 'dn308whatbox', header: 'K' },
      { field: 'dn307expordt', header: 'DteX' },
      { field: 'dn305catatan', header: 'CatT' },
    ];

    this._selectedColumns = this.cols;
  }

  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col: any) => val.includes(col));
  }

  addModal() {
    this.modalAdd = true;
    this.renderer.addClass(document.body, 'modal-open');

    this.transactionForm.reset();
  }

  removeModalTrans() {
    this.modalAdd = false;
    this.modalDetail = false;
    this.renderer.removeClass(document.body, 'modal-open');
  }

  lulaPhoto(event: any) {
    if(event.dn111fotobrg == "" || event.dn111fotobrg == null) {
      this.pictureFile = 'assets/no-image.jpg';
    } else {
      this.pictureFile = event.dn111fotobrg;
    }

    this.modalPhoto = true;
    this.renderer.addClass(document.body, 'modal-open');

  }

  private Form() {
    this.transactionForm = this.formBuilder.group({
     idx: [''],
      idGoods: [''],
      dn307expordt: [''],
      dn308whatbox: [''],
      dn302qantity: ['', [Validators.required]],
      dn300transid: [''],
      dn101bymaker: [''],
      dn102bseries: [''],
      dn103handler: [''],
      dn104release: [''],
      dn107barcoid: [''],
      dn111fotobrg: [''],
      dn000custoid: [''],
      dn006fulname: [''],
      dn200kemasid: [''],
      dn303dealprc: ['', [Validators.required]],
      dn304paypric: ['', [Validators.required]],
      dn305catatan: [''],
      dn312alterby: [''],
      dn309statudt: [''],
      dn310sstatus: [''],
    });
  }

  closed(event: any) {
    console.log('event ' + event.target.className);
    if(event.target.className === "modal fade show") {
      this.modalDetail = false;
      this.modalPhoto = false;
      this.renderer.removeClass(document.body, 'modal-open');
    } 
  }

  async lulaId(event: any) {
    console.log('event ' + JSON.stringify(event));

    this.modalDetail = true;
    this.renderer.addClass(document.body, 'modal-open');   
    
    const dateReleaseGoods = moment(new Date(event.dn104release?.seconds * 1000)).format('MM/DD/YYYY hh:mm:ss A');
    const statusDate = moment(new Date(event.dn309statudt?.seconds * 1000)).format('MM/DD/YYYY hh:mm:ss A');  

    // format type input datetime-local
    // let exportDate: any;
    // if (event.dn307expordt == "") {
    //   exportDate = event.dn307expordt;
    // } else {
    //   exportDate = moment(new Date(event.dn307expordt?.seconds * 1000)).format('yyyy-MM-DDThh:mm');  
    // }    

    const exportDate = moment(new Date(event.dn307expordt?.seconds * 1000)).format('MM/DD/YYYY hh:mm:ss A');     

    this.transactionForm.patchValue({
      idx: event.id,
      idGoods: event.idGood,
      dn000custoid: event.dn000custoid,
      dn006fulname: event.dn006fulname,
      dn307expordt: exportDate,
      dn308whatbox: event.dn308whatbox,
      dn302qantity: event.dn302qantity,
      dn200kemasid: event.dn200kemasid,
      dn103handler: event.dn103handler,
      dn107barcoid: event.dn107barcoid,
      dn101bymaker: event.dn101bymaker,
      dn102bseries: event.dn102bseries,
      dn300transid: event.dn300transid,
      dn104release: dateReleaseGoods,
      dn303dealprc: event.dn303dealprc,
      dn304paypric: event.dn304paypric,
      dn305catatan: event.dn305catatan,
      dn312alterby: event.dn312alterby,
      dn309statudt: statusDate,
      dn310sstatus: event.dn310sstatus,
      dn111fotobrg: event.dn111fotobrg
    });

    this.readAllCustomer.forEach((ev: any) => {
      const customerSelect = ev.filter((data: any) => data.dk000custoid === event.dn000custoid);

      this.selectedData = {
        id: customerSelect[0].id,
        meta: customerSelect[0].meta,
        dk000custoid: customerSelect[0].dk000custoid, 
        dk006fulname: customerSelect[0].dk006fulname, 
        dk001emaddre: customerSelect[0].dk001emaddre,
        dk005catatan: customerSelect[0].dk005catatan,
        dk007created: customerSelect[0].dk007created,
        dk010country: customerSelect[0].dk010country,
        dk011postnum: customerSelect[0].dk011postnum,
        dk012prefect: customerSelect[0].dk012prefect,
        dk013citynam: customerSelect[0].dk013citynam,
        dk014lainnya: customerSelect[0].dk014lainnya,
        dk015telpnum: customerSelect[0].dk015telpnum,
        dk016geopoin: customerSelect[0].dk016geopoin,
      };
    });   
  }

  async userId(event: any, item: any) {
    this.selectedData = {
      dk000custoid: item.dn000custoid, 
      dk006fulname: item.dn006fulname, 
    };

    this.transactionForm.patchValue({
      dn000custoid: item.dk000custoid,
      dn006fulname: item.dk006fulname
    });
  }

  async getAllCustomer() {
    this.isLoading = true;

    (await this.service.readCustomer()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        // Get all data customer
        this.readAllCustomer = this.firestore
          .collection('cl001members', (ref) => ref.orderBy('dk000custoid'))
          .snapshotChanges()
          .pipe(
            map((actions) =>
              actions.map((a) => {
                const data = a.payload.doc.data() as any;
                const id = a.payload.doc.id;
                const meta = a.payload.doc.metadata;

                // console.log('idtrans ' + JSON.stringify(id));
                return { id, meta, ...data };
              })
            )
          );
      } else {
        console.log('Can not reach the server customer');
      }
    });
  }

  async getTransaction() {
    this.isLoading = true;

    // get param id
    this.routeActive.params.subscribe((params) => {
      this.idCustom = params['id'];
    });

    (await this.service.getTrans()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {  
          this.readData = res.filter((data: any) => {
            if (data.dn200kemasid) {
              return data.dn200kemasid == this.idCustom
            }
          });
        } else {
          this.readData = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach the server transaction!',
        });
      }
    });
  }  

  async getShipment() {
    this.isLoading = true;

    (await this.service.readShipment()).subscribe((res: any) => {
      this.isLoading = false;   

      if (res) {
        // Get all data shipment
        this.readDataShipment = res.filter((data: any) => data.hi200kemasid == this.idCustom)[0];
        console.log('this.readDataShipment ' + JSON.stringify(this.readDataShipment));      

        if (this.readDataShipment) {
          this.ifShipment = true;
        } else {
          this.ifShipment = false;
        }
      } else {
        console.log('Can not reach the server shipment');
      }
    });
  }

}
