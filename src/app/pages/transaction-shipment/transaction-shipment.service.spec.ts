import { TestBed } from '@angular/core/testing';

import { TransactionShipmentService } from './transaction-shipment.service';

describe('TransactionShipmentService', () => {
  let service: TransactionShipmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransactionShipmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
