import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxBarcode6Module } from 'ngx-barcode6';
import { TranslateModule } from '@ngx-translate/core';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgApexchartsModule } from 'ng-apexcharts';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { PaginatorModule } from 'primeng/paginator';
import { TableModule } from 'primeng/table';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';

import { SharedModule } from '@shared';
import { BarangRoutingModule } from './barang-routing.module';
import { BarangComponent } from './barang.component';

@Pipe({
  name: 'searchFilter'
})

export class SearchFilterPipe implements PipeTransform {
  transform(value: any, args?: any): any {
      if(!value)return null;
      if(!args)return value;

      args = args.toLowerCase();

      return value.filter(function(data: any){
          return JSON.stringify(data).toLowerCase().includes(args);
      });
  }
}

@NgModule({
  declarations: [BarangComponent, SearchFilterPipe],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    FormsModule,
    NgbModule,
    SharedModule,
    PerfectScrollbarModule,
    NgxBarcode6Module,
    NgApexchartsModule,
    ToastModule,
    PaginatorModule,
    TableModule,
    AutoCompleteModule,
    DropdownModule,
    DialogModule,
    MultiSelectModule,
    BarangRoutingModule,
  ],
})
export class BarangModule {}
