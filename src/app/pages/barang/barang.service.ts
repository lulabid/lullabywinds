import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

const routes = {
  goods: () => `/cl003merchan`,
};

export interface dataContext {
  // The quote's category: 'dev', 'explicit'...
  category: string;
}

export interface idCount {
  idCount: number;
}

export interface goodsContext {
  idx: any;
  dm100merchid: string;
  dm107barcoid: string;
  dm101bymaker: string;
  dm102bseries: string;
  dm103handler: string;
  dm111fotobrg: string;
  dm104release: string;
  dm105catatan: string;
  dm109created: string;
}

export interface transactionContext {
  idx: any;
  dn300transid: string;
  dn307expordt: string;
  dn308whatbox: number;
  dn302qantity: number;
  // dn100merchid: string;
  dn101bymaker: string;
  dn102bseries: string;
  dn103handler: string;
  dn104release: string;
  dn111fotobrg: string;
  dn107barcoid: string;
  dn000custoid: string;
  dn006fulname: string;
  dn200kemasid: string;
  dn303dealprc: number;
  dn304paypric: number;
  dn305catatan: string;
  dn312alterby: string;
  dn309statudt: string;
  dn310sstatus: number;
}

@Injectable({
  providedIn: 'root',
})
export class BarangService {
  idCustom: any;
  value = new Subject<string>();

  constructor(private firestore: AngularFirestore, private fireauth: AngularFireAuth) {}

  calculateEan13(number: any) {
    let code = number;
    let sum: any = 0;

    // Calculate the checksum digit here.
    for (let i = code.length - 1; i >= 0; i--) {
      // This appears to be backwards but the 
      // EAN-13 checksum must be calculated
      if (i % 2) { // odd  
        sum += code[i] * 3;
      }
      else { // even
        sum += code[i] * 1;
      }
    }
    code = (10 - (sum % 10)) % 10;
    return number + code;
  }

  async getGoods(context: goodsContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      return this.firestore
        .collection(routes.goods())
        .doc(context.idx)
        .snapshotChanges()
        .pipe(
          map((action) => {
            const data = action.payload.data() as goodsContext;
            const id = action.payload.id;
            return { id, ...data };
          })
        );
    }
  }

  async readGoods() {
    return this.firestore.collection(routes.goods()).valueChanges() as Observable<goodsContext[]>;
  }

  getOnce() {
    return this.firestore.collection(routes.goods()).doc('--idx0--').get();
  }

  async createGoods(context: goodsContext) {
    const user = await this.fireauth.currentUser;

    if (user) {
      this.getOnce().subscribe(async (data) => {
        // check id increment value
        const value = data.get('bj004bcdmerc');
        console.log('length data value ' + value);
  
        if (value == undefined) {
          // Create id increment 0
          const setId = firebase.firestore().collection(routes.goods()).doc('--idx0--');
          const increment = firebase.firestore.FieldValue.increment(0);
          setId.set({ bj004bcdmerc: increment }, { merge: true });

          console.log('length data value ' + value);

          // Get length document
          this.getOnce()
            .pipe(debounceTime(0))
            .pipe(distinctUntilChanged())
            .subscribe(async (data) => {
              //const lengthData = data.size + 1; // will return the collection size
              // console.log('lengthData ' + lengthData);

              const sliceId = ('00000000000' + (data.data().bj004bcdmerc)).slice(-12);
              console.log('sliceId ' + sliceId);
              const idInc = this.calculateEan13(sliceId); // output: 000000000000;      
              this.idCustom = idInc;

              if (data) {
                // Get id value increment & implement into dm100merchid
                firebase
                  .firestore()
                  .collection('cl003merchan')
                  .doc()
                  .set({
                    dm100merchid: this.idCustom,
                    dm107barcoid: context.dm107barcoid,
                    dm101bymaker: context.dm101bymaker,
                    dm102bseries: context.dm102bseries,
                    dm103handler: context.dm103handler,
                    dm111fotobrg: context.dm111fotobrg,
                    dm104release: context.dm104release,
                    dm105catatan: context.dm105catatan,
                    dm109created: new Date()
                  });
              }
            });
        } else {
          // Create id increment > 0
          const setId = firebase.firestore().collection(routes.goods()).doc('--idx0--');
          const increment = firebase.firestore.FieldValue.increment(1);
          setId.set({ bj004bcdmerc: increment }, { merge: true });

          console.log('length data value ' + value);

          // Get length document
          this.getOnce()
            .pipe(debounceTime(0))
            .pipe(distinctUntilChanged())
            .subscribe(async (data) => {
              //const lengthData = data.size + 1; // will return the collection size
              // console.log('lengthData ' + lengthData);

              const sliceId = ('00000000000' + (data.data().bj004bcdmerc)).slice(-12);
              console.log('sliceId ' + sliceId);
              const idInc = this.calculateEan13(sliceId); // output: 000000000000;      
              this.idCustom = idInc;

              if (data) {
                // Get id value increment & implement into dm100merchid
                firebase
                  .firestore()
                  .collection('cl003merchan')
                  .doc()
                  .set({
                    dm100merchid: this.idCustom,
                    dm107barcoid: context.dm107barcoid,
                    dm101bymaker: context.dm101bymaker,
                    dm102bseries: context.dm102bseries,
                    dm103handler: context.dm103handler,
                    dm111fotobrg: context.dm111fotobrg,
                    dm104release: context.dm104release,
                    dm105catatan: context.dm105catatan,
                    dm109created: new Date()
                  });
              }
            });
        }
      })

      
    }
  }

  async updateGoods(context: goodsContext) {
    let user = await this.fireauth.currentUser;
    console.log('get id update barang ' + context.idx);

    if (user) {
      this.firestore
        .collection(routes.goods())
        .doc(context.idx)
        .update({
          dm107barcoid: context.dm107barcoid,
          dm101bymaker: context.dm101bymaker,
          dm102bseries: context.dm102bseries,
          dm103handler: context.dm103handler,
          dm111fotobrg: context.dm111fotobrg,
          dm105catatan: context.dm105catatan,
          dm109created: new Date()
        });
    }
  }

  async deleteGoods(context: goodsContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      console.log('context.idx ' + context.idx);
      this.firestore.collection(routes.goods()).doc(context.idx).delete();
    }
  }

  async createTransaction(context: transactionContext) {
    const user = await this.fireauth.currentUser;

    if (user) {
      this.getOnce().subscribe(async (data) => {
        // check id increment value
        const value = data.get('bj006bcdtran');
        console.log('length data value bj006bcdtran' + value);
  
        if (value == undefined) {
          // Create id increment 0
          const setId = firebase.firestore().collection(routes.goods()).doc('--idx0--');
          const increment = firebase.firestore.FieldValue.increment(0);
          setId.set({ bj006bcdtran: increment }, { merge: true });

          console.log('length data value bj006bcdtran' + value);

          // Get length document
          this.getOnce()
            .pipe(debounceTime(0))
            .pipe(distinctUntilChanged())
            .subscribe(async (data) => {
              //const lengthData = data.size + 1; // will return the collection size
              // console.log('lengthData ' + lengthData);

              const sliceId = ('00000000000' + (data.data().bj006bcdtran)).slice(-12);
              console.log('sliceId ' + sliceId);
              const idInc = this.calculateEan13(sliceId); // output: 000000000000;      

              if (data) {
                // Get id value increment & implement into dm100merchid
                firebase
                  .firestore()
                  .doc(`cl003merchan/${context.idx}`)
                  .collection('cl004belanja')
                  .doc()
                  .set({
                    dn300transid: idInc,
                    dn307expordt: context.dn307expordt,
                    dn308whatbox: context.dn308whatbox,
                    dn302qantity: context.dn302qantity,
                    dn303dealprc: context.dn303dealprc,
                    dn304paypric: context.dn304paypric,
                    dn305catatan: context.dn305catatan,
                    dn312alterby: context.dn312alterby,
                    dn309statudt: context.dn309statudt,
                    dn310sstatus: context.dn310sstatus,
                    // dn100merchid: context.dn100merchid,
                    dn107barcoid: context.dn107barcoid,
                    dn101bymaker: context.dn101bymaker,
                    dn102bseries: context.dn102bseries,
                    dn103handler: context.dn103handler,
                    dn111fotobrg: context.dn111fotobrg,
                    dn104release: context.dn104release,
                    dn000custoid: context.dn000custoid,
                    dn006fulname: context.dn006fulname,
                    dn200kemasid: context.dn200kemasid,
                  });
              }
            });
        } else {
          // Create id increment > 0
          const setId = firebase.firestore().collection(routes.goods()).doc('--idx0--');
          const increment = firebase.firestore.FieldValue.increment(1);
          setId.set({ bj006bcdtran: increment }, { merge: true });

          console.log('length data value ' + value);

          // Get length document
          this.getOnce()
            .pipe(debounceTime(0))
            .pipe(distinctUntilChanged())
            .subscribe(async (data) => {
              //const lengthData = data.size + 1; // will return the collection size
              // console.log('lengthData ' + lengthData);

              const sliceId = ('00000000000' + (data.data().bj006bcdtran)).slice(-12);
              console.log('sliceId ' + sliceId);
              const idInc = this.calculateEan13(sliceId); // output: 000000000000;      

              if (data) {
                // Get id value increment & implement into dm100merchid
                firebase
                  .firestore()
                  .doc(`cl003merchan/${context.idx}`)
                  .collection('cl004belanja')
                  .doc()
                  .set({
                    dn300transid: idInc,
                    dn307expordt: context.dn307expordt,
                    dn308whatbox: context.dn308whatbox,
                    dn302qantity: context.dn302qantity,
                    dn303dealprc: context.dn303dealprc,
                    dn304paypric: context.dn304paypric,
                    dn305catatan: context.dn305catatan,
                    dn312alterby: context.dn312alterby,
                    dn309statudt: context.dn309statudt,
                    dn310sstatus: context.dn310sstatus,
                    // dn100merchid: context.dn100merchid,
                    dn107barcoid: context.dn107barcoid,
                    dn101bymaker: context.dn101bymaker,
                    dn102bseries: context.dn102bseries,
                    dn103handler: context.dn103handler,
                    dn111fotobrg: context.dn111fotobrg,
                    dn104release: context.dn104release,
                    dn000custoid: context.dn000custoid,
                    dn006fulname: context.dn006fulname,
                    dn200kemasid: context.dn200kemasid,
                  });
              }
            });
        }
      })

    }

  }
}
