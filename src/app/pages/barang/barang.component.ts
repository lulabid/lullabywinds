import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from '@angular/fire/storage'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BarangService } from '@app/pages/barang/barang.service';
import { CustomerService } from '../../home/customer.service';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { NgxImageCompressService } from 'ngx-image-compress';
import { map, switchMap } from 'rxjs/operators';
import { async, from, Observable } from 'rxjs';
import { StartService } from '@app/transaction/start/start.service';

@Component({
  selector: 'app-barang',
  templateUrl: './barang.component.html',
  styleUrls: ['./barang.component.scss'],
  providers: [MessageService, DatePipe, Camera, NgxImageCompressService],
})
export class BarangComponent implements OnInit {
  isLoading = false;
  error: string | undefined;

  modalAdd: boolean = false;
  modalDetail: boolean = false;
  modalAddress: boolean = false;
  modalListAddress: boolean = false;
  modalPhoto: boolean = false;
  selectUser: boolean = false;

  goodsForm!: FormGroup;
  transactionForm!: FormGroup;
  idMerchant: any | undefined;
  listUser: any | undefined;
  idCustom: any | undefined;
  alterBy: any | undefined;
  readAdmin: any | undefined;

  idx: any | undefined;
  goods: any | undefined;
  address: any | undefined;
  dataCustomer: any | undefined;
  nameProvince: any | undefined;
  dataAddress: any | undefined;
  qty: any | undefined;
  readDataCustomer: any | undefined;
  currentUser: any | undefined;

  selectedCountry: any | undefined;
  releaseDate: any | undefined;
  searchValue: string | undefined;
  dataUnique: boolean = false;

  @ViewChild('addAvatar') addAvatar: any;
  noImage: any = 'assets/no-image.jpg';
  pictureFile: any = 'assets/no-image.jpg';
  pictureCamera: any = 'assets/no-image-camera.jpg';
  progressBar: any;
  isActive: boolean = false;

  currentBarcode: string = '';
  newBarcode: string = '';
  getMerchantId: any | undefined;
  idMerchantDoc: any | undefined;

  selectedData: any;

  _selectedColumns: any[];
  cols: any[];

  get f() {
    return this.goodsForm.controls;
  }

  get tf() {
    return this.transactionForm.controls;
  }

  submitted = false;

  province = [
    {
      id: 1,
      name: 'Aceh',
    },
    {
      id: 2,
      name: 'Sumatera Utara',
    },
    {
      id: 3,
      name: 'Sumatera Barat',
    },
    {
      id: 4,
      name: 'Riau',
    },
    {
      id: 5,
      name: 'Jambi',
    },
    {
      id: 6,
      name: 'Sumatera Selatan',
    },
    {
      id: 7,
      name: 'Bengkulu',
    },
    {
      id: 8,
      name: 'Lampung',
    },
    {
      id: 9,
      name: 'Kepulauan Bangka Belitung',
    },
    {
      id: 10,
      name: 'Kepulauan Riau',
    },
    {
      id: 11,
      name: 'Dki Jakarta',
    },
    {
      id: 12,
      name: 'Jawa Barat',
    },
    {
      id: 13,
      name: 'Jawa Tengah',
    },
    {
      id: 14,
      name: 'Di Yogyakarta',
    },
    {
      id: 15,
      name: 'Jawa Timur',
    },
    {
      id: 16,
      name: 'Banten',
    },
    {
      id: 17,
      name: 'Bali',
    },
    {
      id: 18,
      name: 'Nusa Tenggara Barat',
    },
    {
      id: 19,
      name: 'Nusa Tenggara Timur',
    },
    {
      id: 20,
      name: 'Kalimantan Barat',
    },
    {
      id: 21,
      name: 'Kalimantan Tengah',
    },
    {
      id: 22,
      name: 'Kalimantan Selatan',
    },
    {
      id: 23,
      name: 'Kalimantan Timur',
    },
    {
      id: 24,
      name: 'Kalimantan Utara',
    },
    {
      id: 25,
      name: 'Sulawesi Utara',
    },
    {
      id: 26,
      name: 'Sulawesi Tengah',
    },
    {
      id: 27,
      name: 'Sulawesi Selatan',
    },
    {
      id: 28,
      name: 'Sulawesi Tenggara',
    },
    {
      id: 29,
      name: 'Gorontalo',
    },
    {
      id: 30,
      name: 'Sulawesi Barat',
    },
    {
      id: 31,
      name: 'Maluku',
    },
    {
      id: 32,
      name: 'Maluku Utara',
    },
    {
      id: 33,
      name: 'Papua Barat',
    },
    {
      id: 34,
      name: 'Papua',
    },
  ];

  constructor(
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private fireauth: AngularFireAuth,
    private firestore: AngularFirestore,
    private brgService: BarangService,
    private cstService: CustomerService,
    private startService: StartService,
    private messageService: MessageService,
    private router: Router,
    private camera: Camera,
    private imageCompress: NgxImageCompressService,
    private afStorage: AngularFireStorage,
    private datePipe: DatePipe
  ) {
    this.Form();
  }

  ngOnInit() {
    this.getGoods();
    this.getCustomer();
    this.getAdmin();

    this.cols = [
      // { field: 'dm104release', header: 'Release Date' },
      { field: 'dm103handler', header: 'Baran' },
      { field: 'dm101bymaker', header: 'Mker' },
      { field: 'dm102bseries', header: 'Seri' },
      { field: 'dm107barcoid', header: 'BarcID' },
      { field: 'dm111fotobrg', header: 'Foto' },
      { field: 'dm105catatan', header: 'CatB' },
    ];

    this._selectedColumns = this.cols;
  }
  
  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col: any) => val.includes(col));
  }

  
  listTransaction(event: any) {
    this.idMerchant = event.dm103handler;
    this.router.navigate(['transaction', this.idMerchant]);

    console.log('get id customer for transaction and read ' + event.dk000custoid);
  }

  addModal() {
    this.currentBarcode = '';

    this.goodsForm.patchValue({
      dm107barcoid: "",
      dm101bymaker: "",
      dm102bseries: "",
      dm111fotobrg: "",
      dm104release: new Date('2000-01-01T00:00:00'),
      dm105catatan: "",
    });  

    this.dataUnique = true;
    this.modalAdd = true;
    this.renderer.addClass(document.body, 'modal-open');

    this.pictureFile = 'assets/no-image.jpg';   
  }

  createTrans(event: any) {
    console.log('event ' + JSON.stringify(event));
    this.modalAddress = true;
    this.renderer.addClass(document.body, 'modal-open');

    console.log('get id transaksi ' + event.id);
    this.idx = event.id;

    // const dateReleaseGoods = moment(new Date(event.dm104release?.seconds * 1000)).format('YYYY/MM/DD hh:mm:ss');

    this.transactionForm.patchValue({
      idx: this.idx,
      // dn100merchid: event.dm100merchid,
      dn302qantity: 1,
      dn101bymaker: event.dm101bymaker,
      dn102bseries: event.dm102bseries,
      dn103handler: event.dm103handler,
      dn104release: event.dm104release,
      dn105catatan: event.dm105catatan,
      dn303dealprc: 0,
      dn304paypric: -999999999999,
      dn307expordt: new Date('2000-01-01T00:00:00'),
      dn308whatbox: 0,
      dn111fotobrg: event.dm111fotobrg,
      dn107barcoid: event.dm107barcoid,
      dn000custoid: event.dm000custoid,
      dn006fulname: event.dm006fulname,
      dn200kemasid: "",
      dn305catatan: "",
      dn312alterby: this.readAdmin,
      dn309statudt: firebase.firestore.Timestamp.fromDate(new Date()),
      dn310sstatus: 1
    });
  }

  // cancel modal update barang
  removeModal() {
    this.modalDetail = false;
    this.modalAdd = false;
    this.renderer.removeClass(document.body, 'modal-open');

    this.goodsForm.reset();
  }

  // cancel modal add new barang
  removeModalAdd() {
    this.pictureFile = 'assets/no-image.jpg';
    this.modalAdd = false;
    this.renderer.removeClass(document.body, 'modal-open');  

    this.goodsForm.reset();
  }

  removeModalTrans() {
    this.modalAddress = false;
    this.selectedData = "";
    this.renderer.removeClass(document.body, 'modal-open');  

    this.transactionForm.reset();
  }

  listPhoto(event: any) {
    this.idMerchantDoc = event.id;
    this.router.navigate(['merchant-photo', this.idMerchantDoc]);
  }

  private Form() {
    this.goodsForm = this.formBuilder.group({
      idx: [''],
      dm100merchid: [''],
      dm107barcoid: ['', [Validators.minLength(3), Validators.maxLength(20)]],
      dm101bymaker: ['', [Validators.minLength(3), Validators.maxLength(20)]],
      dm102bseries: ['', [Validators.minLength(3), Validators.maxLength(20)]],
      dm103handler: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(300)]],
      dm111fotobrg: [''],
      dm104release: [''],
      dm105catatan: [''],
      dm109created: [''],
    });

    this.transactionForm = this.formBuilder.group({
      idx: [''],
      dn300transid: [''],
      dn307expordt: [''],
      dn308whatbox: [''],
      dn302qantity: [1, [Validators.required]],
      // dn100merchid: [''],
      dn101bymaker: [''],
      dn102bseries: [''],
      dn103handler: [''],
      dn104release: [''],
      dn107barcoid: [''],
      dn111fotobrg: [''],
      dn000custoid: [''],
      dn006fulname: [''],
      dn200kemasid: [''],
      dn303dealprc: [0, [Validators.required]],
      dn304paypric: [-999999999999, [Validators.required]],
      dn305catatan: [''],
      dn312alterby: [''],
      dn309statudt: [''],
      dn310sstatus: [''],
    });
  }

  async getAdmin() {
    let userEmail = await JSON.parse(localStorage.getItem('credentials'));

    // if (userEmail) {
    //   const getAdmin = this.firestore
    //   .collectionGroup(`cl000adminis`, (ref) => ref.where('di001dmymail', '==', userEmail.email)).valueChanges() as Observable<any[]>;

    //   getAdmin.subscribe(async (res) => {
    //     console.log('getAdmin ' + JSON.stringify(res))
    //   })
    // }

    (await this.cstService.getAdmin()).subscribe(async (res: any) => {
      console.log('res admin ' + JSON.stringify(res));
      if (res) {
        this.readAdmin = await res[0].di004admname;
      } else {
        this.readAdmin = [];
      }
    }, err => {
      console.log('res admin errorrrr ' + JSON.stringify(err));
    });
  }

  closed(event: any) {
    console.log('event ' + event.target.className);
    if(event.target.className === "modal fade show") {
      this.modalDetail = false;
      this.modalAdd = false;
      this.modalAddress = false;
      this.modalPhoto = false;
      this.selectedData = "";
      this.renderer.removeClass(document.body, 'modal-open');

      this.goodsForm.reset();
      this.transactionForm.reset();
    } 
  }

  changeFile(file: any) {
    // change image to base64
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  uploadPicture(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];

      this.changeFile(file).then((base64: string): any => {
        if (base64) {
          // console.log('event.target.files ' + event.target.files[0].name);
    
          this.imageCompress.compressFile(base64, 20, 100).then((result: any) => {
            // Only allow uploads of any image file that's less than 1MB
            const limit = 2 * 1024 * 1024;
            const resultSize = this.imageCompress.byteCount(result);

            if (resultSize < limit) { 
              const path = `merchandise/${file.name}`; // full path file
              // const ref = this.afStorage.ref(path);
              
              var uploadTask = firebase.storage().ref().child(path).put(file);

              uploadTask.on('state_changed',
                (snapshot) => {
                  // Observe state change events such as progress, pause, and resume
                  // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                  var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                  this.progressBar = progress;
                  switch (snapshot.state) {
                    case firebase.storage.TaskState.PAUSED: // or 'paused'
                      console.log('Upload is paused');
                      break;
                    case firebase.storage.TaskState.RUNNING: // or 'running'
                      console.log('Upload is running');
                      this.isActive = true;
                      break;
                  }
                },
                (error: any) => {
                  // A full list of error codes is available at
                  // https://firebase.google.com/docs/storage/web/handle-errors
                  switch (error.code) {
                    case 'storage/unauthorized':
                      // User doesn't have permission to access the object
                      console.log('User not have permission to update foto');
                      break;
                    case 'storage/canceled':
                      // User canceled the upload
                      break;
              
                    // ...
              
                    case 'storage/unknown':
                      // Unknown error occurred, inspect error.serverResponse
                      break;
                  }
                }, 
                () => {
                  // Handle successful uploads on complete
                  // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                  uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
                    console.log('File available at', downloadURL);
                    this.isActive = false;

                    if(this.progressBar == 100) {
                      setTimeout(() => {
                        this.pictureFile = downloadURL;

                        this.goodsForm.patchValue({
                          dm111fotobrg: downloadURL,
                        });
                        this.goodsForm.get('dm111fotobrg').updateValueAndValidity();
                      }, 0)
                     
                    }
                  });
                }
              );

              // let task = ref.put(file);
              // task.percentageChanges().subscribe(progress => {
              //   this.progressBar = progress;

              //   if (this.progressBar >= 100) {
              //     ref.put(file).then((downloadURL) => {
              //       downloadURL.ref.getDownloadURL().then(url => {
              //         console.log('File available at ' + url);
    
              //         this.pictureFile = url;
    
              //         this.goodsForm.patchValue({
              //           dm111fotobrg: url,
              //         });
              //         this.goodsForm.get('dm111fotobrg').updateValueAndValidity();
              //       });
              //     });
              //   }
              // });
            } else {
              this.messageService.add({
                key: 'error',
                severity: 'error',
                summary: 'Error Message',
                detail: 'Can not upload foto! File size < 1MB ' + resultSize,
              });
            }

            console.warn('Size in bytes is now: ', this.imageCompress.byteCount(result));
          });

          // console.log([base64], file.name);
        }
      });
    }
  }  

  async selectFile() {
    console.log('File clicked');

    this.addAvatar.nativeElement.click();
  }

  async selectCamera() {
    console.log('Camera clicked');

    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
    };

    this.camera.getPicture(options).then(
      (imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        this.pictureFile = base64Image;

        this.goodsForm.patchValue({
          dm111fotobrg: base64Image,
        });
        this.goodsForm.get('dm111fotobrg').updateValueAndValidity();
      },
      (err) => {
        // Handle error
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Error take photo ' + err,
        });
      }
    );
  }

  lulaPhoto(event: any) {
    if(event.dm111fotobrg == "" || event.dm111fotobrg == null) {
      this.pictureFile = 'assets/no-image.jpg';
    } else {
      this.pictureFile = event.dm111fotobrg;
    }

    this.modalPhoto = true;
    this.renderer.addClass(document.body, 'modal-open');

  }

  async lulaId(event: any) {
    console.log('event ' + JSON.stringify(event));

    const user = await this.fireauth.currentUser;
    console.log('user uid ' + JSON.stringify(user.uid));

    console.log('get id barang ' + event.id);
    this.idx = event.id;
    this.getMerchantId = event.dm100merchid;
    this.currentBarcode = event.dm107barcoid;

    this.modalDetail = true;
    this.renderer.addClass(document.body, 'modal-open');

    const dateReleaseGoods = moment(new Date(event.dm104release?.seconds * 1000)).format('yyyy-MM-DDThh:mm:ss');
    const dateCreateGoods = moment(new Date(event.dm109created?.seconds * 1000)).format('MM/DD/YYYY hh:mm:ss A');

    if(event.dm111fotobrg == "" || event.dm111fotobrg == null) {
      this.pictureFile = 'assets/no-image.jpg';
    } else {
      this.pictureFile = event.dm111fotobrg;
    }

    console.log('this.pictureFile ' + this.pictureFile);

    this.goodsForm.patchValue({
      idx: this.idx,
      dm100merchid: event.dm100merchid,
      dm107barcoid: event.dm107barcoid,
      dm101bymaker: event.dm101bymaker,
      dm102bseries: event.dm102bseries,
      dm103handler: event.dm103handler,
      dm111fotobrg: this.pictureFile,
      dm104release: dateReleaseGoods,
      dm105catatan: event.dm105catatan,
      dm109created: dateCreateGoods,
    });

    (await this.brgService.getGoods(this.goodsForm.value)).subscribe(async (data) => {
      if (data.dm107barcoid == this.currentBarcode) {
        this.dataUnique = true;
        console.log('Current Barcode ID');
      }
    });
    
  }

  async userId(event: any, item: any) {
    console.log('item customer ' + JSON.stringify(item.dk000custoid));

    this.currentUser = item.dk000custoid;

    this.transactionForm.patchValue({
      dn000custoid: item.dk000custoid,
      dn006fulname: item.dk006fulname,
      dn005catatan: item.dk005catatan,
    });
  }

   // Get barcode id value by input
   async checkBarcode(event: any) {
    if (event.target.value.length > 0) {
      this.dataUnique = false;
      const value = event.target.value;
      this.newBarcode = value;
    } else {
      this.dataUnique = true;
      this.newBarcode = '';
    }    
  }

  async getBarcode() {
    const checkBarcode = this.goodsForm.controls['dm107barcoid'].value;
    if (checkBarcode == this.currentBarcode ) {
      this.dataUnique = true;

      this.messageService.add({
        key: 'success',
        severity: 'success',
        summary: 'Success Message',
        detail: 'Barcode ID saat ini',
      });
    } else {
      const merchant = await firebase.firestore().collection('cl003merchan');
      merchant
        .where('dm107barcoid', '==', this.newBarcode)
        .get()
        .then(async (snapshot) => {
          this.isLoading = false;

          if (!snapshot.empty) {
            this.dataUnique = false;

            this.messageService.add({
              key: 'error',
              severity: 'error',
              summary: 'Error Message',
              detail: 'Barcode ID sudah ada',
            });
          } else {
            this.dataUnique = true;

            this.messageService.add({
              key: 'success',
              severity: 'success',
              summary: 'Success Message',
              detail: 'Barcode ID belum ada',
            });
          }
        }); 
    }
    
  } 

  async addGoods() {
    this.isLoading = true;
    this.submitted = true;

    const user = await this.fireauth.currentUser;

    if (this.goodsForm.invalid) {
      this.isLoading = false;

      if (this.f.dm107barcoid.errors) {
        if (this.f.dm107barcoid.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Barcode ID min 3 character!',
          });
        } else if (this.f.dm107barcoid.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Barcode ID min 20 character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Barcode ID can not empty!',
          });
        }
      }

      if (this.f.dm101bymaker.errors) {
        if (this.f.dm101bymaker.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Make by min 3 character!',
          });
        } else if (this.f.dm101bymaker.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Make by min 20 character!',
          });
        }
      }

      if (this.f.dm102bseries.errors) {
        if (this.f.dm102bseries.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Series name min 3 character!',
          });
        } else if (this.f.dm102bseries.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Series name max 20 character!',
          });
        }
      }

      if (this.f.dm103handler.errors) {
        if (this.f.dm103handler.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Nama barang min 3 character!',
          });
        } else if (this.f.dm103handler.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Nama barang max 300 character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Nama barang can not empty!',
          });
        }
      }

      return;
    } else {
      this.submitted = false;
    }

    // this.goodsForm.patchValue({
    //   dm112alterby: user.uid,
    // }); 

    if (this.dataUnique == true) {

      let release = this.goodsForm.controls['dm104release'].value;
      this.goodsForm.patchValue({
        dm104release: new Date(release)
      });

      this.brgService.createGoods(this.goodsForm.value).then((res: any) => {
        this.isLoading = false;
  
        if (res) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Can not create new goods!',
          });
        } else {
          this.messageService.add({
            key: 'success',
            severity: 'success',
            summary: 'Success Message',
            detail: 'Succesfully created a new goods',
          });
  
          this.removeModal();
        }
      });
    } else {
      this.isLoading = false;
  
      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: 'Barcode ID must be unique!',
      });
    }
    
  }

  async getGoods() {
    this.isLoading = true;

    (await this.brgService.readGoods()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        // Get all data customer
        this.goods = this.firestore
          .collection('cl003merchan', (ref) => ref.orderBy('dm100merchid', 'desc'))
          .snapshotChanges()
          .pipe(
            map((actions) =>
              actions.map((a) => {
                const data = a.payload.doc.data() as any;
                const id = a.payload.doc.id;
                const meta = a.payload.doc.metadata;

                // console.log('data ' + JSON.stringify(data));
                this.dataCustomer = data;

                return { id, meta, ...data };
              })
            )
          );
      } else {
        console.log('Can not reach the server');
      }
    });
  }

  async updateGoods() {
    this.isLoading = true;

    // const user = await this.fireauth.currentUser;

    // this.goodsForm.patchValue({
    //   dm112alterby: user.uid,
    // });

    if (this.goodsForm.invalid) {
      this.isLoading = false;

      if (this.f.dm107barcoid.errors) {
        if (this.f.dm107barcoid.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Barcode ID min 3 character!',
          });
        } else if (this.f.dm107barcoid.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Barcode ID min 20 character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Barcode ID can not empty!',
          });
        }
      }

      if (this.f.dm101bymaker.errors) {
        if (this.f.dm101bymaker.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Make by min 3 character!',
          });
        } else if (this.f.dm101bymaker.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Make by min 20 character!',
          });
        }
      }

      if (this.f.dm102bseries.errors) {
        if (this.f.dm102bseries.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Series name min 3 character!',
          });
        } else if (this.f.dm102bseries.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Series name max 20 character!',
          });
        }
      }

      if (this.f.dm103handler.errors) {
        if (this.f.dm103handler.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Nama barang min 3 character!',
          });
        } else if (this.f.dm103handler.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Nama barang max 300 character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Nama barang can not empty!',
          });
        }
      }

      return;
    } else {
      this.submitted = false;
    }

    if (this.dataUnique == true) {
      this.brgService.updateGoods(this.goodsForm.value).then((res: any) => {
        if (res) {
          console.log('Error update data merchant');
        } else {
          console.log('Your merchant success update');
  
          this.messageService.add({
            key: 'success',
            severity: 'success',
            summary: 'Success Message',
            detail: 'Succesfully update merchant',
          });

          this.removeModal();
        }
      });
    } else {
      this.isLoading = false;

      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: 'Barcode ID must be unique!',
      });
    }   
    
  }

  async deleteGoods() {
    this.isLoading = true;    

    const member = await firebase.firestore().collectionGroup('cl004belanja');
    member
      .where('dn100merchid', '==', this.getMerchantId)
      .get()
      .then(async (snapshot) => {
        this.isLoading = false;

        if (!snapshot.empty) {
          this.messageService.add({
            key: 'warning',
            severity: 'warning',
            summary: 'Warning Message',
            detail: 'Masih ada transaksi di keranjang belanja',
          });
        } else {
          this.messageService.add({
            key: 'success',
            severity: 'success',
            summary: 'Success Message',
            detail: 'Tidak ada transaksi di keranjang belanja',
          });

          // Create a reference to the file to delete
          let imageURL = this.pictureFile;
          var desertRef = firebase.storage().ref().child(imageURL.split(RegExp('(%2F)..*(%2F)'))[0].split("?")[0]);

          let getImage = decodeURIComponent(desertRef.name);
          var deleteRef = firebase.storage().ref().child(getImage);

          console.log('getImage ' + getImage);

          // Delete the file
          deleteRef.delete().then(() => {
            // File deleted successfully
            console.log('success delete foto');
          }).catch((error) => {
            // Uh-oh, an error occurred!
            console.log('Error delete foto ' + error.message);
          });

          this.goodsForm.patchValue({
            dm111fotobrg: "",
          });

          this.brgService.deleteGoods(this.goodsForm.value).then((res: any) => {
            if (res) {
              console.log('Error delete data merchant');
            } else {
              console.log('Your merchant success delete');
      
              this.messageService.add({
                key: 'success',
                severity: 'success',
                summary: 'Success Message',
                detail: 'Succesfully delete merchant',
              });
      
              this.removeModal();
            }
          });
        }
      });
   
  }

  seePhoto() {}

  async addTransaction() {
    this.isLoading = true;
    this.submitted = true;

    if (this.transactionForm.invalid) {
      this.isLoading = false;

      if (this.tf.dn302qantity.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Quantity can not empty!',
        });
      }

      if (this.tf.dn303dealprc.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Deal price can not empty!',
        });
      }

      if (this.tf.dn304paypric.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Payment price can not empty!',
        });
      }

      return;
    } else {
      this.submitted = false;
    }

    const qty = this.transactionForm.get('dn302qantity').value;
    console.log('qty ' + JSON.stringify(qty));

    const selectUser = this.transactionForm.get('dn000custoid').value;
    console.log('selectUser ' + JSON.stringify(selectUser));

    if (qty == '' || qty == 0) {
      this.isLoading = false;

      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: 'Quantity can not empty or value 0!',
      });
    } else if (selectUser == '' || selectUser == undefined) {
      this.isLoading = false;

      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: 'Please select customer!',
      });
    } else {
      this.brgService.createTransaction(this.transactionForm.value).then(res => {
        this.isLoading = false;

        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully created a new transaction',
        });
        this.removeModalTrans();
      }).catch(err => {
        console.log("Error create new transaction " + err.message);
      });
    }
  }

  async getCustomer() {
    this.isLoading = true;

    (await this.cstService.readCustomer()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        // Get all data customer
        this.readDataCustomer = this.firestore
          .collection('cl001members', (ref) => ref.orderBy('dk000custoid'))
          .snapshotChanges()
          .pipe(
            map((actions) =>
              actions.map((a) => {
                const data = a.payload.doc.data() as any;
                const id = a.payload.doc.id;
                const meta = a.payload.doc.metadata;

                // console.log('idtrans ' + JSON.stringify(id));
                return { id, meta, ...data };
              })
            )
          );
      } else {
        console.log('Can not reach the server');
      }
    });
  }
}
