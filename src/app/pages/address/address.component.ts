import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddressService } from './address.service';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
  providers: [MessageService, DatePipe],
})
export class AddressComponent implements OnInit {
  isLoading = false;
  error: string | undefined;
  submitted = false;

  modalAddAddress: boolean = false;
  modalAddress: boolean = false;
  modalListAddress: boolean = false;

  addressForm!: FormGroup;
  idCustom: any | undefined;

  idCustomer: any | undefined;
  idAddress: any | undefined;
  address: any | undefined;
  dataAddress: any | undefined;

  selectedCountry: any | undefined;
  selectedProvince: any | undefined;
  selectedGeo: any | undefined;
  searchValue: string | undefined;

  docCustomer: any | undefined;

  _selectedColumns: any[];
  cols: any[];

  get uf() {
    return this.addressForm.controls;
  }

  country = [
    {
      id: 81,
      name: 'Jepang',
    },
    {
      id: 62,
      name: 'Indonesia',
    },
  ];

  province = [
    {
      id: 1,
      name: 'Aceh',
    },
    {
      id: 2,
      name: 'Sumatera Utara',
    },
    {
      id: 3,
      name: 'Sumatera Barat',
    },
    {
      id: 4,
      name: 'Riau',
    },
    {
      id: 5,
      name: 'Jambi',
    },
    {
      id: 6,
      name: 'Sumatera Selatan',
    },
    {
      id: 7,
      name: 'Bengkulu',
    },
    {
      id: 8,
      name: 'Lampung',
    },
    {
      id: 9,
      name: 'Kepulauan Bangka Belitung',
    },
    {
      id: 10,
      name: 'Kepulauan Riau',
    },
    {
      id: 11,
      name: 'Dki Jakarta',
    },
    {
      id: 12,
      name: 'Jawa Barat',
    },
    {
      id: 13,
      name: 'Jawa Tengah',
    },
    {
      id: 14,
      name: 'Di Yogyakarta',
    },
    {
      id: 15,
      name: 'Jawa Timur',
    },
    {
      id: 16,
      name: 'Banten',
    },
    {
      id: 17,
      name: 'Bali',
    },
    {
      id: 18,
      name: 'Nusa Tenggara Barat',
    },
    {
      id: 19,
      name: 'Nusa Tenggara Timur',
    },
    {
      id: 20,
      name: 'Kalimantan Barat',
    },
    {
      id: 21,
      name: 'Kalimantan Tengah',
    },
    {
      id: 22,
      name: 'Kalimantan Selatan',
    },
    {
      id: 23,
      name: 'Kalimantan Timur',
    },
    {
      id: 24,
      name: 'Kalimantan Utara',
    },
    {
      id: 25,
      name: 'Sulawesi Utara',
    },
    {
      id: 26,
      name: 'Sulawesi Tengah',
    },
    {
      id: 27,
      name: 'Sulawesi Selatan',
    },
    {
      id: 28,
      name: 'Sulawesi Tenggara',
    },
    {
      id: 29,
      name: 'Gorontalo',
    },
    {
      id: 30,
      name: 'Sulawesi Barat',
    },
    {
      id: 31,
      name: 'Maluku',
    },
    {
      id: 32,
      name: 'Maluku Utara',
    },
    {
      id: 33,
      name: 'Papua Barat',
    },
    {
      id: 34,
      name: 'Papua',
    },
  ];

  constructor(
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private fireauth: AngularFireAuth,
    private firestore: AngularFirestore,
    private addressService: AddressService,
    private messageService: MessageService,
    private routeActive: ActivatedRoute
  ) {
    this.Form();
  }

  private Form() {
    this.addressForm = this.formBuilder.group({
      idCustomer: [''],
      idAddress: [''],
      ak009namalen: [
        '',
        [Validators.required, Validators.minLength(3), Validators.maxLength(50)],
      ],
      ak010country: [''],
      ak011postnum: [
        '',
        [Validators.minLength(3), Validators.maxLength(20)],
      ],
      ak012prefect: [''],
      ak013citynam: ['', [Validators.minLength(3), Validators.maxLength(50)]],
      ak014lainnya: ['', [Validators.minLength(3), Validators.maxLength(500)]],
      ak015telpnum: ['', [Validators.pattern('^[0-9]*$')]],
      ak016geopoin: ['', Validators.pattern('-?[0-9][0-9]*(\.[0-9]+)?,\s*-?[0-9][0-9]*(\.[0-9]+)?')],
    });
  }

   // Get customer name and compare if exist
   async checkName(event: any) {
    if (event.target.value.length > 0) {
      const lulaname = event.target.value;

      const member = await firebase.firestore().collectionGroup('cl007address');
      member
        .where('ak009namalen', '==', lulaname)
        .get()
        .then(async (snapshot) => {
          this.isLoading = false;

          if (!snapshot.empty) {
            this.messageService.add({
              key: 'warning',
              severity: 'warning',
              summary: 'Warning Message',
              detail: 'Nama sudah ada',
            });
          } else {
            this.messageService.add({
              key: 'success',
              severity: 'success',
              summary: 'Success Message',
              detail: 'Nama belum ada',
            });
          }
        });
    }
  }

  ngOnInit(): void {
    this.getAddress();

    // history data prev route
    this.routeActive.params.subscribe((params) => {
      this.idCustomer = params['id'];
    });

    this.getCustomer();

    this.cols = [
      { field: 'ak009namalen', header: 'Ptrima' },
      { field: 'ak011postnum', header: 'Kdpos' },
      { field: 'ak012prefect', header: 'Prov' },
      { field: 'ak013citynam', header: 'Kota' },
      { field: 'ak014lainnya', header: 'Jln' },
      { field: 'ak015telpnum', header: 'Telp' },
    ];

    this._selectedColumns = this.cols;
    // console.log('convert ean ' + this.generateEAN('000000000007'));
  }

  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col: any) => val.includes(col));
  }

  removeModal() {
    this.modalAddress = false;
    this.renderer.removeClass(document.body, 'modal-open');

    this.addressForm.reset();
  }

  removeModalCreate() {
    this.modalAddAddress = false;
    this.renderer.removeClass(document.body, 'modal-open');

    this.addressForm.reset();
  }

  closed(event: any) {
    if(event.target.className === "modal fade show") {
      this.modalAddress = false;
      this.modalAddAddress = false;
      this.renderer.removeClass(document.body, 'modal-open');
    } 
  }

  getCustomer() {
    this.firestore.collection('cl001members').doc(this.idCustomer).ref.get().then((doc => {
      if (doc.exists) {
        this.docCustomer = doc.data();
      } else {
        console.log('No Data Customer Ref');
      }
    }));
  }

  addAddressModal() {
    this.addressForm.patchValue({
      ak009namalen: "",
      ak010country: 62,
      ak011postnum: "",
      ak012prefect: 1,
      ak013citynam: "",
      ak014lainnya: "",
      ak015telpnum: "",
      ak016geopoin: "0,0"
    });

    this.selectedCountry = 62;
    this.selectedProvince = 1;
    this.selectedGeo = '0,0';

    this.modalAddAddress = true;
    this.renderer.addClass(document.body, 'modal-open');

    // history data prev route
    this.routeActive.params.subscribe((params) => {
      this.idCustomer = params['id'];
    });

    this.addressForm.patchValue({
      idCustomer: this.idCustomer,
    });

    // console.log('get id kustomer and create ' + event.id);
  }

  countryAddressChange(event: any) {
    console.log('country select ' + event.target.value);
    this.addressForm.patchValue({
      ak010country: parseInt(event.target.value),
    });
  }

  provinceAddressChange(event: any) {
    console.log('province select ' + event.target.value);
    this.addressForm.patchValue({
      ak012prefect: parseInt(event.target.value),
    });
  }

  lulaName(event: any) {
    console.log('event ' + JSON.stringify(event));

    console.log('get id address ' + event.id);
    this.idAddress = event.id;

    this.modalAddress = true;
    this.renderer.addClass(document.body, 'modal-open');

    this.addressForm.patchValue({
      idCustomer: this.idCustomer,
      idAddress: this.idAddress,
      ak009namalen: event.ak009namalen,
      ak010country: event.ak010country,
      ak011postnum: event.ak011postnum,
      ak012prefect: event.ak012prefect,
      ak013citynam: event.ak013citynam,
      ak014lainnya: event.ak014lainnya,
      ak015telpnum: event.ak015telpnum,
      ak016geopoin: event.ak016geopoin.latitude + ',' + event.ak016geopoin.longitude,
    });
  }

  async getAddress() {
    this.isLoading = true;

    (await this.addressService.readAddress()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        // Get all data address
        this.address = this.firestore
          .collection(`cl001members/${this.idCustomer}/cl007address`, (ref) => ref.orderBy('ak009namalen', 'asc'))
          .snapshotChanges()
          .pipe(
            map((actions) =>
              actions.map((a) => {
                const data = a.payload.doc.data() as any;
                const id = a.payload.doc.id;
                const meta = a.payload.doc.metadata;

                //console.log('data ' + JSON.stringify(data));
                this.dataAddress = data;

                return { id, meta, ...data };
              })
            )
          );
      } else {
        console.log('Can not reach the server');
      }
    });
  }

  async addAddress() {
    this.isLoading = true;
    this.submitted = true;

    const user = await this.fireauth.currentUser;  

    if (this.addressForm.invalid) {
      this.isLoading = false;

      if (this.uf.ak009namalen.errors) {
        if (this.uf.ak009namalen.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name min 3 character!',
          });
        } else if (this.uf.ak009namalen.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name max 50 character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name can not empty!',
          });
        }
      }

      if (this.uf.ak010country.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Country can not empty!',
        });
      }

      if (this.uf.ak011postnum.errors) {
        if (this.uf.ak011postnum.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Post code min 3 character!',
          });
        } else if (this.uf.ak011postnum.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Post code max 20 character!',
          });
        } 
      }

      if (this.uf.ak012prefect.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Province can not empty!',
        });
      }

      if (this.uf.ak013citynam.errors) {
        if (this.uf.ak013citynam.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City min 3 character!',
          });
        } else if (this.uf.ak013citynam.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City max 50 character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City can not empty!',
          });
        }
      }

      if (this.uf.ak014lainnya.errors) {
        if (this.uf.ak014lainnya.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Address min 3 character!',
          });
        } else if (this.uf.ak014lainnya.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Address max 500 character!',
          });
        }
      }

      if (this.uf.ak015telpnum.errors) {
        if (this.uf.ak015telpnum.errors.pattern) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Phone number only number!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Phone number can not empty!',
          });
        }
      }

      if (this.uf.ak016geopoin.errors) {
        if (this.uf.ak016geopoin.errors.pattern) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Geopoint format wrong! User format coordinate. Ex: latitude between -90 and 90 and longitude between -180 and 180',
          });
        }
      }

      return;
    } else {
      this.submitted = false;
    }

    this.addressService.createAddress(this.addressForm.value).then((res: any) => {
      this.isLoading = false;

      if (res) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not create new address!',
        });
      } else {
        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully created a new address',
        });

        this.removeModalCreate();
      }
    }).catch(error => {
      this.isLoading = false;

      console.log('errrorrr '  + error.message );
      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: error.message,
      });
    });;
  }

  async updateAddress() {
    this.isLoading = true;

    if (this.addressForm.invalid) {
      this.isLoading = false;

      if (this.uf.ak009namalen.errors) {
        if (this.uf.ak009namalen.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name min 3 character!',
          });
        } else if (this.uf.ak009namalen.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name max 50 character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name can not empty!',
          });
        }
      }

      if (this.uf.ak010country.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Country can not empty!',
        });
      }

      if (this.uf.ak011postnum.errors) {
        if (this.uf.ak011postnum.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Post code min 3 character!',
          });
        } else if (this.uf.ak011postnum.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Post code max 20 character!',
          });
        } 
      }

      if (this.uf.ak012prefect.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Province can not empty!',
        });
      }

      if (this.uf.ak013citynam.errors) {
        if (this.uf.ak013citynam.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City min 3 character!',
          });
        } else if (this.uf.ak013citynam.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City max 50 character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City can not empty!',
          });
        }
      }

      if (this.uf.ak014lainnya.errors) {
        if (this.uf.ak014lainnya.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Address min 3 character!',
          });
        } else if (this.uf.ak014lainnya.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Address max 500 character!',
          });
        }
      }

      if (this.uf.ak015telpnum.errors) {
        if (this.uf.ak015telpnum.errors.pattern) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Phone number only number!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Phone number can not empty!',
          });
        }
      }

      if (this.uf.ak016geopoin.errors) {
        if (this.uf.ak016geopoin.errors.pattern) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Geopoint format wrong! User format coordinate. Ex: latitude between -90 and 90 and longitude between -180 and 180',
          });
        }
      }

      return;
    } else {
      this.submitted = false;
    }

    this.addressService.updateAddress(this.addressForm.value).then((res: any) => {
      if (res) {
        this.isLoading = false;
        console.log('Error update data address');
      } else {
        this.isLoading = false;
        console.log('Your address success update');

        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully update address',
        });

        this.removeModal();
      }
    }).catch(error => {
      this.isLoading = false;

      console.log('errrorrr '  + error.message );
      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: error.message,
      });
    });;
  }

  async deleteAddress() {
    this.isLoading = true;

    this.addressService.deleteAddress(this.addressForm.value).then((res: any) => {
      if (res) {
        this.isLoading = false;
        console.log('Error delete data address customer');
      } else {
        this.isLoading = false;
        console.log('Your address customer success delete');

        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully delete address customer',
        });

        this.removeModal();
      }
    });
  }
}
