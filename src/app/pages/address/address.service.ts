import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';

const routes = {
  address: () => `/cl007address`,
};

export interface dataContext {
  // The quote's category: 'dev', 'explicit'...
  category: string;
}

export interface addressContext {
  idCustomer: any;
  idAddress: any;
  ak009namalen: string;
  ak010country: number;
  ak011postnum: string;
  ak012prefect: number;
  ak013citynam: string;
  ak014lainnya: string;
  ak015telpnum: string;
  ak016geopoin: any;
}

@Injectable({
  providedIn: 'root',
})
export class AddressService {
  idCustom: any;
  value = new Subject<string>();

  constructor(private httpClient: HttpClient, private firestore: AngularFirestore, private fireauth: AngularFireAuth) {}

  async readAddress() {
    return this.firestore.collectionGroup('cl007address').valueChanges() as Observable<addressContext[]>;
  }

  async createAddress(context: addressContext) {
    const user = await this.fireauth.currentUser;

    const valGeo = context.ak016geopoin.split(',');
    console.log('valGeo ' + valGeo);
    const latitude = parseFloat(valGeo[0]);
    const longitude = parseFloat(valGeo[1]);
    console.log('latitude ' + latitude);
    console.log('longitude ' + longitude);

    console.log('get id create address ' + context.idCustomer);

    if (user) {
      firebase
        .firestore()
        .doc(`cl001members/${context.idCustomer}`)
        .collection('cl007address')
        .add({
          ak009namalen: context.ak009namalen,
          ak010country: context.ak010country,
          ak011postnum: context.ak011postnum,
          ak012prefect: context.ak012prefect,
          ak013citynam: context.ak013citynam,
          ak014lainnya: context.ak014lainnya,
          ak015telpnum: context.ak015telpnum,
          ak016geopoin: new firebase.firestore.GeoPoint(latitude, longitude),
        });
    }
  }

  async updateAddress(context: addressContext) {
    let user = await this.fireauth.currentUser;

    const valGeo = context.ak016geopoin.split(',');
    console.log('valGeo ' + valGeo);
    const latitude = parseFloat(valGeo[0]);
    const longitude = parseFloat(valGeo[1]);
    console.log('latitude ' + latitude);
    console.log('longitude ' + longitude);

    console.log('get id update customer ' + context.idCustomer);
    console.log('get id update address ' + context.idAddress);

    if (user) {
      this.firestore
        .doc(`cl001members/${context.idCustomer}`)
        .collection('cl007address')
        .doc(context.idAddress)
        .update({
          ak009namalen: context.ak009namalen,
          ak010country: context.ak010country,
          ak011postnum: context.ak011postnum,
          ak012prefect: context.ak012prefect,
          ak013citynam: context.ak013citynam,
          ak014lainnya: context.ak014lainnya,
          ak015telpnum: context.ak015telpnum,
          ak016geopoin: new firebase.firestore.GeoPoint(latitude, longitude),
        });
    }
  }

  async deleteAddress(context: addressContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      this.firestore
        .doc(`cl001members/${context.idCustomer}`)
        .collection('cl007address')
        .doc(context.idAddress)
        .delete();
    }
  }
}
