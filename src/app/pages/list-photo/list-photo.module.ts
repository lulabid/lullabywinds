import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxBarcode6Module } from 'ngx-barcode6';
import { TranslateModule } from '@ngx-translate/core';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgApexchartsModule } from 'ng-apexcharts';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { PaginatorModule } from 'primeng/paginator';
import { TableModule } from 'primeng/table';
import { AutoCompleteModule } from 'primeng/autocomplete';

import { SharedModule } from '@shared';
import { ListPhotoRoutingModule } from './list-photo-routing.module';
import { ListPhotoComponent } from './list-photo.component';



@NgModule({
  declarations: [ListPhotoComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    FormsModule,
    NgbModule,
    SharedModule,
    PerfectScrollbarModule,
    NgxBarcode6Module,
    NgApexchartsModule,
    ToastModule,
    DialogModule,
    PaginatorModule,
    TableModule,
    AutoCompleteModule,
    ListPhotoRoutingModule
  ]
})
export class ListPhotoModule { }
