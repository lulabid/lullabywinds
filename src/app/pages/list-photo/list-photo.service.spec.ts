import { TestBed } from '@angular/core/testing';

import { ListPhotoService } from './list-photo.service';

describe('ListPhotoService', () => {
  let service: ListPhotoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListPhotoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
