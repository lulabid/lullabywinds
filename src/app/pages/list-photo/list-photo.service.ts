import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';

const routes = {
  address: () => `/cl008mrchimg`,
};

export interface dataContext {
  // The quote's category: 'dev', 'explicit'...
  category: string;
}

export interface photoContext {
  idMerchant: any;
  idPhoto: any;
  hm111fotobrg: string;
  hm112caption: any;
}

@Injectable({
  providedIn: 'root'
})
export class ListPhotoService {
  idCustom: any;
  value = new Subject<string>();

  constructor(private httpClient: HttpClient, private firestore: AngularFirestore, private fireauth: AngularFireAuth) {}

  async readPhoto() {
    return this.firestore.collectionGroup('cl008mrchimg').valueChanges() as Observable<photoContext[]>;
  }

  async createPhoto(context: photoContext) {
    const user = await this.fireauth.currentUser;

    console.log('get id create merchant ' + context.idMerchant);

    if (user) {
      firebase
        .firestore()
        .doc(`cl003merchan/${context.idMerchant}`)
        .collection('cl008mrchimg')
        .add({
          hm111fotobrg: context.hm111fotobrg,
          hm112caption: context.hm112caption,
        });
    }
  }

  async updatePhoto(context: photoContext) {
    let user = await this.fireauth.currentUser;

    console.log('get id update merchant ' + context.idMerchant);
    console.log('get id update address ' + context.idPhoto);

    if (user) {
      this.firestore
        .doc(`cl003merchan/${context.idMerchant}`)
        .collection('cl008mrchimg')
        .doc(context.idPhoto)
        .update({
          hm111fotobrg: context.hm111fotobrg,
          hm112caption: context.hm112caption,
        });
    }
  }

  async deletePhoto(context: photoContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      this.firestore
        .doc(`cl003merchan/${context.idMerchant}`)
        .collection('cl008mrchimg')
        .doc(context.idPhoto)
        .delete();
    }
  }
}
