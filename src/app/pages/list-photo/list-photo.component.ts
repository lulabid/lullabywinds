import { Component, OnInit, Renderer2, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ListPhotoService } from './list-photo.service';
import { MessageService } from 'primeng/api';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { NgxImageCompressService } from 'ngx-image-compress';

@Component({
  selector: 'app-list-photo',
  templateUrl: './list-photo.component.html',
  styleUrls: ['./list-photo.component.scss'],
  providers: [MessageService, Camera, NgxImageCompressService],
})
export class ListPhotoComponent implements OnInit {
  isLoading = false;
  error: string | undefined;
  submitted = false;

  modalAddPhoto: boolean = false;
  modalPhoto: boolean = false;
  modalDetailPhoto: boolean = false;
  modalListPhoto: boolean = false;

  photoForm!: FormGroup;
  idCustom: any | undefined;

  idx: any | undefined;
  idMerchant: any | undefined;
  idPhoto: any | undefined;
  photo: any | undefined;
  dataPhoto: any | undefined;

  searchValue: string | undefined;
  docMerchant: any | undefined;

  @ViewChild('addAvatar') addAvatar: any;
  pictureFile: any = 'assets/no-image.jpg';
  pictureCamera: any = 'assets/no-image-camera.jpg';
  progressBar: any;
  isActive: boolean = false;

  get uf() {
    return this.photoForm.controls;
  }

  constructor(
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private fireauth: AngularFireAuth,
    private firestore: AngularFirestore,
    private photoService: ListPhotoService,
    private messageService: MessageService,
    private routeActive: ActivatedRoute,
    private camera: Camera,
    private imageCompress: NgxImageCompressService,
  ) {
    this.Form();
  }

  private Form() {
    this.photoForm = this.formBuilder.group({
      idMerchant: [''],
      idPhoto: [''],
      hm111fotobrg: ['', [Validators.required]],
      hm112caption: [
        '',
        [Validators.minLength(3), Validators.maxLength(50), Validators.pattern(/^[\w\s]+$/)],
      ],
    });
  }

  changeFile(file: any) {
    // change image to base64
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  uploadPicture(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];

      this.changeFile(file).then((base64: string): any => {
        if (base64) {
          // console.log('event.target.files ' + event.target.files[0].name);
    
          this.imageCompress.compressFile(base64, 20, 100).then((result: any) => {
            // Only allow uploads of any image file that's less than 1MB
            const limit = 2 * 1024 * 1024;
            const resultSize = this.imageCompress.byteCount(result);

            if (resultSize < limit) { 
              const path = `list-merchandise/${file.name}`; // full path file
              // const ref = this.afStorage.ref(path);
              
              var uploadTask = firebase.storage().ref().child(path).put(file);

              uploadTask.on('state_changed',
                (snapshot) => {
                  // Observe state change events such as progress, pause, and resume
                  // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                  var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                  this.progressBar = progress;
                  switch (snapshot.state) {
                    case firebase.storage.TaskState.PAUSED: // or 'paused'
                      console.log('Upload is paused');
                      break;
                    case firebase.storage.TaskState.RUNNING: // or 'running'
                      console.log('Upload is running');
                      this.isActive = true;
                      break;
                  }
                },
                (error: any) => {
                  // A full list of error codes is available at
                  // https://firebase.google.com/docs/storage/web/handle-errors
                  switch (error.code) {
                    case 'storage/unauthorized':
                      // User doesn't have permission to access the object
                      console.log('User not have permission to update foto');
                      break;
                    case 'storage/canceled':
                      // User canceled the upload
                      break;
              
                    // ...
              
                    case 'storage/unknown':
                      // Unknown error occurred, inspect error.serverResponse
                      break;
                  }
                }, 
                () => {
                  // Handle successful uploads on complete
                  // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                  uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
                    console.log('File available at', downloadURL);
                    this.isActive = false;

                    if(this.progressBar == 100) {
                      setTimeout(() => {
                        this.pictureFile = downloadURL;

                        this.photoForm.patchValue({
                          hm111fotobrg: downloadURL,
                        });
                        this.photoForm.get('hm111fotobrg').updateValueAndValidity();
                      }, 0)
                     
                    }
                  });
                }
              );
            } else {
              this.messageService.add({
                key: 'error',
                severity: 'error',
                summary: 'Error Message',
                detail: 'Can not upload foto! File size < 1MB ' + resultSize,
              });
            }

            console.warn('Size in bytes is now: ', this.imageCompress.byteCount(result));
          });

          // console.log([base64], file.name);
        }
      });
    }
  }  

  async selectFile() {
    console.log('File clicked');

    this.addAvatar.nativeElement.click();
  }

  async selectCamera() {
    console.log('Camera clicked');

    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
    };

    this.camera.getPicture(options).then(
      (imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        this.pictureFile = base64Image;

        this.photoForm.patchValue({
          hm111fotobrg: base64Image,
        });
        this.photoForm.get('hm111fotobrg').updateValueAndValidity();
      },
      (err) => {
        // Handle error
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Error take photo ' + err,
        });
      }
    );
  }

  lulaDetailPhoto(event: any) {
    if(event.hm111fotobrg == "" || event.hm111fotobrg == null) {
      this.pictureFile = 'assets/no-image.jpg';
    } else {
      this.pictureFile = event.hm111fotobrg;
    }

    this.modalDetailPhoto = true;
    this.renderer.addClass(document.body, 'modal-open');
  }

  lulaPhoto(event: any) {
    if(event.hm111fotobrg == "" || event.hm111fotobrg == null) {
      this.pictureFile = 'assets/no-image.jpg';
    } else {
      this.pictureFile = event.hm111fotobrg;
    }

    this.modalPhoto = true;
    this.renderer.addClass(document.body, 'modal-open');

    this.photoForm.patchValue({
      idMerchant: this.idMerchant,
      idPhoto: event.id,
      hm111fotobrg: event.hm111fotobrg,
      hm112caption: event.hm112caption,
    });

  }

   // Get customer name and compare if exist
   async checkName(event: any) {
    if (event.target.value.length > 0) {
      const lulaname = event.target.value;

      const member = await firebase.firestore().collectionGroup('cl008mrchimg');
      member
        .where('ak009namalen', '==', lulaname)
        .get()
        .then(async (snapshot) => {
          this.isLoading = false;

          if (!snapshot.empty) {
            this.messageService.add({
              key: 'warning',
              severity: 'warning',
              summary: 'Warning Message',
              detail: 'Nama sudah ada',
            });
          } else {
            this.messageService.add({
              key: 'success',
              severity: 'success',
              summary: 'Success Message',
              detail: 'Nama belum ada',
            });
          }
        });
    }
  }

  ngOnInit(): void {
    this.getPhoto();

    // history data prev route
    this.routeActive.params.subscribe((params) => {
      this.idMerchant = params['id'];
    });

    this.getCustomer();
  }

  removeModal() {
    this.modalPhoto = false;
    this.renderer.removeClass(document.body, 'modal-open');

    this.photoForm.reset();
  }

  removeModalCreate() {
    this.pictureFile = 'assets/no-image.jpg';
    this.modalAddPhoto = false;
    this.renderer.removeClass(document.body, 'modal-open');

    this.photoForm.reset();
  }

  closed(event: any) {
    if(event.target.className === "modal fade show") {
      this.modalPhoto = false;
      this.modalDetailPhoto = false;
      this.modalAddPhoto = false;
      this.renderer.removeClass(document.body, 'modal-open');
    } 
  }

  getCustomer() {
    this.firestore.collection('cl003merchan').doc(this.idMerchant).ref.get().then((doc => {
      if (doc.exists) {
        this.docMerchant = doc.data();
      } else {
        console.log('No Data Customer Ref');
      }
    }));
  }

  addPhotoModal() {
    this.pictureFile = 'assets/no-image.jpg';
    this.modalAddPhoto = true;
    this.renderer.addClass(document.body, 'modal-open');

    // history data prev route
    this.routeActive.params.subscribe((params) => {
      this.idMerchant = params['id'];
    });

    this.photoForm.patchValue({
      idMerchant: this.idMerchant,
      hm112caption: "",
    });

    // console.log('get id kustomer and create ' + event.id);
  }

  lulaName(event: any) {
    console.log('event ' + JSON.stringify(event));

    console.log('get id photo ' + event.id);
    this.idPhoto = event.id;

    this.modalPhoto = true;
    this.renderer.addClass(document.body, 'modal-open');

    this.photoForm.patchValue({
      idMerchant: this.idMerchant,
      idPhoto: this.idPhoto,
      hm111fotobrg: event.hm111fotobrg,
      hm112caption: event.hm112caption,
    });
  }

  async getPhoto() {
    this.isLoading = true;

    (await this.photoService.readPhoto()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        // Get all data photo
        this.photo = this.firestore
          .collection(`cl003merchan/${this.idMerchant}/cl008mrchimg`)
          .snapshotChanges()
          .pipe(
            map((actions) =>
              actions.map((a) => {
                const data = a.payload.doc.data() as any;
                const id = a.payload.doc.id;
                const meta = a.payload.doc.metadata;

                //console.log('data ' + JSON.stringify(data));
                this.dataPhoto = data;

                return { id, meta, ...data };
              })
            )
          );
      } else {
        console.log('Can not reach the server');
      }
    });
  }

  async addData() {
    this.isLoading = true;
    this.submitted = true;

    const user = await this.fireauth.currentUser;  

    if (this.photoForm.invalid) {
      this.isLoading = false;

      if (this.uf.hm111fotobrg.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Photo can not empty!',
        });
      }

      if (this.uf.hm112caption.errors) {
        if (this.uf.hm112caption.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Caption min 3 character!',
          });
        } else if (this.uf.hm112caption.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Caption max 50 character!',
          });
        }
      }

      return;
    } else {
      this.submitted = false;
    }

    this.photoService.createPhoto(this.photoForm.value).then((res: any) => {
      this.isLoading = false;

      if (res) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not create new photo!',
        });
      } else {
        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully created a new photo',
        });

        this.removeModalCreate();
      }
    }).catch(error => {
      this.isLoading = false;

      console.log('errrorrr '  + error.message );
      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: error.message,
      });
    });;
  }

  async updateData() {
    this.isLoading = true;

    if (this.photoForm.invalid) {
      this.isLoading = false;

      if (this.uf.hm111fotobrg.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Photo can not empty!',
        });
      }

      if (this.uf.hm112caption.errors) {
        if (this.uf.hm112caption.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Caption min 3 character!',
          });
        } else if (this.uf.hm112caption.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Caption max 50 character!',
          });
        }
      }

      return;
    } else {
      this.submitted = false;
    }

    this.photoService.updatePhoto(this.photoForm.value).then((res: any) => {
      if (res) {
        this.isLoading = false;
        console.log('Error update photo merchant');
      } else {
        this.isLoading = false;
        console.log('Photo merchant success update');

        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully update photo merchant',
        });

        this.removeModal();
      }
    }).catch(error => {
      this.isLoading = false;

      console.log('errrorrr '  + error.message );
      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: error.message,
      });
    });;
  }

  async deleteData() {
    this.isLoading = true;

     // Create a reference to the file to delete
     let imageURL = this.pictureFile;
     var desertRef = firebase.storage().ref().child(imageURL.split(RegExp('(%2F)..*(%2F)'))[0].split("?")[0]);

     let getImage = decodeURIComponent(desertRef.name);
     var deleteRef = firebase.storage().ref().child(getImage);

     console.log('getImage ' + getImage);

     // Delete the file
     deleteRef.delete().then(() => {
       // File deleted successfully
       console.log('success delete foto');
     }).catch((error) => {
       // Uh-oh, an error occurred!
       console.log('Error delete foto ' + error.message);
     });

    this.photoService.deletePhoto(this.photoForm.value).then((res: any) => {
      if (res) {
        this.isLoading = false;
        console.log('Error delete photo merchant');
      } else {
        this.isLoading = false;
        console.log('Photo merchant success delete');

        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully delete photo merchant',
        });

        this.removeModal();
      }
    });
  }

}
