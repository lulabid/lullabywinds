import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxBarcode6Module } from 'ngx-barcode6';
import { TranslateModule } from '@ngx-translate/core';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgApexchartsModule } from 'ng-apexcharts';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';

import { SharedModule } from '@shared';

import { PrintInvoiceRoutingModule } from './print-invoice-routing.module';
import { PrintInvoiceComponent } from './print-invoice.component';

@NgModule({
  declarations: [PrintInvoiceComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    FormsModule,
    NgbModule,
    SharedModule,
    PerfectScrollbarModule,
    NgxBarcode6Module,
    NgApexchartsModule,
    ToastModule,
    DialogModule,
    PrintInvoiceRoutingModule,
  ],
})
export class PrintInvoiceModule {}
