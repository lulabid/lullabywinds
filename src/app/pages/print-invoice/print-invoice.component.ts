import { Component, OnInit, Renderer2 } from '@angular/core';
import { SetokService } from '../../transaction/setok/setok.service';
import { LunasService } from '../../shipment/lunas/lunas.service';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { CustomerService } from '@app/home/customer.service';

@Component({
  selector: 'app-print-invoice',
  templateUrl: './print-invoice.component.html',
  styleUrls: ['./print-invoice.component.scss'],
  providers: [MessageService],
})
export class PrintInvoiceComponent implements OnInit {
  isLoading = false;
  error: string | undefined;

  idCustom: any | undefined;
  readData: any | undefined;
  readDataCustomer: any | undefined;
  dataTransaction: any | undefined;
  dataLength: any | undefined;

  textStatus: any | undefined;
  textKurir: any | undefined;
  quantity: any | undefined;

  province = [
    {
      id: 1,
      name: 'Aceh',
    },
    {
      id: 2,
      name: 'Sumatera Utara',
    },
    {
      id: 3,
      name: 'Sumatera Barat',
    },
    {
      id: 4,
      name: 'Riau',
    },
    {
      id: 5,
      name: 'Jambi',
    },
    {
      id: 6,
      name: 'Sumatera Selatan',
    },
    {
      id: 7,
      name: 'Bengkulu',
    },
    {
      id: 8,
      name: 'Lampung',
    },
    {
      id: 9,
      name: 'Kepulauan Bangka Belitung',
    },
    {
      id: 10,
      name: 'Kepulauan Riau',
    },
    {
      id: 11,
      name: 'Dki Jakarta',
    },
    {
      id: 12,
      name: 'Jawa Barat',
    },
    {
      id: 13,
      name: 'Jawa Tengah',
    },
    {
      id: 14,
      name: 'Di Yogyakarta',
    },
    {
      id: 15,
      name: 'Jawa Timur',
    },
    {
      id: 16,
      name: 'Banten',
    },
    {
      id: 17,
      name: 'Bali',
    },
    {
      id: 18,
      name: 'Nusa Tenggara Barat',
    },
    {
      id: 19,
      name: 'Nusa Tenggara Timur',
    },
    {
      id: 20,
      name: 'Kalimantan Barat',
    },
    {
      id: 21,
      name: 'Kalimantan Tengah',
    },
    {
      id: 22,
      name: 'Kalimantan Selatan',
    },
    {
      id: 23,
      name: 'Kalimantan Timur',
    },
    {
      id: 24,
      name: 'Kalimantan Utara',
    },
    {
      id: 25,
      name: 'Sulawesi Utara',
    },
    {
      id: 26,
      name: 'Sulawesi Tengah',
    },
    {
      id: 27,
      name: 'Sulawesi Selatan',
    },
    {
      id: 28,
      name: 'Sulawesi Tenggara',
    },
    {
      id: 29,
      name: 'Gorontalo',
    },
    {
      id: 30,
      name: 'Sulawesi Barat',
    },
    {
      id: 31,
      name: 'Maluku',
    },
    {
      id: 32,
      name: 'Maluku Utara',
    },
    {
      id: 33,
      name: 'Papua Barat',
    },
    {
      id: 34,
      name: 'Papua',
    },
  ];

  constructor(
    private renderer: Renderer2,
    private lunasService: LunasService,
    private selesaiService: SetokService,
    private routeActive: ActivatedRoute,
    private messageService: MessageService,
    private customerService: CustomerService,
  ) {}

  ngOnInit() {
    this.getTransaction();
  }

  printInvoice() {
    window.print();
  }

  async getTransaction() {
    this.isLoading = true;

    // get param id
    this.routeActive.params.subscribe((params) => {
      this.idCustom = params['id'];
    });

    (await this.lunasService.getShip()).subscribe(async (res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          const filterStatus = res.filter((data: any) => data.hi200kemasid == this.idCustom);

          let matches = new Set();
          const filterKemas = filterStatus.filter((elem: any) => {
            const { hi200kemasid } = elem;
            if (matches.has(hi200kemasid)) {
              return false;
            } else {
              matches.add(hi200kemasid);
              return true;
            }
          });

          this.readData = filterKemas;

          this.readData.forEach((value: any) => {
            if (value.hi210statuss == 5) {
              this.textStatus = "Sudah Lunas";
            } else if (value.hi210statuss == 6) {
              this.textStatus = "Sudah Dikemas";
            } else if (value.hi210statuss == 7) {
              this.textStatus = "Sudah Dikirim";
            } else if (value.hi210statuss == 8) {
              this.textStatus = "Sudah Sampai";
            } 

            if (value.hi205viakuri == 0) {
              this.textKurir = "Default";
            } else if (value.hi205viakuri == 1) {
              this.textKurir = "Ambil Sendiri";
            } else if (value.hi205viakuri == 2) {
              this.textKurir = "Anter Aja";
            } else if (value.hi205viakuri == 3) {
              this.textKurir = "GO-SEND";
            } else if (value.hi205viakuri == 4) {
              this.textKurir = "GOJEK";
            } else if (value.hi205viakuri == 5) {
              this.textKurir = "GRAB";
            } else if (value.hi205viakuri == 6) {
              this.textKurir = "JNE";
            } else if (value.hi205viakuri == 7) {
              this.textKurir = "JNE JTR";
            } else if (value.hi205viakuri == 8) {
              this.textKurir = "JNE OK";
            } else if (value.hi205viakuri == 9) {
              this.textKurir = "JNE YES";
            } else if (value.hi205viakuri == 10) {
              this.textKurir = "J&T";
            } else if (value.hi205viakuri == 11) {
              this.textKurir = "Kurir";
            } else if (value.hi205viakuri == 12) {
              this.textKurir = "MEX";
            } else if (value.hi205viakuri == 13) {
              this.textKurir = "POS EXPRESS";
            } else if (value.hi205viakuri == 14) {
              this.textKurir = "POS KILAT KHUSUS";
            } else if (value.hi205viakuri == 15) {
              this.textKurir = "REX";
            } else if (value.hi205viakuri == 16) {
              this.textKurir = "Si Cepat";
            } else if (value.hi205viakuri == 17) {
              this.textKurir = "TIKI";
            } else if (value.hi205viakuri == 18) {
              this.textKurir = "TIKI ECO";
            } else if (value.hi205viakuri == 19) {
              this.textKurir = "TIKI ONS";
            } else if (value.hi205viakuri == 20) {
              this.textKurir = "TIKI TCR";
            }
          });

          console.log('this.readData ' + JSON.stringify(this.readData));

          // const filterStatusByCustomer = res.filter((data: any) => data.hi000custoid == '0000000000086');

          // let matchesData = new Set();
          // const filterKemasCustomer = filterStatusByCustomer.filter((elem: any) => {
          //   const { hi200kemasid } = elem;
          //   if (matchesData.has(hi200kemasid)) {
          //     return false;
          //   } else {
          //     matchesData.add(hi200kemasid);
          //     return true;
          //   }
          // });

          // (await this.customerService.readCustomer()).subscribe((res: any) => {
          //   this.isLoading = false;
      
          //   if (res) {
          //    const filterCustomer = res.filter((data: any) => data.dk000custoid == '0000000000086');
          //     this.readDataCustomer = filterCustomer[0];

          //     console.log('readDataCustomer ' + JSON.stringify(this.readDataCustomer));
          //   }
          // })

          // window.print();
        } else {
          this.readData = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach the server!',
        });
      }
    });

    (await this.customerService.readCustomer()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          const filterCustomer = res.filter((data: any) => data.dk000custoid == '0000000000086');
          this.readDataCustomer = filterCustomer[0];
  
          console.log('readDataCustomer ' + JSON.stringify(this.readDataCustomer));
        } else {
          this.readDataCustomer = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach the server!',
        });
      }
    });

    (await this.lunasService.getTrans()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          // console.log('belanja data ' + JSON.stringify(this.dataTransaction));
          this.dataTransaction = res.filter((data: any) => data.dn200kemasid == this.idCustom);

          // filter unique value barcode id
          const filterUnique = this.dataTransaction
            .map((item: any) => item.dn107barcoid)
            .filter((value: any, index: any, self: string | any[]) => self.indexOf(value) === index);

          // count length value barcode id
          this.dataLength = filterUnique.length;
          
          // sum quantity in transaction
          let count = 0;
          for (let item of this.dataTransaction) {
            count = count + item.dn302qantity;  
            this.quantity = count;          
            console.log('total quantity ' + JSON.stringify(count));
          }
          return count;
        } else {
          this.readData = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach the server!',
        });
      }
    });
  }
}
