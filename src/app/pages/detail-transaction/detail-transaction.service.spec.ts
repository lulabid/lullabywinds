import { TestBed } from '@angular/core/testing';

import { DetailTransactionService } from './detail-transaction.service';

describe('DetailTransactionService', () => {
  let service: DetailTransactionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DetailTransactionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
