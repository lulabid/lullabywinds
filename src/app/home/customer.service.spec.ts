import { TestBed } from '@angular/core/testing';
import { CustomerService } from './customer.service';

describe('CustomerService', () => {
  let customerService: CustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    customerService = TestBed.inject(CustomerService);
  });

  it('should be created', () => {
    expect(customerService).toBeTruthy();
  });
});
