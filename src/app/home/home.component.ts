import { Component, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';
import { MessageService } from 'primeng/api';

import { CustomerService } from './customer.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { Router } from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [MessageService, DatePipe],
})
export class HomeComponent implements OnInit {
  isLoading = false;
  error: string | undefined;

  modalAdd: boolean = false;
  modalDetail: boolean = false;
  modalAddress: boolean = false;
  modalListAddress: boolean = false;

  userForm!: FormGroup;
  addressForm!: FormGroup;
  listUser: any | undefined;
  idCustom: any | undefined;

  idCustomer: any | undefined;
  idTransaction: any | undefined;
  getCustomerId: any | undefined;

  customer: any | undefined;
  dataCustomer: any | undefined;
  nameProvince: any | undefined;
  address: any | undefined;
  dataAddress: any | undefined;

  selectedCountry: any | undefined;
  selectedProvince: any | undefined;
  selectedGeo: any | undefined;
  searchValue: string | undefined;

  testCols: any | undefined;
  grades = [{ grade1: "grade1", grade2: "grade2" }];
  students: any | undefined;
  @ViewChild('dt') dt: any;

  _selectedColumns: any[];
  cols: any[];

  get f() {
    return this.userForm.controls;
  }

  get uf() {
    return this.addressForm.controls;
  }

  submitted = false;

  country = [
    {
      id: 81,
      name: 'Jepang',
    },
    {
      id: 62,
      name: 'Indonesia',
    },
  ];

  province = [
    {
      id: 1,
      name: 'Aceh',
    },
    {
      id: 2,
      name: 'Sumatera Utara',
    },
    {
      id: 3,
      name: 'Sumatera Barat',
    },
    {
      id: 4,
      name: 'Riau',
    },
    {
      id: 5,
      name: 'Jambi',
    },
    {
      id: 6,
      name: 'Sumatera Selatan',
    },
    {
      id: 7,
      name: 'Bengkulu',
    },
    {
      id: 8,
      name: 'Lampung',
    },
    {
      id: 9,
      name: 'Kepulauan Bangka Belitung',
    },
    {
      id: 10,
      name: 'Kepulauan Riau',
    },
    {
      id: 11,
      name: 'Dki Jakarta',
    },
    {
      id: 12,
      name: 'Jawa Barat',
    },
    {
      id: 13,
      name: 'Jawa Tengah',
    },
    {
      id: 14,
      name: 'Di Yogyakarta',
    },
    {
      id: 15,
      name: 'Jawa Timur',
    },
    {
      id: 16,
      name: 'Banten',
    },
    {
      id: 17,
      name: 'Bali',
    },
    {
      id: 18,
      name: 'Nusa Tenggara Barat',
    },
    {
      id: 19,
      name: 'Nusa Tenggara Timur',
    },
    {
      id: 20,
      name: 'Kalimantan Barat',
    },
    {
      id: 21,
      name: 'Kalimantan Tengah',
    },
    {
      id: 22,
      name: 'Kalimantan Selatan',
    },
    {
      id: 23,
      name: 'Kalimantan Timur',
    },
    {
      id: 24,
      name: 'Kalimantan Utara',
    },
    {
      id: 25,
      name: 'Sulawesi Utara',
    },
    {
      id: 26,
      name: 'Sulawesi Tengah',
    },
    {
      id: 27,
      name: 'Sulawesi Selatan',
    },
    {
      id: 28,
      name: 'Sulawesi Tenggara',
    },
    {
      id: 29,
      name: 'Gorontalo',
    },
    {
      id: 30,
      name: 'Sulawesi Barat',
    },
    {
      id: 31,
      name: 'Maluku',
    },
    {
      id: 32,
      name: 'Maluku Utara',
    },
    {
      id: 33,
      name: 'Papua Barat',
    },
    {
      id: 34,
      name: 'Papua',
    },
  ];

  constructor(
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private fireauth: AngularFireAuth,
    private firestore: AngularFirestore,
    private customerService: CustomerService,
    private messageService: MessageService,
    private router: Router
  ) {
    this.Form();

    this.testCols = [
      { field: "name", header: "Name" },
      { field: "email", header: "Email" },
      { field: "phnNumber", header: "Contact No" },
      { field: this.grades[0]["grade1"], header: "Grade1" }
    ];

    this.students = [
      {
        name: "Abi",
        email: "test1@gmail.com",
        phnNumber: "23456787654",
        grades: [
          {
            grade1: "AA",
            grade2: "X"
          }
        ]
      },
      {
        name: "Ray",
        email: "test2@gmail.com",
        phnNumber: "1234543666",
        grades: [
          {
            grade1: "CC",
            grade2: "Y"
          }
        ]
      },
      {
        name: "Harry",
        email: "test3@gmail.com",
        phnNumber: "23212324",
        grades: [
          {
            grade1: "BB",
            grade2: "Z"
          }
        ]
      }
    ];
  }

  myFilter(e: any) {
    this.dt.value.forEach((e: any) => {
      e['grade1'] = e['grades'][0]['grade1'];
    });
    this.dt.filterGlobal(e.target.value, 'contains');
    setTimeout(() => {
      this.dt.value.forEach((e: any) => {
        delete e['grade1'];
      });
    }, 500)
  }

  onSort(e: any) {
    let filed = e.field;
    let arr = [...this.students];
    if (filed === 'grade1') {
      // custome sort only for these fields

      let order = e.order; // 1 means abc and -1 means cba
      if (order === 1) {
        this.students = this.students.sort(this.OrderListBy(filed));
        console.log('order 1' + JSON.stringify(this.students));
      } else {
        this.students = this.students.sort(this.OrderListBy(filed)).reverse();
        console.log('order -1' + JSON.stringify(this.students));
      }
    }
  }

  OrderListBy(prop: any) {
    return function (a: any, b: any) {
      if (a['grades'][0][prop] > b['grades'][0][prop]) {
        return 1;
      }
      else if (a['grades'][0][prop] < b['grades'][0][prop]) {
        return -1;
      }
      return 0;
    }
  }

  // Get customer name and compare if exist
  async checkName(event: any) {
    if (event.target.value.length > 0) {
      const lulaname = event.target.value;

      const member = await firebase.firestore().collection('cl001members');
      member
        .where('dk006fulname', '==', lulaname)
        .get()
        .then(async (snapshot) => {
          this.isLoading = false;

          if (!snapshot.empty) {
            this.messageService.add({
              key: 'warning',
              severity: 'warning',
              summary: 'Warning Message',
              detail: 'Nama sudah ada',
            });
          } else {
            this.messageService.add({
              key: 'success',
              severity: 'success',
              summary: 'Success Message',
              detail: 'Nama belum ada',
            });
          }
        });
    }
  }  
  
  // generateEAN(number: any) {
  //   let code = number;
  //   let weightflag = true;
  //   let sum: any = 0;

  //   for (let i = code.length - 1; i >= 0; i--) {
  //     sum += code[i] * (weightflag ? 3 : 1);
  //     weightflag = !weightflag;
  //   }
  //   code = (10 - (sum % 10)) % 10;
  //   return number + code;
  // }

  ngOnInit() {
    this.getCustomer();

    this.cols = [
      { field: 'dk000custoid', header: 'CustID' },
      { field: 'dk006fulname', header: 'Pmbeli' },
      { field: 'dk001emaddre', header: 'Email' },
      { field: 'dk011postnum', header: 'Kdpos' },
      { field: 'dk012prefect', header: 'Prov' },
      { field: 'dk013citynam', header: 'Kota' },
      { field: 'dk014lainnya', header: 'Jln' },
      { field: 'dk015telpnum', header: 'Telp' },
    ];

    this._selectedColumns = this.cols;
    // console.log('convert ean ' + this.generateEAN('000000000007'));
  }

  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col: any) => val.includes(col));
  }

  addModal() {
    // this.selectedCountry = 62;
    // this.selectedProvince = 1;
    // this.selectedGeo = '0,0';

    this.userForm.patchValue({
      dk001emaddre: "",
      dk010country: 62,
      dk011postnum: "",
      dk012prefect: 1,
      dk013citynam: "",
      dk014lainnya: "",
      dk015telpnum: "",
      dk016geopoin: "0,0",
      dk005catatan: "",
    });
    
    this.modalAdd = true;
    this.renderer.addClass(document.body, 'modal-open');
  }

  removeModal() {
    this.modalAdd = false;
    this.modalDetail = false;
    this.modalAddress = false;
    this.modalListAddress = false;
    this.renderer.removeClass(document.body, 'modal-open');

    this.userForm.reset();
    this.addressForm.reset();
  }

  removeModalCreate() {
    this.modalAdd = false;
    this.modalAddress = false;
    this.renderer.removeClass(document.body, 'modal-open');

    this.userForm.reset();
    this.addressForm.reset();
  }

  addressModal(event: any) {
    this.modalAddress = true;
    this.renderer.addClass(document.body, 'modal-open');

    this.idCustomer = event.id;

    this.addressForm.patchValue({
      idCustomer: this.idCustomer,
    });

    // console.log('get id kustomer and create ' + event.id);
  }

  listAddressModal(event: any) {
    // this.modalListAddress = true;
    // this.renderer.addClass(document.body, 'modal-open');

    this.idCustomer = event.id;

    // this.getAddress();

    this.router.navigate(['customer-address', this.idCustomer]);

    // console.log('get id kustomer and read ' + event.id);
  }

  closed(event: any) {
    console.log('event ' + event.target.className);
    if(event.target.className === "modal fade show") {
      this.modalDetail = false;
      this.modalAddress = false;
      this.modalAdd = false;
      this.renderer.removeClass(document.body, 'modal-open');

      this.userForm.reset();
    } 
  }

  async getCustomerData() {
    return this.firestore
      .collection('cl001members')
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as any;
            const id = a.payload.doc.id;
            const meta = a.payload.doc.metadata;
            return { id, meta, ...data };
          })
        )
      );
  }

  filterCustomer(e: any) {
    this.dt.value.forEach((e: any) => {
      e['dk011postnum'] = e['dk008address'][0]['dk011postnum'];
    });
    this.dt.filterGlobal(e.target.value, 'contains');
    setTimeout(() => {
      this.dt.value.forEach((e: any) => {
        delete e['dk011postnum'];
      });
    }, 500)
  }

  // filterName(search: any) {
    
  //   this.fireauth.onAuthStateChanged(async (user) => {
  //     if (search) {
      
        
  //       (await this.getCustomerData()).subscribe((res: any) => {
  //         if (res) {
  //           this.customer = this.firestore
  //             .collection('cl001members', (ref) => ref.where('dk006fulname', '<=', search))
  //             .valueChanges();
  //         } else {
  //           this.customer = [];

  //           this.messageService.add({
  //             key: 'error',
  //             severity: 'error',
  //             summary: 'Error Message',
  //             detail: 'Can not reach data customer from server!',
  //           });
  //         }
  //       });
       
  //     } else {
  //       this.getCustomer();
  //     }
  //   });
  // }

  // search() {
  //   this.customer = this.filterName(this.searchValue);
  // }

  listTransaction(event: any) {
    this.idTransaction = event.dk000custoid;
    this.router.navigate(['transaction', this.idTransaction]);

    // console.log('get id customer for transaction and read ' + event.dk000custoid);
  }

  countryChange(event: any) {
    // console.log('country select ' + event.target.value);
    this.userForm.controls['dk010country'].setValue(parseInt(event.target.value));
  }

  provinceChange(event: any) {
    // console.log('province select ' + event.target.value);
    this.userForm.controls['dk012prefect'].setValue(parseInt(event.target.value));
  }

  countryAddressChange(event: any) {
    // console.log('country select ' + event.target.value);
    this.addressForm.controls['ak010country'].setValue(parseInt(event.target.value));
  }

  provinceAddressChange(event: any) {
    // console.log('province select ' + event.target.value);
    this.addressForm.controls['ak012prefect'].setValue(parseInt(event.target.value));
  }

  selectCountry(event: any) {
    this.userForm.patchValue({
      dk010country: parseInt(event.target.value),
    });
  }

  selectProvince(event: any) {
    this.userForm.patchValue({
      dk012prefect: parseInt(event.target.value),
    });
  }

  private Form() {
    this.userForm = this.formBuilder.group({
      idCustomer: [''],
      dk000custoid: [''],
      dk001emaddre: ['', [Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      dk006fulname: [
        '',
        [Validators.required, Validators.minLength(3), Validators.maxLength(50)],
      ],
      dk010country: [''],
      dk011postnum: [
        '',
        [Validators.minLength(3), Validators.maxLength(20)],
      ],
      dk012prefect: [''],
      dk013citynam: ['', [Validators.minLength(3), Validators.maxLength(50)]],
      dk014lainnya: ['', [Validators.minLength(3), Validators.maxLength(500)]],
      dk015telpnum: ['', [Validators.pattern('^[0-9]*$')]],
      dk016geopoin: ['', Validators.pattern('-?[0-9][0-9]*(\.[0-9]+)?,\s*-?[0-9][0-9]*(\.[0-9]+)?')],
      dk005catatan: [''],
      dk007created: [''],
    });

    this.addressForm = this.formBuilder.group({
      idCustomer: [''],
      ak009namalen: [
        '',
        [Validators.required, Validators.minLength(3), Validators.maxLength(50), Validators.pattern(/^[\w\s]+$/)],
      ],
      ak010country: [''],
      ak011postnum: [
        '',
        [Validators.minLength(3), Validators.maxLength(20), Validators.pattern(/^[\w\s]+$/)],
      ],
      ak012prefect: [''],
      ak013citynam: ['', [Validators.minLength(3), Validators.maxLength(50)]],
      ak014lainnya: ['', [Validators.minLength(3), Validators.maxLength(500)]],
      ak015telpnum: ['', [Validators.pattern('^[0-9]*$')]],
      ak016geopoin: ['', Validators.pattern('-?[0-9][0-9]*(\.[0-9]+)?,\s*-?[0-9][0-9]*(\.[0-9]+)?')],
    });
  }

  async addCustomer() {
    this.isLoading = true;
    this.submitted = true;

    const user = await this.fireauth.currentUser;

    if (this.userForm.invalid) {
      this.isLoading = false;

      if (this.f.dk001emaddre.errors) {
        if (this.f.dk001emaddre.errors.email) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Email format wrong. Ex:youremail@email.com',
          });
        } else if (this.f.dk001emaddre.errors.pattern) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Email format wrong. Use format email! Ex: email@domain.com',
          });
        }
      }

      if (this.f.dk006fulname.errors) {
        if (this.f.dk006fulname.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name min 3 character!',
          });
        } else if (this.f.dk006fulname.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name max 50 character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name can not empty!',
          });
        }
      }

      if (this.f.dk010country.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Country can not empty!',
        });
      }

      if (this.f.dk011postnum.errors) {
        if (this.f.dk011postnum.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Post code min 3 character!',
          });
        } else if (this.f.dk011postnum.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Post code max 20 character!',
          });
        }
      }

      if (this.f.dk012prefect.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Province can not empty!',
        });
      }

      if (this.f.dk013citynam.errors) {
        if (this.f.dk013citynam.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City min 3 character!',
          });
        } else if (this.f.dk013citynam.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City max 50 character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City can not empty!',
          });
        }
      }

      if (this.f.dk014lainnya.errors) {
        if (this.f.dk014lainnya.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Address min 3 character!',
          });
        } else if (this.f.dk014lainnya.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Address max 500 character!',
          });
        }
      }

      if (this.f.dk015telpnum.errors) {
        if (this.f.dk015telpnum.errors.pattern) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Phone number only number!',
          });
        }
      }
      
      if (this.f.dk016geopoin.errors) {
        if (this.f.dk016geopoin.errors.pattern) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Geopoint format wrong! User format coordinate. Ex: latitude between -90 and 90 and longitude between -180 and 180',
          });
        }
      }

      return;
    } else {
      this.submitted = false;
    }

    // this.fireauth.onAuthStateChanged(async (user) => {
    //   if (user) {
    //       this.isLoading = false;
    //       // const customId = firebase.firestore.FieldValue.increment(1);
    //       // console.log(JSON.stringify(customId));
    //       // this.userForm.controls['dk000custoid'].setValue(customId);

    //      (await this.customerService.addCustomer(this.userForm.value)).subscribe();
    //   }
    // });

    // const setId = firebase.firestore().collection('cl001members').doc('--idx--');
    // const increment = firebase.firestore.FieldValue.increment(1);
    // setId.set({ idCount: increment }, { merge: true });

    // if (setId.set) {
    //   this.userForm.controls['dk000custoid'].setValue(this.idCustom);
    // }

    this.customerService.createCustomer(this.userForm.value).then((res: any) => {
      this.isLoading = false;

      if (res) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not create new customer!',
        });
      } else {
        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully created a new customer',
        });

        this.removeModal();
      }
    }).catch(error => {
      this.isLoading = false;

      console.log('errrorrr '  + error.message );
      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: error.message,
      });
    });
  }

  async getCustomer() {
    this.isLoading = true;

    (await this.customerService.readCustomer()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        // this.customer = res.splice(1);
        // console.log('data custoemr' + JSON.stringify(res.splice(1)));

        // Get all data customer
        this.customer = this.firestore
          .collection('cl001members', (ref) => ref.orderBy('dk000custoid', 'desc'))
          .snapshotChanges()
          .pipe(
            map((actions) =>
              actions.map((a) => {
                const data = a.payload.doc.data() as any;
                const id = a.payload.doc.id;
                const meta = a.payload.doc.metadata;

                // console.log('data ' + JSON.stringify(data));
                this.dataCustomer = data;

                return { id, meta, ...data };
              })
            )
          );

        // for (let item of res) {
        //   for (let data of item.dk008address) {
        //     console.log('detail ' + JSON.stringify(data));
        //   }
        // }
      } else {
        console.log('Can not reach the server');
      }
    });
  }

  async updateCustomer() {
    this.isLoading = true;
    this.submitted = true;

    if (this.userForm.invalid) {
      this.isLoading = false;

      if (this.f.dk001emaddre.errors) {
        if (this.f.dk001emaddre.errors.email) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Email format wrong. Ex:youremail@email.com',
          });
        } else if (this.f.dk001emaddre.errors.pattern) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Email format wrong. Use format email! Ex: email@domain.com',
          });
        }
      }

      if (this.f.dk006fulname.errors) {
        if (this.f.dk006fulname.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name min 3 character!',
          });
        } else if (this.f.dk006fulname.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name max 50 character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name can not empty!',
          });
        }
      }

      if (this.f.dk010country.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Country can not empty!',
        });
      }

      if (this.f.dk011postnum.errors) {
        if (this.f.dk011postnum.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Post code min 3 character!',
          });
        } else if (this.f.dk011postnum.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Post code max 20 character!',
          });
        }
      }

      if (this.f.dk012prefect.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Province can not empty!',
        });
      }

      if (this.f.dk013citynam.errors) {
        if (this.f.dk013citynam.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City min 3 character!',
          });
        } else if (this.f.dk013citynam.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City max 50 character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City can not empty!',
          });
        }
      }

      if (this.f.dk014lainnya.errors) {
        if (this.f.dk014lainnya.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Address min 3 character!',
          });
        } else if (this.f.dk014lainnya.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Address max 500 character!',
          });
        }
      }

      if (this.f.dk015telpnum.errors) {
        if (this.f.dk015telpnum.errors.pattern) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Phone number only number!',
          });
        }
      }

      if (this.f.dk016geopoin.errors) {
        if (this.f.dk016geopoin.errors.pattern) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Geopoint format wrong! User format coordinate. Ex: latitude between -90 and 90 and longitude between -180 and 180',
          });
        }
      }

      return;
    } else {
      this.submitted = false;
    }

    this.customerService.updateCustomer(this.userForm.value).then((res: any) => {
      if (res) {
        console.log('Error update data customer');
      } else {
        console.log('Your customer success update');

        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully update customer',
        });

        this.removeModal();
      }
    }).catch(error => {
      this.isLoading = false;

      console.log('errrorrr '  + error.message );
      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: error.message,
      });
    });
  }

  async deleteCustomer() {
    this.isLoading = true;

    console.log('id customer ' + this.getCustomerId);

    const member = await firebase.firestore().collectionGroup('cl004belanja');
    member
      .where('dn000custoid', '==', this.getCustomerId)
      .get()
      .then(async (snapshot) => {
        this.isLoading = false;

        if (!snapshot.empty) {
          this.messageService.add({
            key: 'warning',
            severity: 'warning',
            summary: 'Warning Message',
            detail: 'Masih ada transaksi di keranjang belanja',
          });
        } else {
          this.messageService.add({
            key: 'success',
            severity: 'success',
            summary: 'Success Message',
            detail: 'Tidak ada transaksi di keranjang belanja',
          });

          this.customerService.deleteCustomer(this.userForm.value).then((res: any) => {
            if (res) {
              console.log('Error delete data customer');
            } else {
              console.log('Your customer success delete');
      
              this.messageService.add({
                key: 'success',
                severity: 'success',
                summary: 'Success Message',
                detail: 'Succesfully delete customer',
              });
      
              this.removeModal();
            }
          });
        }
      });
  }

  lulaId(event: any) {
    console.log('event ' + JSON.stringify(event));

    // console.log('get id kustomer ' + event.id);
    this.idCustomer = event.id;
    this.getCustomerId = event.dk000custoid;

    this.modalDetail = true;
    this.renderer.addClass(document.body, 'modal-open');

    const formDate = moment(new Date(event.dk007created?.seconds * 1000)).format('MM/DD/YYYY hh:mm:ss A');

    this.userForm.patchValue({
      idCustomer: this.idCustomer,
      dk000custoid: event.dk000custoid,
      dk001emaddre: event.dk001emaddre,
      dk006fulname: event.dk006fulname,
      dk010country: event.dk010country,
      dk011postnum: event.dk011postnum,
      dk012prefect: event.dk012prefect,
      dk013citynam: event.dk013citynam,
      dk014lainnya: event.dk014lainnya,
      dk015telpnum: event.dk015telpnum,
      dk016geopoin: event.dk016geopoin.latitude + ',' + event.dk016geopoin.longitude,
      dk005catatan: event.dk005catatan,
      dk007created: formDate,
    });
  }

  lulaName(event: any) {
    console.log('event ' + JSON.stringify(event));
  }

  async addAddress() {
    this.isLoading = true;
    this.submitted = true;

    const user = await this.fireauth.currentUser;

    if (this.addressForm.invalid) {
      this.isLoading = false;

      if (this.uf.ak009namalen.errors) {
        if (this.uf.ak009namalen.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name min 3 character!',
          });
        } else if (this.uf.ak009namalen.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name max 50 character!',
          });
        } else if (this.uf.ak009namalen.errors.pattern) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name can not input special character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Full name can not empty!',
          });
        }
      }

      if (this.uf.ak010country.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Country can not empty!',
        });
      }

      if (this.uf.ak011postnum.errors) {
        if (this.uf.ak011postnum.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Post code min 3 character!',
          });
        } else if (this.uf.ak011postnum.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Post code max 20 character!',
          });
        } else if (this.uf.ak011postnum.errors.pattern) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Post code can not input special character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Post code can not empty!',
          });
        }
      }

      if (this.uf.ak012prefect.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Province can not empty!',
        });
      }

      if (this.uf.ak013citynam.errors) {
        if (this.uf.ak013citynam.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City min 3 character!',
          });
        } else if (this.uf.ak013citynam.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City max 50 character!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'City can not empty!',
          });
        }
      }

      if (this.uf.ak014lainnya.errors) {
        if (this.uf.ak014lainnya.errors.minlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Address min 3 character!',
          });
        } else if (this.uf.ak014lainnya.errors.maxlength) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Address max 500 character!',
          });
        }
      }

      if (this.uf.ak015telpnum.errors) {
        if (this.uf.ak015telpnum.errors.pattern) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Phone number only number!',
          });
        } else {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Phone number can not empty!',
          });
        }
      }

      return;
    } else {
      this.submitted = false;
    }

    this.customerService.createAddress(this.addressForm.value).then((res: any) => {
      this.isLoading = false;

      if (res) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not create new address!',
        });
      } else {
        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully created a new address',
        });

        this.removeModalCreate();
      }
    });
  }

  async getAddress() {
    this.isLoading = true;

    (await this.customerService.readAddress()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        // Get all data address
        this.address = this.firestore
          .collection(`cl001members/${this.idCustomer}/cl007address`, (ref) => ref.orderBy('ak009namalen'))
          .snapshotChanges()
          .pipe(
            map((actions) =>
              actions.map((a) => {
                const data = a.payload.doc.data() as any;
                const id = a.payload.doc.id;
                const meta = a.payload.doc.metadata;

                // console.log('data ' + JSON.stringify(data));
                this.dataAddress = data;

                return { id, meta, ...data };
              })
            )
          );

        // for (let item of res) {
        //   for (let data of item.dk008address) {
        //     console.log('detail ' + JSON.stringify(data));
        //   }
        // }
      } else {
        console.log('Can not reach the server');
      }
    });
  }
}
