import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { firestore } from 'firebase';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

const routes = {
  data: (c: dataContext) => `/jokes/random?category=${c.category}`,
  member: () => `/cl001members`,
  address: () => `/cl007address`,
};

const credentialsKey = 'credentials';

export interface dataContext {
  // The quote's category: 'dev', 'explicit'...
  category: string;
}

export interface idCount {
  idCount: number;
}

export interface geoPoint {
  latitude: number;
  longitude: number;
}

export interface memberContext {
  idCustomer: any;
  dk000custoid: string;
  dk001emaddre: string;
  dk006fulname: string;
  dk010country: number;
  dk011postnum: string;
  dk012prefect: number;
  dk013citynam: string;
  dk014lainnya: string;
  dk015telpnum: number;
  dk016geopoin: any;
  dk005catatan: string;
  dk007created: string;
}

export interface addressContext {
  idCustomer: any;
  ak009namalen: string;
  ak008address: any;
  ak010country: string;
  ak011postnum: string;
  ak012prefect: number;
  ak013citynam: string;
  ak014lainnya: string;
  ak015telpnum: number;
  ak016geopoin: any;
}

export interface adminContext {
  di000adminid: string;
  di001dmymail: string;
  di003authori: string;
  di004admname: string;
  di005catatan: string;
}

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  idCustom: any;
  lengthData: any;
  value = new Subject<string>();

  constructor(private httpClient: HttpClient, private firestore: AngularFirestore, private fireauth: AngularFireAuth) {}

  calculateEan13(number: any) {
    let code = number;
    let sum: any = 0;

    // Calculate the checksum digit here.
    for (let i = code.length - 1; i >= 0; i--) {
      // This appears to be backwards but the 
      // EAN-13 checksum must be calculated
      if (i % 2) { // odd  
        sum += code[i] * 3;
      }
      else { // even
        sum += code[i] * 1;
      }
    }
    code = (10 - (sum % 10)) % 10;
    return number + code;
  }

  getId() {
    return this.firestore.collection(routes.member()).doc('--idx--').valueChanges() as Observable<idCount[]>;
  }

  getOnce() {
    return this.firestore.collection(routes.member()).doc('--idx--').get();
  }

  async readCustomer() {
    return this.firestore.collection(routes.member()).valueChanges() as Observable<memberContext[]>;
  }

  async createCustomer(context: memberContext) {
    const user = await this.fireauth.currentUser;
    
    if (user) { 

      this.getOnce().subscribe(async (data) => {
        // check id increment value
        const value = data.get('bn004bcduser');
        console.log('length data value ' + value);
  
        if (value == undefined) {
          // Create id increment 0
          const setId = firebase.firestore().collection(routes.member()).doc('--idx--');
          const increment = firebase.firestore.FieldValue.increment(0);
          setId.set({ bn004bcduser: increment }, { merge: true });

          console.log('increment value ' + increment);

          this.getOnce()
            .pipe(debounceTime(0))
            .pipe(distinctUntilChanged())
            .subscribe((data: any) => {
              // Save value id increment in variable
              const sliceId = ('00000000000' + (data.data().bn004bcduser + 8)).slice(-12); 
              const idInc = this.calculateEan13(sliceId); // output: 000000000000;      
              this.idCustom = idInc;
              console.log('sliceId ' + sliceId);
              console.log('horeeeeeeeeeeee id' + this.idCustom);

              const valGeo = context.dk016geopoin.split(',');
              console.log('valGeo ' + valGeo);
              const latitude = parseFloat(valGeo[0]);
              const longitude = parseFloat(valGeo[1]);
              console.log('latitude ' + latitude);
              console.log('longitude ' + longitude);

              // Get id value increment & implement into dk000custoid
              firebase
                .firestore()
                .collection(routes.member())
                .doc()
                .set({
                  dk000custoid: this.idCustom,
                  dk001emaddre: context.dk001emaddre,
                  dk006fulname: context.dk006fulname,
                  dk010country: context.dk010country,
                  dk011postnum: context.dk011postnum,
                  dk012prefect: context.dk012prefect,
                  dk013citynam: context.dk013citynam,
                  dk014lainnya: context.dk014lainnya,
                  dk015telpnum: context.dk015telpnum,
                  dk016geopoin: new firebase.firestore.GeoPoint(latitude, longitude),
                  dk005catatan: context.dk005catatan,
                  dk007created: firebase.firestore.FieldValue.serverTimestamp(),
                });
            });
        } else {
          // Create id increment > 0
          const setId = firebase.firestore().collection(routes.member()).doc('--idx--');
          const increment = firebase.firestore.FieldValue.increment(1);
          setId.set({ bn004bcduser: increment }, { merge: true });

          console.log('length data value ' + value);

          this.getOnce()
            .pipe(debounceTime(0))
            .pipe(distinctUntilChanged())
            .subscribe((data: any) => {
              // Save value id increment in variable
              const sliceId = ('00000000000' + (data.data().bn004bcduser + 8)).slice(-12); 
              const idInc = this.calculateEan13(sliceId); // output: 000000000000;      
              this.idCustom = idInc;
              console.log('sliceId ' + sliceId);
              console.log('horeeeeeeeeeeee id' + this.idCustom);

              const valGeo = context.dk016geopoin.split(',');
              console.log('valGeo ' + valGeo);
              const latitude = parseFloat(valGeo[0]);
              const longitude = parseFloat(valGeo[1]);
              console.log('latitude ' + latitude);
              console.log('longitude ' + longitude);

              // Get id value increment & implement into dk000custoid
              firebase
                .firestore()
                .collection(routes.member())
                .doc()
                .set({
                  dk000custoid: this.idCustom,
                  dk001emaddre: context.dk001emaddre,
                  dk006fulname: context.dk006fulname,
                  dk010country: context.dk010country,
                  dk011postnum: context.dk011postnum,
                  dk012prefect: context.dk012prefect,
                  dk013citynam: context.dk013citynam,
                  dk014lainnya: context.dk014lainnya,
                  dk015telpnum: context.dk015telpnum,
                  dk016geopoin: new firebase.firestore.GeoPoint(latitude, longitude),
                  dk005catatan: context.dk005catatan,
                  dk007created: firebase.firestore.FieldValue.serverTimestamp(),
                });
            });
        }
      })      

      // Get length document
      this.firestore
        .collection(routes.member())
        .get()
        .pipe(debounceTime(1100))
        .pipe(distinctUntilChanged())
        .subscribe(async (data) => {
          const lengthData = data.size + 1; // will return the collection size
          this.lengthData = lengthData;
          // console.log('lengthData ' + lengthData);

          // const idInc = ('0000000000000' + lengthData).slice(-13); // output: 000000000000;
          // const idRef = firebase.firestore().collection(routes.member()).doc();
          // const batch = firebase.firestore().batch();

          // batch.set(idRef, { dk000custoid: lengthData });
          // batch.set(setId, { idCount: increment }, { merge: true });
          // batch.commit();

          // this.firestore
          //   .collection(routes.member())
          //   .doc('--idx--')
          //   .snapshotChanges()
          //   .pipe(
          //     map((action) => {
          //       const data = action.payload.data() as idCount;
          //       return { ...data };
          //     })
          //   ).subscribe(async (data) => {
          //     console.log('dataaaa ' + data.idCount);
          //   });

          // this.getId().subscribe(async (data: any) => {
          //   if (data) {
          //     const idInc = ('000000000000' + (data.idCount + 1)).slice(-12); // output: 000000000000;
          //     console.log('idInc' + JSON.stringify(idInc));
          //     this.idCustom = idInc;
          //   }
          // });        

          // await firebase.firestore().collection('cl001members').doc().set({dk000custoid: firebase.firestore.FieldValue.increment(1)});

          // var docRef = firebase.firestore().collection('cl001members').doc();

          // firebase.firestore().runTransaction(function(transaction) {
          //     return transaction.get(docRef).then(function(incDoc): any {

          //         //if no value exist, assume it as 0 and increase to 1
          //         var newIncId = (incDoc.data().number || 0) + 1;

          //         transaction.set(docRef, { number: newIncId });
          //         return newIncId;
          //     });
          // }).then(function(newIncId) {
          //     //`newIncId` This is your new number incremented

          //     console.log("New autoincremented number ", newIncId);
          // }).catch(function(err) {
          //     // Catch block to get any error
          //     console.error(err);
          // });

          // // Auto increment value
          // firebase.firestore()
          //   .collection('cl001members')
          //   .doc()
          //   .set({
          //     dk000custoid: idInc,
          //     dk001emaddre: context.dk001emaddre,
          //     dk006fulname: context.dk006fulname,
          //     dk008address: [
          //       {
          //         dk010country: context.dk010country,
          //         dk011postnum: context.dk011postnum.toLowerCase(),
          //         dk012prefect: context.dk012prefect,
          //         dk013citynam: context.dk013citynam,
          //         dk014lainnya: context.dk014lainnya,
          //       }
          //     ],
          //     dk015telpnum: context.dk015telpnum,
          //     dk016geopoin: new firebase.firestore.GeoPoint(latitude, longitude),
          //     dk005catatan: context.dk005catatan,
          //     dk007created: firebase.firestore.FieldValue.serverTimestamp()
          //   });
        });

      // Get data list customer
      // this.firestore
      //   .collection('cl001members')
      //   .doc('--idx--')
      //   .get()
      //   .subscribe((data: any) => {
      //     // const sliceId = ('000000000000' + data.data().bn004bcduser).slice(-13); 
      //     // const idInc = this.calculateEan13(sliceId); // output: 000000000000;      
      //     // this.idCustom = idInc;
      //     // console.log('yes id' + this.idCustom);

          
      //   });
     
    }

    //   if (user) {
    //     return new Promise<any>((resolve, reject) => {
    //       firebase.firestore()
    //         .collection(routes.member())
    //         .doc(user.uid)
    //         .set({
    //           dk000custoid: firebase.firestore.FieldValue.increment(1),
    //           dk001emaddre: context.dk001emaddre,
    //           dk006fulname: context.dk006fulname,
    //           dk008address: [
    //             {
    //               dk010country: context.dk010country,
    //               dk011postnum: context.dk011postnum.toLowerCase(),
    //               dk012prefect: context.dk012prefect,
    //               dk013citynam: context.dk013citynam,
    //               dk014lainnya: context.dk014lainnya,
    //             }
    //           ],
    //           dk015telpnum: context.dk015telpnum,
    //           dk016geopoin: new firebase.firestore.GeoPoint(latitude, longitude),
    //           dk005catatan: context.dk005catatan,
    //           dk007created: firebase.firestore.FieldValue.serverTimestamp()
    //         })
    //         .then(
    //           (res: any) => {
    //             alert('Your customer success added');
    //             // this.router.navigate(['/profile-display'], { replaceUrl: true });
    //           },
    //           (err) => {
    //             alert('Status error: ' + err);
    //             reject(err);
    //           }
    //         );
    //     });
    //   }
    // }
  }

  async updateCustomer(context: memberContext) {
    let user = await this.fireauth.currentUser;
    console.log('get id update customer ' + context.idCustomer);

    const valGeo = context.dk016geopoin.split(',');
    console.log('valGeo ' + valGeo);
    const latitude = parseFloat(valGeo[0]);
    const longitude = parseFloat(valGeo[1]);
    console.log('latitude ' + latitude);
    console.log('longitude ' + longitude);

    if (user) {
      this.firestore
        .collection(routes.member())
        .doc(context.idCustomer)
        .update({
          dk001emaddre: context.dk001emaddre,
          dk006fulname: context.dk006fulname,
          dk010country: context.dk010country,
          dk011postnum: context.dk011postnum,
          dk012prefect: context.dk012prefect,
          dk013citynam: context.dk013citynam,
          dk014lainnya: context.dk014lainnya,
          dk015telpnum: context.dk015telpnum,
          dk016geopoin: new firebase.firestore.GeoPoint(latitude, longitude),
          dk005catatan: context.dk005catatan,
          dk007created: firebase.firestore.FieldValue.serverTimestamp(),
        });
    }
  }

  async deleteCustomer(context: memberContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      this.firestore.collection(routes.member()).doc(context.idCustomer).delete();
    }
  }

  async readAddress() {
    return this.firestore.collectionGroup('cl007address').valueChanges() as Observable<addressContext[]>;
  }

  async createAddress(context: addressContext) {
    const user = await this.fireauth.currentUser;

    const latitude = 0;
    const longitude = 0;

    console.log('get id create address ' + context.idCustomer);

    if (user) {
      firebase
        .firestore()
        .doc(`cl001members/${context.idCustomer}`)
        .collection('cl007address')
        .add({
          ak009namalen: context.ak009namalen,
          ak010country: context.ak010country,
          ak011postnum: context.ak011postnum,
          ak012prefect: context.ak012prefect,
          ak013citynam: context.ak013citynam,
          ak014lainnya: context.ak014lainnya,
          ak015telpnum: context.ak015telpnum,
          ak016geopoin: new firebase.firestore.GeoPoint(latitude, longitude),
        });
    }
  }

  async getAdmin() {
    let user = await this.fireauth.currentUser;
    let userEmail = await JSON.parse(localStorage.getItem(credentialsKey));
    console.log('userEmail credentialsKey' + JSON.stringify(userEmail.email));

    if (userEmail) {
      return this.firestore
      .collectionGroup(`cl000adminis`, (ref) => ref.where('di001dmymail', '==', userEmail.email)).valueChanges() as Observable<adminContext[]>;
    }
  }
}
