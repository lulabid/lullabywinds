import { TestBed } from '@angular/core/testing';

import { KotakService } from './kotak.service';

describe('KotakService', () => {
  let service: KotakService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KotakService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
