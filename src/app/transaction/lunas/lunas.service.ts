import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

const routes = {
  read: () => `/cl004belanja`,
  customer: () => '/cl001members',
};

export interface dataContext {
  // The quote's category: 'dev', 'explicit'...
  category: string;
}

export interface idCount {
  idCount: number;
}

export interface transactionContext {
  idt: any;
  idx: any;
  idGoods: any;
  dn300transid: string;
  dn307expordt: string;
  dn308whatbox: number;
  dn302qantity: number;
  dn101bymaker: string;
  dn102bseries: string;
  dn103handler: string;
  dn104release: string;
  dn111fotobrg: string;
  dn107barcoid: string;
  dn000custoid: string;
  dn006fulname: string;
  dn200kemasid: string;
  dn303dealprc: number;
  dn304paypric: number;
  dn305catatan: string;
  dn312alterby: string;
  dn309statudt: string;
  dn310sstatus: number;
}

export interface shipmentContext {
  ids: any;
  idx: any;
  idCustomer: any;
  hi200kemasid: string;
  hi205viakuri: number;
  hi206resinum: string;
  hi203onkiprc: number;
  hi204paypric: number;
  hi205catatan: string;
  hi212alterby: string;
  hi209statudt: string,
  hi210statuss: number;
  hi000custoid: string;
  hi006fulname: string;
  hi009namalen: string;
  hi010country: number;
  hi011postnum: string;
  hi012prefect: number;
  hi013citynam: string;
  hi014lainnya: string;
  hi015telpnum: string;
  hi016geopoin: string;
}

export interface memberContext {
  idCustomer: any;
  dk000custoid: string;
  dk001emaddre: string;
  dk006fulname: string;
  dk010country: number;
  dk011postnum: string;
  dk012prefect: number;
  dk013citynam: string;
  dk014lainnya: string;
  dk015telpnum: number;
  dk016geopoin: any;
  dk005catatan: string;
  dk007created: string;
}

@Injectable({
  providedIn: 'root',
})
export class LunasService {
  idCustom: any;
  value = new Subject<string>();

  constructor(private firestore: AngularFirestore, private fireauth: AngularFireAuth) {}

  async readTrans() {
    return this.firestore.collectionGroup('cl004belanja').valueChanges() as Observable<transactionContext[]>;
  }

  async getTrans() {
    return this.firestore
      .collectionGroup(`cl004belanja`)
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as any;
            const id = a.payload.doc.id;
            const idGood = a.payload.doc.ref.parent.parent.id;
            const meta = a.payload.doc.metadata;

            return { id, meta, idGood, ...data };
          })
        )
      );
  }

  async updateTrans(context: transactionContext) {
    let user = await this.fireauth.currentUser;
    console.log('get id goods ' + context.idGoods);
    console.log('get id transaksi ' + context.idx);

    if (user) {
      this.firestore
        .doc(`cl003merchan/${context.idGoods}`)
        .collection('cl004belanja')
        .doc(context.idx)
        .update({
          dn302qantity: context.dn302qantity,
          dn000custoid: context.dn000custoid,
          dn006fulname: context.dn006fulname,
          dn200kemasid: context.dn200kemasid,
          dn303dealprc: context.dn303dealprc,
          dn304paypric: context.dn304paypric,
          dn305catatan: context.dn305catatan,
        });
    }
  }

  async updateStatus(context: transactionContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      this.firestore
        .doc(`cl003merchan/${context.idGoods}`)
        .collection('cl004belanja')
        .doc(context.idx)
        .update({
          dn312alterby: context.dn312alterby,
          dn309statudt: context.dn309statudt,
          dn310sstatus: context.dn310sstatus,
        });
    }
  }

  async deleteTrans(context: transactionContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      this.firestore.collection(routes.read()).doc(context.idx).delete();
    }
  }

  // async readShip() {
  //   return this.firestore.collectionGroup('cl005shipmnt', (ref) => ref.orderBy('hi200kemasid')).valueChanges() as Observable<shipmentContext[]>;
  // }

  async updateCountShip(context: idCount) {
    let user = await this.fireauth.currentUser;

    if (user) {
      firebase.firestore().doc(`cl001members`).collection("cl005shipmnt").orderBy("idCount", "asc").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          doc.ref.update({
            idCount: firebase.firestore.FieldValue.increment(1)
          });
        });
      });
    }
  }

  getOnceIdShip() {
    return this.firestore.collection('cl001members').doc('--idx--').valueChanges();
  }

  async readShipCount() {
    return this.firestore
      .collectionGroup(`cl005shipmnt`)
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as Observable<shipmentContext[]>;
            const id = a.payload.doc.id;
            const meta = a.payload.doc.metadata;

            return { id, meta, ...data };
          })
        )
      );
  }

  async readShip() {
    return this.firestore
      .collectionGroup(`cl005shipmnt`, (ref) => ref.orderBy('hi200kemasid'))
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as Observable<shipmentContext[]>;
            const id = a.payload.doc.id;
            const idCustomer = a.payload.doc.ref.parent.parent.id;
            const meta = a.payload.doc.metadata;

            return { id, meta, idCustomer, ...data };
          })
        )
      );
  }

  async updateKemasShipment(context: shipmentContext) {
    let user = await this.fireauth.currentUser;
    console.log('get id customer ' + context.idCustomer);
    console.log('get ids ' + context.ids);

    if (user) {
      this.firestore.doc(`cl001members/${context.idCustomer}`).collection('cl005shipmnt').doc(context.ids).update({
        hi200kemasid: context.hi200kemasid,
      });
    }
  }

  async updateKemasTrans(context: transactionContext) {
    let user = await this.fireauth.currentUser;
    console.log('get id idGoods ' + context.idGoods);
    console.log('get idt ' + context.idt);
    console.log('updateKemasTrans dn200kemasid ' + context.dn200kemasid);

    if (user) {
      this.firestore.doc(`cl003merchan/${context.idGoods}`).collection('cl004belanja').doc(context.idt).update({
        dn200kemasid: context.dn200kemasid,
      });
    }
  }

  async readCustomer() {
    return this.firestore.collection(routes.customer()).valueChanges() as Observable<memberContext[]>;
  }

  async readShipment() {
    return this.firestore.collectionGroup('cl005shipmnt').valueChanges() as Observable<shipmentContext[]>;
  }
}
