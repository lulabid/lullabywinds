import { TestBed } from '@angular/core/testing';

import { LunasService } from './lunas.service';

describe('LunasService', () => {
  let service: LunasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LunasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
