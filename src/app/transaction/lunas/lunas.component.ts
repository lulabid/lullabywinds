import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LunasService } from './lunas.service';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { map } from 'rxjs/operators';
import { CustomerService } from '@app/home/customer.service';
import { SetokService } from '../setok/setok.service';

@Component({
  selector: 'app-lunas',
  templateUrl: './lunas.component.html',
  styleUrls: ['./lunas.component.scss'],
  providers: [MessageService, DatePipe],
})
export class LunasComponent implements OnInit {
  isLoading = false;
  error: string | undefined;

  modalKemas: Boolean = false;
  modalAdd: boolean = false;
  modalDetail: boolean = false;
  modalKirim: boolean = false;
  modalListAddress: boolean = false;
  modalPhoto: boolean = false;
  checKemas: boolean = false;
  checNewKemas: boolean = false;
  changeNewUser: boolean = false;
  readAdmin: any | undefined;

  transactionForm!: FormGroup;
  shipmentForm!: FormGroup;

  idShipment: any | undefined;
  idCustomer: any | undefined;
  idCustom: any | undefined;
  alterBy: any | undefined;

  idt: any | undefined;
  ids: any | undefined;
  idx: any | undefined;
  idGoods: any | undefined;
  readData: any | undefined;
  readTransLength: any | undefined;
  dataLength: any | undefined;
  searchValue: string | undefined;
  searchValueKemas: string | undefined;
  selectIdTrans: any | undefined;

  currentUser: any | undefined;
  readShipment: any | undefined;  
  filterKemas: any | undefined;
  idKemasTrans: any | undefined;

  statusID: any | undefined;
  countId: any | undefined;
  valueCount: any | undefined;

  selectKemas: any | undefined;
  selectedData: any;
  readDataCustomer: any | undefined;

  custoIdTrans: any | undefined;
  fulnameTrans: any | undefined;
  kemasIdTrans: any | undefined;

  currentKemas: any | undefined;
  whatBox: any | undefined;
  selectedTrans: any | undefined;

  getIdCs: any | undefined;
  selectedCustomer: any | undefined;
  selectedNewCustomer: any | undefined;
  detailTrans: any | undefined;
  currentIdCustomer: any | undefined;
  selectShipment: any | undefined;

  get tf() {
    return this.transactionForm.controls;
  }

  submitted = false;

  noImage: any = 'assets/no-image.jpg';
  pictureFile: any = 'assets/no-image.jpg';

  _selectedColumns: any[];
  cols: any[];

  _selectedColumnsKemas: any[];
  colsKemas: any[];

  province = [
    {
      id: 1,
      name: 'Aceh',
    },
    {
      id: 2,
      name: 'Sumatera Utara',
    },
    {
      id: 3,
      name: 'Sumatera Barat',
    },
    {
      id: 4,
      name: 'Riau',
    },
    {
      id: 5,
      name: 'Jambi',
    },
    {
      id: 6,
      name: 'Sumatera Selatan',
    },
    {
      id: 7,
      name: 'Bengkulu',
    },
    {
      id: 8,
      name: 'Lampung',
    },
    {
      id: 9,
      name: 'Kepulauan Bangka Belitung',
    },
    {
      id: 10,
      name: 'Kepulauan Riau',
    },
    {
      id: 11,
      name: 'Dki Jakarta',
    },
    {
      id: 12,
      name: 'Jawa Barat',
    },
    {
      id: 13,
      name: 'Jawa Tengah',
    },
    {
      id: 14,
      name: 'Di Yogyakarta',
    },
    {
      id: 15,
      name: 'Jawa Timur',
    },
    {
      id: 16,
      name: 'Banten',
    },
    {
      id: 17,
      name: 'Bali',
    },
    {
      id: 18,
      name: 'Nusa Tenggara Barat',
    },
    {
      id: 19,
      name: 'Nusa Tenggara Timur',
    },
    {
      id: 20,
      name: 'Kalimantan Barat',
    },
    {
      id: 21,
      name: 'Kalimantan Tengah',
    },
    {
      id: 22,
      name: 'Kalimantan Selatan',
    },
    {
      id: 23,
      name: 'Kalimantan Timur',
    },
    {
      id: 24,
      name: 'Kalimantan Utara',
    },
    {
      id: 25,
      name: 'Sulawesi Utara',
    },
    {
      id: 26,
      name: 'Sulawesi Tengah',
    },
    {
      id: 27,
      name: 'Sulawesi Selatan',
    },
    {
      id: 28,
      name: 'Sulawesi Tenggara',
    },
    {
      id: 29,
      name: 'Gorontalo',
    },
    {
      id: 30,
      name: 'Sulawesi Barat',
    },
    {
      id: 31,
      name: 'Maluku',
    },
    {
      id: 32,
      name: 'Maluku Utara',
    },
    {
      id: 33,
      name: 'Papua Barat',
    },
    {
      id: 34,
      name: 'Papua',
    },
  ];

  kurir = [
    {
      id: 0,
      name: 'Default',
    },
    {
      id: 1,
      name: 'Ambil Sendiri',
    },
    {
      id: 2,
      name: 'Anter Aja',
    },
    {
      id: 3,
      name: 'GO-SEND',
    },
    {
      id: 4,
      name: 'GOJEK',
    },
    {
      id: 5,
      name: 'GRAB',
    },
    {
      id: 6,
      name: 'JNE',
    },
    {
      id: 7,
      name: 'JNE JTR',
    },
    {
      id: 8,
      name: 'JNE OK',
    },
    {
      id: 9,
      name: 'JNE YES',
    },
    {
      id: 10,
      name: 'J&T',
    },
    {
      id: 11,
      name: 'Kurir',
    },
    {
      id: 12,
      name: 'MEX',
    },
    {
      id: 13,
      name: 'Pos Express',
    },
    {
      id: 14,
      name: 'Pos Kilat Khusus',
    },
    {
      id: 15,
      name: 'REX',
    },
    {
      id: 16,
      name: 'Si Cepat',
    },
    {
      id: 17,
      name: 'TIKI',
    },
    {
      id: 18,
      name: 'TIKI ECO',
    },
    {
      id: 19,
      name: 'TIKI ONS',
    },
    {
      id: 20,
      name: 'TIKI TCR',
    },
  ];

  calculateEan13(number: any) {
    let code = number;
    let sum: any = 0;

    // Calculate the checksum digit here.
    for (let i = code.length - 1; i >= 0; i--) {
      // This appears to be backwards but the 
      // EAN-13 checksum must be calculated
      if (i % 2) { // odd  
        sum += code[i] * 3;
      }
      else { // even
        sum += code[i] * 1;
      }
    }
    code = (10 - (sum % 10)) % 10;
    return number + code;
  }

  constructor(
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private fireauth: AngularFireAuth,
    private firestore: AngularFirestore,
    private service: LunasService,
    private serviceSetok: SetokService,
    private messageService: MessageService,
    private cstService: CustomerService,
    private datePipe: DatePipe
  ) {
    this.Form();
  }

  ngOnInit() {
    this.getTransaction();
    this.getCustomer();
    this.getShipment();    
    this.getAdmin();

    // set item column on p-table
    this.cols = [
      // { field: 'dn304paypric', header: 'Payment Price' },
      // { field: 'dn111fotobrg', header: 'Photo' },
      { field: 'dn107barcoid', header: 'BarcID' },
      { field: 'dn103handler', header: 'Baran' },
      { field: 'dn101bymaker', header: 'Mker' },
      { field: 'dn102bseries', header: 'Seri' },
      { field: 'dn302qantity', header: 'Q' },
      { field: 'dn000custoid', header: 'CustID' },
      { field: 'dn006fulname', header: 'Pmbeli' },
      { field: 'dn303dealprc', header: 'Sisa' },
      { field: 'dn200kemasid', header: 'KmasID' },
      { field: 'dn308whatbox', header: 'K' },
      { field: 'dn307expordt', header: 'DteX' },
      { field: 'dn305catatan', header: 'CatT' },
    ];

    this._selectedColumns = this.cols;

    this.colsKemas = [
      // { field: 'hi204paypric', header: 'Payment Price' },
      // { field: 'hi011postnum', header: 'Post Code' },
      { field: 'hi200kemasid', header: 'KmasID' },
      { field: 'hi000custoid', header: 'CustID' },
      { field: 'hi006fulname', header: 'Pmbeli' },
      { field: 'hi203onkiprc', header: 'Onkr' },
      { field: 'hi009namalen', header: 'Ptrima' },
      { field: 'hi012prefect', header: 'Prov' },
      { field: 'hi013citynam', header: 'Kota' },
      { field: 'hi014lainnya', header: 'Jln' },
      { field: 'hi015telpnum', header: 'Telp' },
      { field: 'hi205viakuri', header: 'Via' },
      { field: 'hi206resinum', header: 'ResiNo' },
      { field: 'hi205catatan', header: 'CatS' },
    ];

    this._selectedColumnsKemas = this.colsKemas;
  }  

  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }
  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col: any) => val.includes(col));
  }

  @Input() get selectedColumnsKemas(): any[] {
    return this._selectedColumnsKemas;
  }
  set selectedColumnsKemas(val: any[]) {
    //restore original order
    this._selectedColumnsKemas = this.colsKemas.filter((col: any) => val.includes(col));
  }

  addModal() {
    this.modalAdd = true;
    this.renderer.addClass(document.body, 'modal-open');

    this.transactionForm.reset();
  }

  removeModalTrans() {
    this.modalAdd = false;
    this.modalDetail = false;
    this.renderer.removeClass(document.body, 'modal-open');
  }

  removeModalKemas() {
    this.currentUser = "";
    this.selectKemas = "";
    this.searchValueKemas = "";
    this.modalKemas = false;
    this.checKemas = false;
    this.checNewKemas = false;
    this.renderer.removeClass(document.body, 'modal-open');
  }

  private Form() {
    this.transactionForm = this.formBuilder.group({
      idt: [''],
      idx: [''],
      idGoods: [''],
      dn307expordt: [''],
      dn308whatbox: [''],
      dn302qantity: ['', [Validators.required]],
      dn300transid: [''],
      dn101bymaker: [''],
      dn102bseries: [''],
      dn103handler: [''],
      dn104release: [''],
      dn107barcoid: [''],
      dn111fotobrg: [''],
      dn000custoid: [''],
      dn006fulname: [''],
      dn200kemasid: [''],
      dn303dealprc: ['', [Validators.required]],
      dn304paypric: ['', [Validators.required]],
      dn305catatan: [''],
      dn312alterby: [''],
      dn309statudt: [''],
      dn310sstatus: [''],
    });

    this.shipmentForm = this.formBuilder.group({
      ids: [''],
      idx: [''],
      idCustomer: [''],
      hi200kemasid: ['', [Validators.required]],
      hi205viakuri: [0],
      hi206resinum: [''],
      hi203onkiprc: [0],
      hi204paypric: [-999999999999],
      hi205catatan: [''],
      hi212alterby: [''],
      hi209statudt: [''],
      hi210statuss: [''],
      hi000custoid: [''],
      hi006fulname: [''],
      hi009namalen: [''],
      hi010country: [''],
      hi011postnum: [''],
      hi012prefect: [''],
      hi013citynam: [''],
      hi014lainnya: [''],
      hi015telpnum: [''],
      hi016geopoin: [''],
    });
  }

  async getAdmin() {
    (await this.cstService.getAdmin()).subscribe((res: any) => {
      if (res) {
        const status = res;
        if (status) {
          this.readAdmin = res[0].di004admname;
        } else {
          this.readAdmin = [];
        }
      }
    });
  }

  closed(event: any) {
    console.log('event ' + event.target.className);
    if(event.target.className === "modal fade show") {
      this.modalDetail = false;
      this.modalPhoto = false;
      this.renderer.removeClass(document.body, 'modal-open');
    } 
  }

  closedKemas(event: any) {
    console.log('event ' + event.target.className);
    if (event.target.className === "modal fade show") {
      this.modalKemas = false;
      this.currentUser = "";
      this.selectKemas = "";
      this.searchValueKemas = "";
      this.checKemas = false;
      this.checNewKemas = false;
      this.renderer.removeClass(document.body, 'modal-open');
    }
  }

  lulaPhoto(event: any) {
    if(event.dn111fotobrg == "" || event.dn111fotobrg == null) {
      this.pictureFile = 'assets/no-image.jpg';
    } else {
      this.pictureFile = event.dn111fotobrg;
    }

    this.modalPhoto = true;
    this.renderer.addClass(document.body, 'modal-open');

  }

  lulaId(event: any) {
    console.log('event ' + JSON.stringify(event));

    // this.getTransaction();

    // console.log('get id transaksi ' + event.id );    
    this.idx = event.id;

    this.modalDetail = true;
    this.renderer.addClass(document.body, 'modal-open');

    const dateReleaseGoods = moment(new Date(event.dn104release?.seconds * 1000)).format('YYYY/MM/DD hh:mm:ss');
    const statusDate = moment(new Date(event.dn309statudt?.seconds * 1000)).format('MM/DD/YYYY hh:mm:ss A');  

    const exportDate = moment(new Date(event.dn307expordt?.seconds * 1000)).format('MM/DD/YYYY hh:mm:ss A');

    this.detailTrans = event;

    this.transactionForm.patchValue({
      idx: event.id,
      idGoods: event.idGood,
      dn000custoid: event.dn000custoid,
      dn006fulname: event.dn006fulname,
      dn307expordt: exportDate,
      dn308whatbox: event.dn308whatbox,
      dn302qantity: event.dn302qantity,
      dn200kemasid: event.dn200kemasid,
      dn103handler: event.dn103handler,
      dn107barcoid: event.dn107barcoid,
      dn101bymaker: event.dn101bymaker,
      dn102bseries: event.dn102bseries,
      dn300transid: event.dn300transid,
      dn104release: dateReleaseGoods,
      dn303dealprc: event.dn303dealprc,
      dn304paypric: event.dn304paypric,
      dn305catatan: event.dn305catatan,
      dn312alterby: event.dn312alterby,
      dn309statudt: statusDate,
      dn310sstatus: event.dn310sstatus,
      dn111fotobrg: event.dn111fotobrg
    });

    this.readDataCustomer.forEach((ev: any) => {
      const customerSelect = ev.filter((data: any) => data.dk000custoid === event.dn000custoid);

      this.selectedData = {
        id: customerSelect[0]?.id,
        meta: customerSelect[0].meta,
        dk000custoid: customerSelect[0].dk000custoid, 
        dk006fulname: customerSelect[0].dk006fulname, 
        dk001emaddre: customerSelect[0].dk001emaddre,
        dk005catatan: customerSelect[0].dk005catatan,
        dk007created: customerSelect[0].dk007created,
        dk010country: customerSelect[0].dk010country,
        dk011postnum: customerSelect[0].dk011postnum,
        dk012prefect: customerSelect[0].dk012prefect,
        dk013citynam: customerSelect[0].dk013citynam,
        dk014lainnya: customerSelect[0].dk014lainnya,
        dk015telpnum: customerSelect[0].dk015telpnum,
        dk016geopoin: customerSelect[0].dk016geopoin,
      };
    });

    this.getTransaction();    

    const currUser = this.readShipment.filter((val: any) => val.hi000custoid == event.dn000custoid);
    console.log('this.selectedData currUser ' + JSON.stringify(currUser));     
    this.currentIdCustomer = currUser; 
  }

  async userId(event: any, item: any) {
    this.changeNewUser = false;

    this.selectIdTrans = item;
    console.log('this.detailTrans.dn000custoid ' + JSON.stringify(this.detailTrans.dn000custoid)); 
    console.log('this.detailTrans.dn006fulname ' + JSON.stringify(this.detailTrans.dn006fulname)); 
    
    // dont display current kemas id
    const getKemas = this.readShipment.filter((val: any ) => val.hi000custoid == this.detailTrans.dn000custoid && val.hi200kemasid == this.detailTrans.dn200kemasid);
    // filter same kemas id
    const filterShipment = getKemas.filter((val: any) => val.hi210statuss == 5 || val.hi210statuss == 6 || val.hi210statuss == 7 );
    console.log('length new buy filterDataShipment ' + JSON.stringify(filterShipment));

    filterShipment.forEach((data: any) => {
      if (data.hi000custoid == item.dk000custoid && data.hi006fulname == item.dk006fulname) {
        this.changeNewUser = false;
        console.log('data user sama');

        console.log('this.changeNewUser ' + this.changeNewUser);  
      } else {
        this.changeNewUser = true;
        console.log('data user gaa sama');

        this.selectShipment = data;
        console.log('data user in shipment ' + JSON.stringify(this.selectShipment));   

        console.log('this.changeNewUser ' + this.changeNewUser);     
      }
    })

    this.selectedData = {
      id: this.selectIdTrans.id,
      dk000custoid: this.selectIdTrans.dn000custoid, 
      dk006fulname: this.selectIdTrans.dn006fulname, 
    };

    this.transactionForm.patchValue({
      id: this.selectIdTrans.id,
      dn000custoid: this.selectIdTrans.dk000custoid,
      dn006fulname: this.selectIdTrans.dk006fulname
    });
  }

  async moveStatus(event: any) {
    const user = await this.fireauth.currentUser;

    if (event.dn200kemasid == '' || event.dn200kemasid == 'undefined') {
      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: 'Can not move to status bungkus. Make sure merchandise sudah dikemas!',
      });
    } else {
      // this.lulaId(event);

      this.transactionForm.patchValue({
        dn312alterby: user.uid,
        dn309statudt: firebase.firestore.Timestamp.fromDate(new Date()),
        dn310sstatus: 6,
      });

      this.service.updateStatus(this.transactionForm.value).then((res: any) => {
        this.isLoading = false;

        if (res) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Can not move merchandise to status bungkus!',
          });
        } else {
          this.messageService.add({
            key: 'success',
            severity: 'success',
            summary: 'Success Message',
            detail: 'Succesfully move merchandise to status bungkus',
          });

          this.removeModalTrans();
        }
      });
    }
  }

  async getTransaction() {
    this.isLoading = true;

    (await this.service.getTrans()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          this.readData = res.filter(
            (data: any) =>
              data.dn310sstatus == 5 || data.dn310sstatus == 6 || data.dn310sstatus == 7
          );

          // this.readData.forEach((value: any) => {
          //   if (value.dn308whatbox == 0) {
          //     this.whatBox = "Out";
          //   } else if (value.dn308whatbox == 1) {
          //     this.whatBox = "A";
          //   } else if (value.dn308whatbox == 2) {
          //     this.whatBox = "B";
          //   } else if (value.dn308whatbox == 3) {
          //     this.whatBox = "C";
          //   } else if (value.dn308whatbox == 4) {
          //     this.whatBox = "D";
          //   } 
          // });
        } else {
          this.readData = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach the server!',
        });
      }
    });
  }

  async updateTransaction() {
    this.isLoading = true;
    this.submitted = true;

    const user = await this.fireauth.currentUser;

    if (this.transactionForm.invalid) {
      this.isLoading = false;

      if (this.tf.dn302qantity.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Quantity can not empty!',
        });
      }

      if (this.tf.dn303dealprc.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Deal price can not empty!',
        });
      }

      if (this.tf.dn304paypric.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Payment price can not empty!',
        });
      }

      return;
    } else {
      this.submitted = false;
    }

    if (this.changeNewUser == true) {
      console.log('this.selectShipment.idCustomer ' + this.selectShipment.idCustomer);
      console.log('this.selectShipment.id ' + this.selectShipment.id);

      // create new kemas id in shipment
      firebase
        .firestore()
        .doc(`cl001members/${this.selectShipment.idCustomer}`)
        .collection('cl005shipmnt')
        .doc(this.selectShipment.id)
        .update({
          hi000custoid: this.selectIdTrans.dk000custoid,
          hi006fulname: this.selectIdTrans.dk006fulname,
          hi009namalen: this.selectIdTrans.dk006fulname,
        }).then(() => {
          this.messageService.add({
            key: 'success',
            severity: 'success',
            summary: 'Success Message',
            detail: 'Your transaction success change new buyer',
          });

          this.removeModalKemas();
        });
    }

    this.service.updateTrans(this.transactionForm.value).then((res: any) => {
      this.isLoading = false;

      if (res) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not update transaction!',
        });
      } else {
        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully update transaction',
        });

        this.removeModalTrans();
      }
    });
  }

  async deleteTransaction() {
    this.isLoading = true;

    this.service.deleteTrans(this.transactionForm.value).then((res: any) => {
      if (res) {
        console.log('Error delete data transaction');
      } else {
        console.log('Your transaction success delete');

        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully delete transaction',
        });

        this.removeModalTrans();
      }
    });
  }

  async getShipment() {
    this.isLoading = true;

    (await this.service.readShip()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          // Get all data address
          // console.log('this.idCustomer ' + JSON.stringify(res));

          this.readShipment = res.filter((data: any) => data.hi210statuss == 5 || 6 || 7);
        } else {
          this.readShipment = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach shipment from the server!',
        });
      }
    });
  }

  // changeKemas(event: any) {
  //   // console.log('change customer ' + JSON.stringify(event.idCustomer));
  //   // console.log('change id ' + JSON.stringify(event.id));
  //   this.idCustomer = event.idCustomer;
  //   this.idx = event.id;

  //   this.modalListAddress = true;

  //   this.getShipment();
  // }

  noKemas(event: any) {
    this.getShipmentCount();
    
    this.modalKemas = true;
    this.renderer.addClass(document.body, 'modal-open');
    console.log('event no kemas' + JSON.stringify(event));

    this.selectedTrans = event;

    this.idt = event.id;
    this.idGoods = event.idGood;
    this.idKemasTrans = event.dn000custoid;

    this.custoIdTrans = event.dn000custoid;
    this.fulnameTrans = event.dn006fulname;
    this.kemasIdTrans = event.dn200kemasid;

    // get current no kemas custo id from shipment
    const currKemas = this.readShipment.filter((val: any ) => val.hi200kemasid == event.dn200kemasid);
    console.log('currKemas ' + JSON.stringify(currKemas[0]));    
    this.currentKemas = currKemas[0];

     // get same id kemas in transaction
     const filterTrans = this.readData.filter((val: any ) => val.dn200kemasid == event.dn200kemasid);
     console.log('filterTrans length ' + JSON.stringify(filterTrans.length)); 
     this.readTransLength = filterTrans.length;

    // dont display current kemas id
    const getKemas = this.readShipment.filter((val: any ) => val.hi000custoid == event.dn000custoid && val.hi200kemasid != event.dn200kemasid);
    // filter same kemas id
    const filterShipment = getKemas.filter((val: any) => val.hi210statuss == 5 || val.hi210statuss == 6 || val.hi210statuss == 7 );
    // filter duplicate same kemas id
    let matches = new Set();
    const filterKemas = filterShipment.filter((elem: any) => {
      const { hi200kemasid } = elem;
      if (matches.has(hi200kemasid)) {
        return false;
      } else {
        matches.add(hi200kemasid);
        return true;
      }
    })

    // get all after filter kemas id and display
    this.filterKemas = filterKemas;

    // get all data shipment by kustomer id and status
    const getDataShipment = this.readShipment.filter((val: any ) => val.hi000custoid == event.dn000custoid && val.hi210statuss == event.dn310sstatus);
    const filterDataShipment = getDataShipment.filter((val: any) => val.hi210statuss == 5 || val.hi210statuss == 6 || val.hi210statuss == 7 );
    this.idShipment = filterDataShipment;
    console.log('getKemasShipment ID' + JSON.stringify(this.idShipment));

    this.transactionForm.patchValue({
      idt: event.id,
      idGoods: event.idGood,
      dn200kemasid: event.dn200kemasid,
    });

    // this.shipmentForm.patchValue({
    //   ids: this.idShipment?.id,
    //   idCustomer: this.idShipment?.idCustomer,
    //   hi200kemasid: event.dn200kemasid,
    // });
  }

  newKemasId(item: any, newId: any) {
    // this.getShipment();  
    
    console.log('this.ids new currentKemas ' + JSON.stringify(this.currentKemas?.id));
    console.log('this.idCustomer new currentKemas ' + JSON.stringify(this.currentKemas?.idCustomer));

    console.log('this.id barang ' + JSON.stringify(this.idt));
    console.log('this.idGoods ' + JSON.stringify(this.idGoods));

    this.checNewKemas = !this.checNewKemas;
    console.log('this.checNewKemas ' + JSON.stringify(this.checNewKemas));
    this.checKemas = false;
    
    this.selectKemas = this.checNewKemas ? item.target.value : '';
    console.log('this.selectKemas ' + JSON.stringify(this.selectKemas));

    // set value current kemas 0
    this.currentUser = "";

    console.log('this.select New Kemas Id ' + JSON.stringify(newId));
    console.log('this.currentKemas length ' + JSON.stringify(this.idShipment.length));

    this.transactionForm.patchValue({
      idt: this.idt,
      idGoods: this.idGoods,
      dn200kemasid: newId,
    });

    this.shipmentForm.patchValue({
      ids: this.currentKemas?.id,
      idCustomer: this.currentKemas?.idCustomer,
      hi200kemasid: newId,
    });

    // // get id kemas shipment same with no kemas transaction selected
    // const getKemasShipment = this.readShipment.filter((val: any ) => val.hi000custoid == this.idKemasTrans);
    // console.log('getKemasShipment' + JSON.stringify(getKemasShipment));
    
    // console.log('this.idt' + JSON.stringify(this.idt));
    // console.log('this.idGoods' + JSON.stringify(this.idGoods));
    // console.log('this.idShipment' + JSON.stringify(this.idShipment)); 
    // console.log('this.valueCount is customer' + JSON.stringify(this.valueCount));

    // this.transactionForm.patchValue({
    //   idt: this.idt,
    //   idGoods: this.idGoods,
    //   dn200kemasid: item.target.value,
    // });

    // this.shipmentForm.patchValue({
    //   ids: this.currentKemas.id,
    //   idCustomer: this.currentKemas.idCustomer,
    //   hi200kemasid: item.target.value,
    // });
   
  }

  kemasId(event: any, item: any) {
    // set value list kemas 0
    this.selectKemas = "";

    console.log('kemas data ' + JSON.stringify(item));
    this.currentUser = item.hi200kemasid;

    console.log('this.ids currentKemas' + JSON.stringify(this.currentKemas?.id));
    console.log('this.idCustomer currentKemas' + JSON.stringify(this.currentKemas?.idCustomer));

    console.log('this.id barang' + JSON.stringify(this.idt));
    console.log('this.idGoods' + JSON.stringify(this.idGoods));

    this.checKemas = !this.checKemas;
    console.log('this.checKemas ' + JSON.stringify(this.checKemas));
    this.checNewKemas = false;
    console.log('this.checNewKemas ' + JSON.stringify(this.checNewKemas));

    this.transactionForm.patchValue({
      idt: this.idt,
      idGoods: this.idGoods,
      dn200kemasid: item.hi200kemasid,
    });

    this.shipmentForm.patchValue({
      ids: this.currentKemas?.id,
      idCustomer: this.currentKemas?.idCustomer,
      hi200kemasid: item.hi200kemasid,
    });
  }

  updateKemas() {
    this.isLoading = true;
    this.submitted = true;  

    console.log('this.checKemas dn200kemasid ' + this.transactionForm.get('dn200kemasid').value);    

    this.service.updateKemasTrans(this.transactionForm.value).then((res: any) => {
      this.isLoading = false;
      this.removeModalKemas();
    });

    // update kemas id if button kemas selected
    if (this.checKemas === true) {
      this.service.updateKemasTrans(this.transactionForm.value).then((res: any) => {
        if (res) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Error update kemas id data transaction!',
          });
        } else {
          this.messageService.add({
            key: 'success',
            severity: 'success',
            summary: 'Success Message',
            detail: 'Your transaction success update kemas id',
          });

          setTimeout(() => {
            this.service.updateKemasShipment(this.shipmentForm.value).then((res: any) => {
              this.isLoading = false;
              this.removeModalKemas();

              if (res) {
                this.messageService.add({
                  key: 'error',
                  severity: 'error',
                  summary: 'Error Message',
                  detail: 'Error update kemas id data shipment!',
                });
              } else {
                this.messageService.add({
                  key: 'success',
                  severity: 'success',
                  summary: 'Success Message',
                  detail: 'Your shipment success update kemas id',
                });
              }
            });
          }, 0);
        }
      });
    }

    // set new kemas id if button new kemas selected
    if (this.checNewKemas === true) {

      // setTimeout(() => {
      //   this.service.updateKemasShipment(this.shipmentForm.value).then((res: any) => {
      //     this.isLoading = false;
      //     this.removeModalKemas();

      //     if (res) {
      //       this.messageService.add({
      //         key: 'error',
      //         severity: 'error',
      //         summary: 'Error Message',
      //         detail: 'Error update kemas id data shipment!',
      //       });
      //     } else {
      //       this.messageService.add({
      //         key: 'success',
      //         severity: 'success',
      //         summary: 'Success Message',
      //         detail: 'Your shipment success update kemas id',
      //       });
      //     }
      //   });
      // }, 0);
      // console.log('this.selectKemas ' + this.selectKemas);

      // increment value bn006bcdship
      const setId = firebase.firestore().collection('cl001members').doc('--idx--');
      const increment = firebase.firestore.FieldValue.increment(1);
      setId.update({ bn006bcdship: increment }).then(() => {
        // get value from document
        return this.firestore.collection('cl001members').doc('--idx--').get().subscribe(async (data: any) => {   
          
          // this.readDataShipment.forEach(async (rdc: any) => {
          //   const filterCustomer = rdc.filter((data: any) => data.dk000custoid == this.selectedTrans.dn000custoid);
          //   this.selectedCustomer = filterCustomer[0];
          //   console.log('this.selectedShipment ' + JSON.stringify(this.selectedCustomer));        
          // });

          this.readDataCustomer.forEach(async (rdc: any) => {
            const filterCustomer = rdc.filter((data: any) => data.dk000custoid == this.selectedTrans.dn000custoid);
            this.selectedCustomer = filterCustomer[0];
            console.log('this.selectedCustomer ' + JSON.stringify(this.selectedCustomer));        
          });

            if (data) {

              setTimeout(() => {
                if (this.selectedCustomer) {

                  setTimeout(() => {
                    console.log('button kemas is true');

                    const sliceId = ('00000000000' + (data.data().bn006bcdship)).slice(-12);
                    const idInc = this.calculateEan13(sliceId); // output: 000000000000;      
                    console.log('success get new id bn006bcdship ' + idInc);

                     // if data shipment same id length > 1 remove old one then set new one
                    // if (this.readShipmentLength > 2) {
                    //   firebase
                    //   .firestore()
                    //   .doc(`cl001members/${this.currentKemas?.idCustomer}`)
                    //   .collection('cl005shipmnt')
                    //   .doc(this.currentKemas?.id)
                    //   .delete().then(() => {
                    //     console.log('success get new id bn006bcdship ' + idInc);

                    //     // this.messageService.add({
                    //     //   key: 'success',
                    //     //   severity: 'success',
                    //     //   summary: 'Success Message',
                    //     //   detail: 'Your transaction in shipment success delete old kemas id',
                    //     // });
                    //   });
                    // }

                    // create new kemas id in shipment
                    firebase
                      .firestore()
                      .doc(`cl001members/${this.currentKemas?.idCustomer}`)
                      .collection('cl005shipmnt')
                      .doc()
                      .set({
                        hi200kemasid: idInc,
                        hi205viakuri: 0,
                        hi206resinum: "",
                        hi203onkiprc: 0,
                        hi204paypric: -999999999999,
                        hi205catatan: "",
                        hi212alterby: this.readAdmin,
                        hi209statudt: firebase.firestore.Timestamp.fromDate(new Date()),
                        hi210statuss: this.selectedTrans.dn310sstatus,
                        hi000custoid: this.selectedTrans.dn000custoid,
                        hi006fulname: this.selectedTrans.dn006fulname,
                        hi009namalen: this.selectedCustomer.dk006fulname,
                        hi010country: this.selectedCustomer.dk010country,
                        hi011postnum: this.selectedCustomer.dk011postnum,
                        hi012prefect: this.selectedCustomer.dk012prefect,
                        hi013citynam: this.selectedCustomer.dk013citynam,
                        hi014lainnya: this.selectedCustomer.dk014lainnya,
                        hi015telpnum: this.selectedCustomer.dk015telpnum,
                        hi016geopoin: this.selectedCustomer.dk016geopoin,
                      }).then(() => {

                        console.log('delete current old kemas shipment after create new one ' + this.currentKemas?.id);
                        // delete current old kemas shipment after create new one

                        if (this.readTransLength == 1 || this.idShipment.length > 1) {
                          firebase
                          .firestore()
                          .doc(`cl001members/${this.currentKemas?.idCustomer}`)
                          .collection('cl005shipmnt')
                          .doc(this.currentKemas?.id)
                          .delete();
                        }

                        this.messageService.add({
                          key: 'success',
                          severity: 'success',
                          summary: 'Success Message',
                          detail: 'Your transaction success create new kemas id',
                        });

                        this.removeModalKemas();
                      });

                    // update new kemas id in transaction
                    this.firestore.doc(`cl003merchan/${this.idGoods}`).collection('cl004belanja').doc(this.idt).update({
                      dn200kemasid: idInc,
                    });

                  }, 0);
                } else {
                  this.messageService.add({
                    key: 'error',
                    severity: 'error',
                    summary: 'Error Message',
                    detail: 'Your transaction error create new kemas id. Can not get customer. Try again',
                  });
                }
              }, 1000);
            }

        });
      });      
    } else {
      console.log('button kemas is falsessssss');    
    }
  }

  async updateCount(event: any) {
    console.log('kemas data ' + JSON.stringify(event));
    this.currentUser = event.hi200kemasid;

    const setId = firebase.firestore().collection('cl001members').doc('--idx--');
    const increment = firebase.firestore.FieldValue.increment(1);
    setId.update({ bn006bcdship: increment });
  }  

  async getShipmentCount() {
    this.isLoading = true;

    (await this.service.getOnceIdShip()).subscribe((res: any) => {
      this.isLoading = false;

      // console.log('getShipmentCount ' + JSON.stringify(res.bn006bcdship));

      if (res) {
        const status = res;
        if (status) {
          let total = 0;
          let count = total += res.bn006bcdship + 1;
          const sliceId = ('000000000000' + count).slice(-12); // output: 000000000000;
          const idInc = this.calculateEan13(sliceId); 
          this.countId = idInc;
        } else {
          this.countId = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach shipment from the server!',
        });
      }
    });
  }

  async getCustomer() {
    this.isLoading = true;

    (await this.service.readCustomer()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        // Get all data customer
        this.readDataCustomer = this.firestore
          .collection('cl001members', (ref) => ref.orderBy('dk000custoid'))
          .snapshotChanges()
          .pipe(
            map((actions) =>
              actions.map((a) => {
                const data = a.payload.doc.data() as any;
                const id = a.payload.doc.id;
                const meta = a.payload.doc.metadata;

                // console.log('idtrans ' + JSON.stringify(id));
                return { id, meta, ...data };
              })
            )
          );
      } else {
        console.log('Can not reach the server');
      }
    });
  }
}
