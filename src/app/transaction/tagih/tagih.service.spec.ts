import { TestBed } from '@angular/core/testing';

import { TagihService } from './tagih.service';

describe('TagihService', () => {
  let service: TagihService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TagihService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
