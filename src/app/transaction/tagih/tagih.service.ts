import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

const routes = {
  read: () => `/cl004belanja`,
  customer: () => '/cl001members',
};

export interface dataContext {
  // The quote's category: 'dev', 'explicit'...
  category: string;
}

export interface idCount {
  idCount: number;
}

export interface transactionContext {
  idx: any;
  idGoods: any;
  dn300transid: string;
  dn307expordt: string;
  dn308whatbox: number;
  dn302qantity: number;
  dn101bymaker: string;
  dn102bseries: string;
  dn103handler: string;
  dn104release: string;
  dn111fotobrg: string;
  dn107barcoid: string;
  dn000custoid: string;
  dn006fulname: string;
  dn200kemasid: string;
  dn303dealprc: number;
  dn304paypric: number;
  dn305catatan: string;
  dn312alterby: string;
  dn309statudt: string;
  dn310sstatus: number;
}

export interface memberContext {
  idCustomer: any;
  dk000custoid: string;
  dk001emaddre: string;
  dk006fulname: string;
  dk010country: number;
  dk011postnum: string;
  dk012prefect: number;
  dk013citynam: string;
  dk014lainnya: string;
  dk015telpnum: number;
  dk016geopoin: any;
  dk005catatan: string;
  dk007created: string;
}

export interface shipmentContext {
  idx: any;
  hi200kemasid: string;
  hi205viakuri: number;
  hi206resinum: string;
  hi203onkiprc: number;
  hi204paypric: number;
  hi205catatan: string;
  hi212alterby: string;
  hi209statudt: string,
  hi210statuss: number;
  hi000custoid: string;
  hi006fulname: string;
  hi009namalen: string;
  hi010country: number;
  hi011postnum: string;
  hi012prefect: number;
  hi013citynam: string;
  hi014lainnya: string;
  hi015telpnum: string;
  hi016geopoin: string;
}

@Injectable({
  providedIn: 'root',
})
export class TagihService {
  idCustom: any;
  value = new Subject<string>();

  constructor(private firestore: AngularFirestore, private fireauth: AngularFireAuth) {}

  calculateEan13(number: any) {
    let code = number;
    let sum: any = 0;

    // Calculate the checksum digit here.
    for (let i = code.length - 1; i >= 0; i--) {
      // This appears to be backwards but the 
      // EAN-13 checksum must be calculated
      if (i % 2) { // odd  
        sum += code[i] * 3;
      }
      else { // even
        sum += code[i] * 1;
      }
    }
    code = (10 - (sum % 10)) % 10;
    return number + code;
  }

  async readTrans() {
    return this.firestore.collectionGroup('cl004belanja').valueChanges() as Observable<transactionContext[]>;
  }

  async getTrans() {
    return this.firestore
      .collectionGroup(`cl004belanja`)
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as any;
            const id = a.payload.doc.id;
            const idGood = a.payload.doc.ref.parent.parent.id;
            const meta = a.payload.doc.metadata;

            return { id, meta, idGood, ...data };
          })
        )
      );
  }

  async updateTrans(context: transactionContext) {
    let user = await this.fireauth.currentUser;
    console.log('get id goods ' + context.idGoods);
    console.log('get id transaksi ' + context.idx);

    if (user) {
      this.firestore
        .doc(`cl003merchan/${context.idGoods}`)
        .collection('cl004belanja')
        .doc(context.idx)
        .update({
          // dn307expordt: context.dn307expordt,
          // dn308whatbox: context.dn308whatbox,
          dn302qantity: context.dn302qantity,
          // dn300transid: context.dn300transid,
          // dn106itemnam: [
          //   {
          //     dn101bymaker: context.dn101bymaker,
          //     dn102bseries: context.dn102bseries,
          //     dn103handler: context.dn103handler,
          //   }
          // ],
          // dn104release: context.dn104release,
          // dn105catatan: context.dn105catatan,
          dn000custoid: context.dn000custoid,
          dn006fulname: context.dn006fulname,
          dn200kemasid: context.dn200kemasid,
          dn303dealprc: context.dn303dealprc,
          dn304paypric: context.dn304paypric,
          dn305catatan: context.dn305catatan,
        });
    }
  } 

  async deleteTrans(context: transactionContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      this.firestore.doc(`cl003merchan/${context.idGoods}`).collection('cl004belanja').doc(context.idx).delete();
    }
  }

  getOnce() {
    return this.firestore.collection('cl001members').doc('--idx--').get();
  }

  async createShipment(context: shipmentContext) {
    const user = await this.fireauth.currentUser;

    // this.firestore
    //   .collectionGroup('cl005shipmnt')
    //   .valueChanges()
    //   .subscribe((data: any) => {
    //     if (data) {
    //       console.log('yes id' + JSON.stringify(data[0].bn006bcdship));
    //       const idInc = ('000000000000' + data[0].bn006bcdship).slice(-12); // output: 000000000000;
    //       this.idCustom = idInc;
    //     }
    //   });

    if (user) {
      this.getOnce().subscribe(async (data) => {
        // check id increment value
        const value = data.get('bn006bcdship');
        console.log('length data value ' + value);

        if (value == undefined) {
          // Create id increment 0
          const setId = firebase.firestore().collection('cl001members').doc('--idx--');
          const increment = firebase.firestore.FieldValue.increment(0);
          setId.set({ bn006bcdship: increment }, { merge: true });

          // Get id Custom
          this.getOnce()
            .pipe(debounceTime(0))
            .pipe(distinctUntilChanged())
            .subscribe((data: any) => {
              // Save value id increment in variable
              if (data) {
                const sliceId = ('00000000000' + (data.data().bn006bcdship)).slice(-12);
                const idInc = this.calculateEan13(sliceId); // output: 000000000000;      
                this.idCustom = idInc;
                console.log('success get id' + this.idCustom);

                firebase
                  .firestore()
                  .doc(`cl001members/${context.idx}`)
                  .collection('cl005shipmnt')
                  .doc()
                  .set({
                    hi200kemasid: this.idCustom,
                    hi205viakuri: context.hi205viakuri,
                    hi206resinum: context.hi206resinum,
                    hi203onkiprc: context.hi203onkiprc,
                    hi204paypric: context.hi204paypric,
                    hi205catatan: context.hi205catatan,
                    hi212alterby: context.hi212alterby,
                    hi209statudt: context.hi209statudt,
                    hi210statuss: context.hi210statuss,
                    hi000custoid: context.hi000custoid,
                    hi006fulname: context.hi006fulname,
                    hi009namalen: context.hi009namalen,
                    hi010country: context.hi010country,
                    hi011postnum: context.hi011postnum,
                    hi012prefect: context.hi012prefect,
                    hi013citynam: context.hi013citynam,
                    hi014lainnya: context.hi014lainnya,
                    hi015telpnum: context.hi015telpnum,
                    hi016geopoin: context.hi016geopoin,
                  });
              }
            });

        } else {
          // Create id increment > 0
          const setId = firebase.firestore().collection('cl001members').doc('--idx--');
          const increment = firebase.firestore.FieldValue.increment(1);
          setId.set({ bn006bcdship: increment }, { merge: true });

          // Get id Custom
          this.getOnce()
            .pipe(debounceTime(0))
            .pipe(distinctUntilChanged())
            .subscribe((data: any) => {
              // Save value id increment in variable
              if (data) {
                const sliceId = ('00000000000' + (data.data().bn006bcdship)).slice(-12);
                const idInc = this.calculateEan13(sliceId); // output: 000000000000;      
                this.idCustom = idInc;
                console.log('success get id' + this.idCustom);

                firebase
                  .firestore()
                  .doc(`cl001members/${context.idx}`)
                  .collection('cl005shipmnt')
                  .doc()
                  .set({
                    hi200kemasid: this.idCustom,
                    hi205viakuri: context.hi205viakuri,
                    hi206resinum: context.hi206resinum,
                    hi203onkiprc: context.hi203onkiprc,
                    hi204paypric: context.hi204paypric,
                    hi205catatan: context.hi205catatan,
                    hi212alterby: context.hi212alterby,
                    hi209statudt: context.hi209statudt,
                    hi210statuss: context.hi210statuss,
                    hi000custoid: context.hi000custoid,
                    hi006fulname: context.hi006fulname,
                    hi009namalen: context.hi009namalen,
                    hi010country: context.hi010country,
                    hi011postnum: context.hi011postnum,
                    hi012prefect: context.hi012prefect,
                    hi013citynam: context.hi013citynam,
                    hi014lainnya: context.hi014lainnya,
                    hi015telpnum: context.hi015telpnum,
                    hi016geopoin: context.hi016geopoin,
                  });
              }
            });
        }
      
      });      
    }
  }
  
  async updateStatus(context: transactionContext) {
    let user = await this.fireauth.currentUser;

    // Get id Custom
    this.getOnce()
      .pipe(debounceTime(0))
      .pipe(distinctUntilChanged())
      .subscribe((data: any) => {
        // Save value id increment in variable
        const sliceId = ('00000000000' + (data.data().bn006bcdship)).slice(-12); 
        const idInc = this.calculateEan13(sliceId); // output: 000000000000;      
        this.idCustom = idInc;
        console.log('success get id' + this.idCustom);

        this.firestore
        .doc(`cl003merchan/${context.idGoods}`)
        .collection('cl004belanja')
        .doc(context.idx)
        .update({
          dn200kemasid: this.idCustom,
          dn312alterby: context.dn312alterby,
          dn309statudt: context.dn309statudt,
          dn310sstatus: context.dn310sstatus,
        });
      });
  }

  async updateStatusKemas(context: transactionContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      this.firestore
        .doc(`cl003merchan/${context.idGoods}`)
        .collection('cl004belanja')
        .doc(context.idx)
        .update({
          dn200kemasid: context.dn200kemasid,
          dn312alterby: context.dn312alterby,
          dn309statudt: context.dn309statudt,
          dn310sstatus: context.dn310sstatus,
        });
    }
  }

  async createShipmentKemas(context: shipmentContext) {
    const user = await this.fireauth.currentUser;

    firebase
      .firestore()
      .doc(`cl001members/${context.idx}`)
      .collection('cl005shipmnt')
      .doc()
      .set({
        hi200kemasid: context.hi200kemasid,
        hi205viakuri: context.hi205viakuri,
        hi206resinum: context.hi206resinum,
        hi203onkiprc: context.hi203onkiprc,
        hi204paypric: context.hi204paypric,
        hi205catatan: context.hi205catatan,
        hi212alterby: context.hi212alterby,
        hi209statudt: context.hi209statudt,
        hi210statuss: context.hi210statuss,
        hi000custoid: context.hi000custoid,
        hi006fulname: context.hi006fulname,
        hi009namalen: context.hi009namalen,
        hi010country: context.hi010country,
        hi011postnum: context.hi011postnum,
        hi012prefect: context.hi012prefect,
        hi013citynam: context.hi013citynam,
        hi014lainnya: context.hi014lainnya,
        hi015telpnum: context.hi015telpnum,
        hi016geopoin: context.hi016geopoin,
        //hi207lastset: context.hi207lastset,
      });
  }

  async readCustomer() {
    return this.firestore.collection(routes.customer()).valueChanges() as Observable<memberContext[]>;
  }

  async readShipment() {
    return this.firestore.collectionGroup('cl005shipmnt').valueChanges() as Observable<shipmentContext[]>;
  }
}
