import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TagihService } from './tagih.service';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { StartService } from '../start/start.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-tagih',
  templateUrl: './tagih.component.html',
  styleUrls: ['./tagih.component.scss'],
  providers: [MessageService, DatePipe],
})
export class TagihComponent implements OnInit {
  isLoading = false;
  error: string | undefined;

  modalAdd: boolean = false;
  modalDetail: boolean = false;
  modalKirim: boolean = false;
  modalListAddress: boolean = false;
  modalPhoto: boolean = false;

  transactionForm!: FormGroup;
  shipmentForm!: FormGroup;
  idCustom: any | undefined;
  alterBy: any | undefined;

  idx: any | undefined;
  idGoods: any | undefined;
  readData: any | undefined;
  readDataCustomer: any | undefined;
  readDataShipment: any | undefined;
  dataLength: any | undefined;
  searchValue: string | undefined;

  selectedData: any;
  latestBox: any | undefined;
  selectDataCustomer: any | undefined;
  selectEvent: any | undefined;

  statusID: any | undefined;
  readAdmin: any | undefined;
  idCustomer: any | undefined;

  getIdCs: any | undefined;
  getIdCustomer: any | undefined;
  getCustomerIdShipment: any | undefined;
  getCustomerKemasShipment: any | undefined;

  selectedCustomer: any | undefined;
  selectedTrans: any | undefined;
  selectIdCustomer: any | undefined;
  selectKemas: any | undefined;
  selectFullname: any | undefined;
  whatBox: any | undefined;

  get tf() {
    return this.transactionForm.controls;
  }

  submitted = false;

  noImage: any = 'assets/no-image.jpg';
  pictureFile: any = 'assets/no-image.jpg';

  _selectedColumns: any[];
  cols: any[];

  constructor(
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private fireauth: AngularFireAuth,
    private firestore: AngularFirestore,
    private service: TagihService,
    private messageService: MessageService,
    private startService: StartService,
    private datePipe: DatePipe
  ) {
    this.Form();
  }

  getOnce() {
    return this.firestore.collectionGroup('cl005shipmnt').valueChanges() as any;
  }

  ngOnInit() {
    this.getTransaction();
    this.getCustomer();
    this.getAdmin();
    this.getShipment();

    // set item column on p-table
    this.cols = [
      // { field: 'dn304paypric', header: 'Payment Price' },
      // { field: 'dn111fotobrg', header: 'Photo' },
      { field: 'dn107barcoid', header: 'BarcID' },
      { field: 'dn103handler', header: 'Baran' },
      { field: 'dn101bymaker', header: 'Mker' },
      { field: 'dn102bseries', header: 'Seri' },
      { field: 'dn302qantity', header: 'Q' },
      { field: 'dn000custoid', header: 'CustID' },
      { field: 'dn006fulname', header: 'Pmbeli' },
      { field: 'dn303dealprc', header: 'Sisa' },
      { field: 'dn200kemasid', header: 'KmasID' },
      { field: 'dn308whatbox', header: 'K' },
      { field: 'dn307expordt', header: 'DteX' },
      { field: 'dn305catatan', header: 'CatT' },
    ];

    this._selectedColumns = this.cols;
  }

  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col: any) => val.includes(col));
  }

  addModal() {
    this.modalAdd = true;
    this.renderer.addClass(document.body, 'modal-open');

    this.transactionForm.reset();
  }

  noKemas(event: any) {
    console.log('event ' + JSON.stringify(event));
  }

  removeModalTrans() {
    this.modalAdd = false;
    this.modalDetail = false;
    this.renderer.removeClass(document.body, 'modal-open');
  }
  
  selectCustomer(event: any) {
    console.log('customer id ' + JSON.stringify(event.target.value));
    const selectID = event.target.value;

    this.readDataCustomer.forEach((ev: any) => {
      const customerSelect = ev.filter((data: any) => data.dk000custoid === selectID);
      console.log('event ' + JSON.stringify(customerSelect[0].dk006fulname));

      this.transactionForm.patchValue({
        dn000custoid: selectID,
        dn005catatan: customerSelect[0].dk005catatan,
        dn006fulname: customerSelect[0].dk006fulname,
      });
    });
  }

  private Form() {
    this.transactionForm = this.formBuilder.group({
      idx: [''],
      idGoods: [''],
      dn307expordt: [''],
      dn308whatbox: [''],
      dn302qantity: ['', [Validators.required]],
      dn300transid: [''],
      dn101bymaker: [''],
      dn102bseries: [''],
      dn103handler: [''],
      dn104release: [''],
      dn107barcoid: [''],
      dn111fotobrg: [''],
      dn000custoid: [''],
      dn006fulname: [''],
      dn200kemasid: [''],
      dn303dealprc: ['', [Validators.required]],
      dn304paypric: ['', [Validators.required]],
      dn305catatan: [''],
      dn312alterby: [''],
      dn309statudt: [''],
      dn310sstatus: [''],
    });

    this.shipmentForm = this.formBuilder.group({
      idx: [''],
      hi200kemasid: ['', [Validators.required]],
      hi205viakuri: [0],
      hi206resinum: [''],
      hi203onkiprc: [0],
      hi204paypric: [-999999999999],
      hi205catatan: [''],
      hi212alterby: [''],
      hi209statudt: [''],
      hi210statuss: [''],
      hi000custoid: [''],
      hi006fulname: [''],
      hi009namalen: [''],
      hi010country: [''],
      hi011postnum: [''],
      hi012prefect: [''],
      hi013citynam: [''],
      hi014lainnya: [''],
      hi015telpnum: [''],
      hi016geopoin: [''],
    });
  }

  closed(event: any) {
    console.log('event ' + event.target.className);
    if(event.target.className === "modal fade show") {
      this.modalDetail = false;
      this.modalPhoto = false;
      this.renderer.removeClass(document.body, 'modal-open');
    } 
  }

  lulaPhoto(event: any) {
    if(event.dn111fotobrg == "" || event.dn111fotobrg == null) {
      this.pictureFile = 'assets/no-image.jpg';
    } else {
      this.pictureFile = event.dn111fotobrg;
    }

    this.modalPhoto = true;
    this.renderer.addClass(document.body, 'modal-open');

  }

  onChangeWhatbox(event: any) {
    const latestBox = event.target.value;
    this.latestBox = parseInt(latestBox);
    console.log('this.latestBox ' + JSON.stringify(this.latestBox));

    this.transactionForm.patchValue({
      dn308whatbox: this.latestBox,
    });    
  }

  lulaId(event: any) {
    console.log('event ' + JSON.stringify(event));

    this.modalDetail = true;
    this.renderer.addClass(document.body, 'modal-open');

    const dateReleaseGoods = moment(new Date(event.dn104release?.seconds * 1000)).format('YYYY/MM/DD hh:mm:ss');
    const statusDate = moment(new Date(event.dn309statudt?.seconds * 1000)).format('MM/DD/YYYY hh:mm:ss A');  

    this.selectedTrans = event;

    // // format type input datetime-local
    // let exportDate: any;
    // if (event.dn307expordt == "") {
    //   exportDate = event.dn307expordt;
    // } else {
    //   exportDate = moment(new Date(event.dn307expordt?.seconds * 1000)).format('yyyy-MM-DDThh:mm');
    // }

    const exportDate = moment(new Date(event.dn307expordt?.seconds * 1000)).format('MM/DD/YYYY hh:mm:ss A');

    this.selectIdCustomer = event.id;
    this.idGoods = event.idGood;
    this.selectKemas = event.dn200kemasid;
    this.selectFullname = event.dn006fulname;
    this.idCustomer = event.dn000custoid;

    this.transactionForm.patchValue({
      idx: event.id,
      idGoods: event.idGood,
      dn000custoid: event.dn000custoid,
      dn006fulname: event.dn006fulname,
      dn307expordt: exportDate,
      dn308whatbox: event.dn308whatbox,
      dn302qantity: event.dn302qantity,
      dn200kemasid: event.dn200kemasid,
      dn103handler: event.dn103handler,
      dn107barcoid: event.dn107barcoid,
      dn101bymaker: event.dn101bymaker,
      dn102bseries: event.dn102bseries,
      dn300transid: event.dn300transid,
      dn104release: dateReleaseGoods,
      dn303dealprc: event.dn303dealprc,
      dn304paypric: event.dn304paypric,
      dn305catatan: event.dn305catatan,
      dn312alterby: event.dn312alterby,
      dn309statudt: statusDate,
      dn310sstatus: event.dn310sstatus,
      dn111fotobrg: event.dn111fotobrg
    });

    this.readDataCustomer.forEach((ev: any) => {
      const customerSelect = ev.filter((data: any) => data.dk000custoid === event.dn000custoid);

      this.selectedData = {
        id: customerSelect[0]?.id,
        meta: customerSelect[0].meta,
        dk000custoid: customerSelect[0].dk000custoid, 
        dk006fulname: customerSelect[0].dk006fulname, 
        dk001emaddre: customerSelect[0].dk001emaddre,
        dk005catatan: customerSelect[0].dk005catatan,
        dk007created: customerSelect[0].dk007created,
        dk010country: customerSelect[0].dk010country,
        dk011postnum: customerSelect[0].dk011postnum,
        dk012prefect: customerSelect[0].dk012prefect,
        dk013citynam: customerSelect[0].dk013citynam,
        dk014lainnya: customerSelect[0].dk014lainnya,
        dk015telpnum: customerSelect[0].dk015telpnum,
        dk016geopoin: customerSelect[0].dk016geopoin,
      };
    });

    this.getTransaction();
  }

  async userId(event: any, item: any) {
    this.selectedData = {
      dk000custoid: item.dn000custoid, 
      dk006fulname: item.dn006fulname, 
    };

    this.transactionForm.patchValue({
      dn000custoid: item.dk000custoid,
      dn006fulname: item.dk006fulname
    });
  }

  async moveStatus(event: any) {
    // console.log('event ' + JSON.stringify(event));

    const user = await this.fireauth.currentUser;

    const payment = event.dn304paypric;
    console.log('payment ' + JSON.stringify(payment));

    this.getCustomer();
    this.getShipment();

    if (payment == 0) {
      // fungsi promise janjian yang duluan pertama di eksekusi
      // function initValue() {
      //   return new Promise<void>((resolve, reject) => {
      //     console.log('initValue');
      //     resolve();
      //   });
      // }

      // function sendData() {
      //   console.log('sendData');
      // }

      // initValue().then(res => {
      //   sendData();
      // });
      
      this.readDataShipment.forEach(async (rds: any) => {
        const filterShipment = rds.filter((data: any) => data.hi000custoid == event.dn000custoid && data.hi210statuss == 5);
        this.getCustomerKemasShipment = filterShipment[0]?.hi200kemasid;
        this.getCustomerIdShipment = filterShipment[0]?.hi000custoid;
        //console.log('getCustomerIdShipment ' + JSON.stringify(this.getCustomerIdShipment));
        //console.log('getCustomerKemasShipment ' + this.getCustomerKemasShipment);
      });
    
      this.readDataCustomer.forEach(async (rdc: any) => {
        this.getIdCustomer = event.dn000custoid;
        //console.log('getIdCustomer ' + JSON.stringify(this.getIdCustomer));

        const filterCustomer = rdc.filter((data: any) => data.dk000custoid == event.dn000custoid);
        this.getIdCs = filterCustomer[0].id;
        //console.log('getIdCs ' + JSON.stringify(this.getIdCs));

        setTimeout(() => {
          const statusId = this.getIdCustomer == this.getCustomerIdShipment;
          //console.log('statusId ' + JSON.stringify(statusId));
  
          if (this.getIdCustomer == this.getCustomerIdShipment) {
            console.log('customer id in shipment ada. Samakan no kemas');
            console.log('filterCustomer[0] ' + JSON.stringify(filterCustomer[0]));
  
            setTimeout(() => {
              // create new field shipment lunas
              this.shipmentForm.patchValue({
                idx: this.getIdCs,
                hi200kemasid: this.getCustomerKemasShipment,
                hi205viakuri: 0,
                hi206resinum: "",
                hi203onkiprc: 0,
                hi204paypric: -999999999999,
                hi205catatan: "",
                hi212alterby: this.readAdmin,
                hi209statudt: firebase.firestore.Timestamp.fromDate(new Date()),
                hi210statuss: 5,
                hi000custoid: event.dn000custoid,
                hi006fulname: event.dn006fulname,
                hi009namalen: filterCustomer[0].dk006fulname,
                hi010country: filterCustomer[0].dk010country,
                hi011postnum: filterCustomer[0].dk011postnum,
                hi012prefect: filterCustomer[0].dk012prefect,
                hi013citynam: filterCustomer[0].dk013citynam,
                hi014lainnya: filterCustomer[0].dk014lainnya,
                hi015telpnum: filterCustomer[0].dk015telpnum,
                hi016geopoin: filterCustomer[0].dk016geopoin,
              });
  
              // set create new data status collection shipment
              this.service.createShipmentKemas(this.shipmentForm.value).then((res: any) => {
                this.isLoading = false;
  
                if (res) {
                  this.messageService.add({
                    key: 'error',
                    severity: 'error',
                    summary: 'Error Message',
                    detail: 'Can not move merchandise to shipment!',
                  });
                } else {
                  this.messageService.add({
                    key: 'success',
                    severity: 'success',
                    summary: 'Success Message',
                    detail: 'Succesfully move merchandise to shipment',
                  });
  
                  this.removeModalTrans();
                }
              });
            }, 0);
  
            setTimeout(() => {
              // this.lulaId(event);
              this.removeModalTrans();
              // set update status transaction tagih
              this.transactionForm.patchValue({
                idx: event.id,
                idGoods: event.idGood,
                dn200kemasid: this.getCustomerKemasShipment,
                dn312alterby: this.readAdmin,
                dn309statudt: firebase.firestore.Timestamp.fromDate(new Date()),
                dn310sstatus: 5,
              });
  
              this.service.updateStatusKemas(this.transactionForm.value).then((res: any) => {
                this.isLoading = false;
  
                if (res) {
                  this.messageService.add({
                    key: 'error',
                    severity: 'error',
                    summary: 'Error Message',
                    detail: 'Can not move merchandise to status lunas!',
                  });
                } else {
                  this.messageService.add({
                    key: 'success',
                    severity: 'success',
                    summary: 'Success Message',
                    detail: 'Succesfully move merchandise to status lunas',
                  });
  
                  this.removeModalTrans();
                }
              });
            }, 1000);
  
          } else {
            console.log('customer id in shipment tidak ada. Buat no kemas baru');
            setTimeout(() => {
              // create new field shipment lunas
              this.shipmentForm.patchValue({
                idx: this.getIdCs,
                hi200kemasid: event.dn200kemasid,
                hi205viakuri: 0,
                hi206resinum: "",
                hi203onkiprc: 0,
                hi204paypric: -999999999999,
                hi205catatan: "",
                hi212alterby: this.readAdmin,
                hi209statudt: firebase.firestore.Timestamp.fromDate(new Date()),
                hi210statuss: 5,
                hi000custoid: event.dn000custoid,
                hi006fulname: event.dn006fulname,
                hi009namalen: filterCustomer[0].dk006fulname,
                hi010country: filterCustomer[0].dk010country,
                hi011postnum: filterCustomer[0].dk011postnum,
                hi012prefect: filterCustomer[0].dk012prefect,
                hi013citynam: filterCustomer[0].dk013citynam,
                hi014lainnya: filterCustomer[0].dk014lainnya,
                hi015telpnum: filterCustomer[0].dk015telpnum,
                hi016geopoin: filterCustomer[0].dk016geopoin,
              });
  
              // set create new data status collection shipment
              this.service.createShipment(this.shipmentForm.value).then((res: any) => {
                this.isLoading = false;
  
                if (res) {
                  this.messageService.add({
                    key: 'error',
                    severity: 'error',
                    summary: 'Error Message',
                    detail: 'Can not move merchandise to shipment!',
                  });
                } else {
                  this.messageService.add({
                    key: 'success',
                    severity: 'success',
                    summary: 'Success Message',
                    detail: 'Succesfully move merchandise to shipment',
                  });
  
                  this.removeModalTrans();
                }
              });
            }, 100);
  
            setTimeout(() => {
              // this.lulaId(event);
              this.removeModalTrans();
              // set update status transaction tagih
              this.transactionForm.patchValue({
                idx: event.id,
                idGoods: event.idGood,
                dn200kemasid: event.dn200kemasid,
                dn312alterby: this.readAdmin,
                dn309statudt: firebase.firestore.Timestamp.fromDate(new Date()),
                dn310sstatus: 5,
              });
  
              this.service.updateStatus(this.transactionForm.value).then((res: any) => {
                this.isLoading = false;
  
                if (res) {
                  this.messageService.add({
                    key: 'error',
                    severity: 'error',
                    summary: 'Error Message',
                    detail: 'Can not move merchandise to status lunas!',
                  });
                } else {
                  this.messageService.add({
                    key: 'success',
                    severity: 'success',
                    summary: 'Success Message',
                    detail: 'Succesfully move merchandise to status lunas',
                  });
  
                  this.removeModalTrans();
                }
              });
            }, 1000);
  
          }
        }, 100);
      });

     

    } else {
      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: 'Can not move to status lunas and shipment. Make sure payment be Lunas! Input value 0',
      });
    }
  }

  async getAdmin() {
    (await this.startService.getAdmin()).subscribe((res: any) => {
      if (res) {
        const status = res;
        if (status) {
          this.readAdmin = res[0].di004admname;
        } else {
          this.readAdmin = [];
        }
      }
    });
  }

  async getTransaction() {
    this.isLoading = true;

    (await this.service.getTrans()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          this.readData = res.filter((data: any) => (data.dn310sstatus == 4 && data.dn000custoid != '0000000000086') && (data.dn310sstatus == 4 && data.dn000custoid != '0000000000093'));

          // this.readData.forEach((value: any) => {
          //   if (value.dn308whatbox == 0) {
          //     this.whatBox = "Out";
          //   } else if (value.dn308whatbox == 1) {
          //     this.whatBox = "A";
          //   } else if (value.dn308whatbox == 2) {
          //     this.whatBox = "B";
          //   } else if (value.dn308whatbox == 3) {
          //     this.whatBox = "C";
          //   } else if (value.dn308whatbox == 4) {
          //     this.whatBox = "D";
          //   } 
          // });
        } else {
          this.readData = [];
        }

        // // Get all data address
        // this.readData = this.firestore
        // .collectionGroup(`cl004belanja`)
        // .snapshotChanges()
        // .pipe(
        //   map((actions) =>
        //     actions.map((a) => {
        //       const data = a.payload.doc.data() as any;
        //       const id = a.payload.doc.id;
        //       const idgood = a.payload.doc.ref.parent.parent.id;
        //       const meta = a.payload.doc.metadata;

        //       this.idGoods = idgood;

        //       this.statusID = data.dn310sstatus;

        //       // console.log('idtrans ' + JSON.stringify(id));
        //       // console.log('idgood ' + JSON.stringify(idgood));
        //       this.dataLength = data;

        //       return { id, meta, ...data };
        //     })
        //   )
        // );
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach the server!',
        });
      }
    });
  }

  async updateTransaction() {
    this.isLoading = true;
    this.submitted = true;

    const user = await this.fireauth.currentUser;

    if (this.transactionForm.invalid) {
      this.isLoading = false;

      if (this.tf.dn302qantity.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Quantity can not empty!',
        });
      }

      if (this.tf.dn303dealprc.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Deal price can not empty!',
        });
      }

      if (this.tf.dn304paypric.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Payment price can not empty!',
        });
      }

      return;
    } else {
      this.submitted = false;
    }

    this.service.updateTrans(this.transactionForm.value).then((res: any) => {
      this.isLoading = false;

      if (res) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not update transaction!',
        });
      } else {
        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully update transaction',
        });

        this.removeModalTrans();
      }
    });

    let getPrice = this.transactionForm.controls['dn304paypric'].value;
    console.log('get price ' + JSON.stringify(getPrice)); 
    
    if (getPrice == 0) {
      this.readDataShipment.forEach(async (rds: any) => {
        const filterShipment = rds.filter((data: any) => data.hi000custoid == this.selectedTrans.dn000custoid && data.hi210statuss == 5);
        this.getCustomerKemasShipment = filterShipment[0]?.hi200kemasid;
        this.getCustomerIdShipment = filterShipment[0]?.hi000custoid;
      });  

      this.readDataCustomer.forEach(async (rdc: any) => {
        const filterCustomer = rdc.filter((data: any) => data.dk000custoid == this.selectedTrans.dn000custoid);
        this.getIdCs = filterCustomer[0]?.id;
        this.selectedCustomer = filterCustomer[0];
        //console.log('getIdCs ' + JSON.stringify(this.getIdCs));        
      });
    
      setTimeout(() => {
        if (this.selectedTrans.dn000custoid == this.getCustomerIdShipment) {
          console.log('customer id in shipment ada. Samakan no kemas');

          setTimeout(() => {
            if (this.selectedCustomer) {
              setTimeout(() => {
                // set update status transaction lunas
                this.transactionForm.patchValue({
                  idx: this.selectedTrans.id,
                  idGoods: this.idGoods,
                  dn200kemasid: this.getCustomerKemasShipment,
                  dn312alterby: this.readAdmin,
                  dn309statudt: firebase.firestore.Timestamp.fromDate(new Date()),
                  dn310sstatus: 5,
                });

                this.service.updateStatusKemas(this.transactionForm.value).then((res: any) => {
                  this.isLoading = false;

                  if (res) {
                    this.messageService.add({
                      key: 'error',
                      severity: 'error',
                      summary: 'Error Message',
                      detail: 'Can not move merchandise to status lunas!',
                    });
                  } else {
                    this.messageService.add({
                      key: 'success',
                      severity: 'success',
                      summary: 'Success Message',
                      detail: 'Succesfully move merchandise to status lunas',
                    });

                    this.removeModalTrans();
                  }
                });
              }, 1000);
            }
          }, 1000);
        } else {
          console.log('customer id in shipment tidak ada. Buat no kemas baru');

          setTimeout(() => {
            if (this.selectedCustomer) {
              // create new field shipment lunas
              this.shipmentForm.patchValue({
                idx: this.getIdCs,
                hi200kemasid: this.selectedTrans.dn200kemasid,
                hi205viakuri: 0,
                hi206resinum: "",
                hi203onkiprc: 0,
                hi204paypric: -999999999999,
                hi205catatan: "",
                hi212alterby: this.readAdmin,
                hi209statudt: firebase.firestore.Timestamp.fromDate(new Date()),
                hi210statuss: 5,
                hi000custoid: this.selectedTrans.dn000custoid,
                hi006fulname: this.selectedTrans.dn006fulname,
                hi009namalen: this.selectedCustomer.dk006fulname,
                hi010country: this.selectedCustomer.dk010country,
                hi011postnum: this.selectedCustomer.dk011postnum,
                hi012prefect: this.selectedCustomer.dk012prefect,
                hi013citynam: this.selectedCustomer.dk013citynam,
                hi014lainnya: this.selectedCustomer.dk014lainnya,
                hi015telpnum: this.selectedCustomer.dk015telpnum,
                hi016geopoin: this.selectedCustomer.dk016geopoin,
              });

              // set create new data status collection shipment
              this.service.createShipment(this.shipmentForm.value).then((res: any) => {
                this.isLoading = false;

                if (res) {
                  this.messageService.add({
                    key: 'error',
                    severity: 'error',
                    summary: 'Error Message',
                    detail: 'Can not create new kemas id to shipment!',
                  });
                } else {
                  this.messageService.add({
                    key: 'success',
                    severity: 'success',
                    summary: 'Success Message',
                    detail: 'Succesfully create new kemas id to shipment',
                  });

                  this.removeModalTrans();
                }
              });

              setTimeout(() => {
                // set update status transaction tagih
                this.transactionForm.patchValue({
                  idx: this.selectedTrans.id,
                  idGoods: this.idGoods,
                  dn200kemasid: this.selectedTrans.dn200kemasid,
                  dn312alterby: this.readAdmin,
                  dn309statudt: firebase.firestore.Timestamp.fromDate(new Date()),
                  dn310sstatus: 5,
                });

                this.service.updateStatus(this.transactionForm.value).then((res: any) => {
                  this.isLoading = false;

                  if (res) {
                    this.messageService.add({
                      key: 'error',
                      severity: 'error',
                      summary: 'Error Message',
                      detail: 'Can not move merchandise to status lunas!',
                    });
                  } else {
                    this.messageService.add({
                      key: 'success',
                      severity: 'success',
                      summary: 'Success Message',
                      detail: 'Succesfully move merchandise to status lunas',
                    });

                    this.removeModalTrans();
                  }
                });
              }, 1000);
            }
          }, 1000);

        }
      }, 1000);
    } else {
      setTimeout(() => {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not move to status lunas and shipment. Make sure payment be Lunas! Input value 0',
        });
      }, 2000)
    }
  }

  async updateTransactionMulti() {
    this.isLoading = true;
    this.submitted = true;

    const user = await this.fireauth.currentUser;

    if (this.transactionForm.invalid) {
      this.isLoading = false;

      if (this.tf.dn302qantity.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Quantity can not empty!',
        });
      }

      if (this.tf.dn303dealprc.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Deal price can not empty!',
        });
      }

      if (this.tf.dn304paypric.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Payment price can not empty!',
        });
      }

      return;
    } else {
      this.submitted = false;
    }

    this.service.updateTrans(this.transactionForm.value).then((res: any) => {
      this.isLoading = false;

      if (res) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not update transaction!',
        });
      } else {
        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully update transaction',
        });

        this.removeModalTrans();
      }
    });

    let getPrice = this.transactionForm.controls['dn304paypric'].value;
    console.log('get price ' + JSON.stringify(getPrice)); 
    
    if (getPrice == 0) {
      this.readDataShipment.forEach(async (rds: any) => {
        const filterShipment = rds.filter((data: any) => data.hi000custoid == this.idCustomer && data.hi210statuss == 5);
        this.getCustomerKemasShipment = filterShipment[0]?.hi200kemasid;
        this.getCustomerIdShipment = filterShipment[0]?.hi000custoid;
        //console.log('getCustomerIdShipment ' + JSON.stringify(this.getCustomerIdShipment));
        console.log('getCustomerKemasShipment ' + this.getCustomerKemasShipment);
      });
    
      this.readDataCustomer.forEach(async (rdc: any) => {
        this.getIdCustomer = this.idCustomer;
        //console.log('getIdCustomer ' + JSON.stringify(this.getIdCustomer));

        const filterCustomer = rdc.filter((data: any) => data.dk000custoid == this.idCustomer);
        this.getIdCs = filterCustomer[0].id;
        //console.log('getIdCs ' + JSON.stringify(this.getIdCs));

        setTimeout(() => {
          const statusId = this.getIdCustomer == this.getCustomerIdShipment;
          //console.log('statusId ' + JSON.stringify(statusId));
  
          if (this.getIdCustomer == this.getCustomerIdShipment) {
            console.log('customer id in shipment ada. Samakan no kemas');
            console.log('filterCustomer[0] ' + JSON.stringify(filterCustomer[0]));
  
            setTimeout(() => {
              // create new field shipment lunas
              this.shipmentForm.patchValue({
                idx: this.getIdCs,
                hi200kemasid: this.getCustomerKemasShipment,
                hi205viakuri: 0,
                hi206resinum: "",
                hi203onkiprc: 0,
                hi204paypric: -999999999999,
                hi205catatan: "",
                hi212alterby: this.readAdmin,
                hi209statudt: firebase.firestore.Timestamp.fromDate(new Date()),
                hi210statuss: 5,
                hi000custoid: this.idCustomer,
                hi006fulname: this.selectFullname,
                hi009namalen: filterCustomer[0].dk006fulname,
                hi010country: filterCustomer[0].dk010country,
                hi011postnum: filterCustomer[0].dk011postnum,
                hi012prefect: filterCustomer[0].dk012prefect,
                hi013citynam: filterCustomer[0].dk013citynam,
                hi014lainnya: filterCustomer[0].dk014lainnya,
                hi015telpnum: filterCustomer[0].dk015telpnum,
                hi016geopoin: filterCustomer[0].dk016geopoin,
              });
  
              // set create new data status collection shipment
              this.service.createShipmentKemas(this.shipmentForm.value).then((res: any) => {
                this.isLoading = false;
  
                if (res) {
                  this.messageService.add({
                    key: 'error',
                    severity: 'error',
                    summary: 'Error Message',
                    detail: 'Can not move merchandise to shipment!',
                  });
                } else {
                  this.messageService.add({
                    key: 'success',
                    severity: 'success',
                    summary: 'Success Message',
                    detail: 'Succesfully move merchandise to shipment',
                  });
  
                  this.removeModalTrans();
                }
              });
            }, 0);
  
            setTimeout(() => {
              // this.lulaId(event);
              this.removeModalTrans();
              // set update status transaction tagih
              this.transactionForm.patchValue({
                idx: this.selectIdCustomer,
                idGoods: this.idGoods,
                dn200kemasid: this.getCustomerKemasShipment,
                dn312alterby: this.readAdmin,
                dn309statudt: firebase.firestore.Timestamp.fromDate(new Date()),
                dn310sstatus: 5,
              });
  
              this.service.updateStatusKemas(this.transactionForm.value).then((res: any) => {
                this.isLoading = false;
  
                if (res) {
                  this.messageService.add({
                    key: 'error',
                    severity: 'error',
                    summary: 'Error Message',
                    detail: 'Can not move merchandise to status lunas!',
                  });
                } else {
                  this.messageService.add({
                    key: 'success',
                    severity: 'success',
                    summary: 'Success Message',
                    detail: 'Succesfully move merchandise to status lunas',
                  });
  
                  this.removeModalTrans();
                }
              });
            }, 1000);
  
          } else {
            console.log('customer id in shipment tidak ada. Buat no kemas baru');
            setTimeout(() => {
              // create new field shipment lunas
              this.shipmentForm.patchValue({
                idx: this.getIdCs,
                hi200kemasid: this.selectKemas,
                hi205viakuri: 0,
                hi206resinum: "",
                hi203onkiprc: 0,
                hi204paypric: -999999999999,
                hi205catatan: "",
                hi212alterby: this.readAdmin,
                hi209statudt: firebase.firestore.Timestamp.fromDate(new Date()),
                hi210statuss: 5,
                hi000custoid: this.idCustomer,
                hi006fulname: this.selectFullname,
                hi009namalen: filterCustomer[0].dk006fulname,
                hi010country: filterCustomer[0].dk010country,
                hi011postnum: filterCustomer[0].dk011postnum,
                hi012prefect: filterCustomer[0].dk012prefect,
                hi013citynam: filterCustomer[0].dk013citynam,
                hi014lainnya: filterCustomer[0].dk014lainnya,
                hi015telpnum: filterCustomer[0].dk015telpnum,
                hi016geopoin: filterCustomer[0].dk016geopoin,
              });
  
              // set create new data status collection shipment
              this.service.createShipment(this.shipmentForm.value).then((res: any) => {
                this.isLoading = false;
  
                if (res) {
                  this.messageService.add({
                    key: 'error',
                    severity: 'error',
                    summary: 'Error Message',
                    detail: 'Can not move merchandise to shipment!',
                  });
                } else {
                  this.messageService.add({
                    key: 'success',
                    severity: 'success',
                    summary: 'Success Message',
                    detail: 'Succesfully move merchandise to shipment',
                  });
  
                  this.removeModalTrans();
                }
              });
            }, 100);
  
            setTimeout(() => {
              // this.lulaId(event);
              this.removeModalTrans();
              // set update status transaction tagih
              this.transactionForm.patchValue({
                idx: this.selectIdCustomer,
                idGoods: this.idGoods,
                dn200kemasid: this.selectKemas,
                dn312alterby: this.readAdmin,
                dn309statudt: firebase.firestore.Timestamp.fromDate(new Date()),
                dn310sstatus: 5,
              });
  
              this.service.updateStatus(this.transactionForm.value).then((res: any) => {
                this.isLoading = false;
  
                if (res) {
                  this.messageService.add({
                    key: 'error',
                    severity: 'error',
                    summary: 'Error Message',
                    detail: 'Can not move merchandise to status lunas!',
                  });
                } else {
                  this.messageService.add({
                    key: 'success',
                    severity: 'success',
                    summary: 'Success Message',
                    detail: 'Succesfully move merchandise to status lunas',
                  });
  
                  this.removeModalTrans();
                }
              });
            }, 1000);
  
          }
        }, 100);
      });
    } else {
      setTimeout(() => {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not move to status lunas and shipment. Make sure payment be Lunas! Input value 0',
        });
      }, 2000)
    }
  }

  async deleteTransaction() {
    this.isLoading = true;

    this.service.deleteTrans(this.transactionForm.value).then((res: any) => {
      if (res) {
        console.log('Error delete data transaction');
      } else {
        console.log('Your transaction success delete');

        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully delete transaction',
        });

        this.removeModalTrans();
      }
    });
  }

  async getCustomer() {
    this.isLoading = true;

    (await this.service.readCustomer()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        // Get all data customer
        this.readDataCustomer = this.firestore
          .collection('cl001members', (ref) => ref.orderBy('dk000custoid'))
          .snapshotChanges()
          .pipe(
            map((actions) =>
              actions.map((a) => {
                const data = a.payload.doc.data() as any;
                const id = a.payload.doc.id;
                const meta = a.payload.doc.metadata;

                // console.log('idtrans ' + JSON.stringify(id));
                return { id, meta, ...data };
              })
            )
          );
      } else {
        console.log('Can not reach the server');
      }
    });
  }

  async getShipment() {
    this.isLoading = true;

    (await this.service.readShipment()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        // Get all data shipment
        this.readDataShipment = this.firestore
          .collectionGroup('cl005shipmnt', (ref) => ref.orderBy('hi000custoid'))
          .snapshotChanges()
          .pipe(
            map((actions) =>
              actions.map((a) => {
                const data = a.payload.doc.data() as any;
                const id = a.payload.doc.id;
                const meta = a.payload.doc.metadata;

                // console.log('idtrans ' + JSON.stringify(id));
                return { id, meta, ...data };
              })
            )
          );
      } else {
        console.log('Can not reach the server');
      }
    });
  }
}
