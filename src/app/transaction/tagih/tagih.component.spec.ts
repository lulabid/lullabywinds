import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagihComponent } from './tagih.component';

describe('TagihComponent', () => {
  let component: TagihComponent;
  let fixture: ComponentFixture<TagihComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TagihComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagihComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
