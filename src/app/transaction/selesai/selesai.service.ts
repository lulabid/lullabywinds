import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

const routes = {
  read: () => `/cl004belanja`,
  customer: () => '/cl001members',
};

export interface dataContext {
  // The quote's category: 'dev', 'explicit'...
  category: string;
}

export interface idCount {
  idCount: number;
}

export interface transactionContext {
  idx: any;
  idGoods: any;
  dn300transid: string;
  dn307expordt: string;
  dn308whatbox: number;
  dn302qantity: number;
  dn101bymaker: string;
  dn102bseries: string;
  dn103handler: string;
  dn104release: string;
  dn111fotobrg: string;
  dn107barcoid: string;
  dn000custoid: string;
  dn006fulname: string;
  dn200kemasid: string;
  dn303dealprc: number;
  dn304paypric: number;
  dn305catatan: string;
  dn312alterby: string;
  dn309statudt: string;
  dn310sstatus: number;
}

@Injectable({
  providedIn: 'root',
})
export class SelesaiService {
  idCustom: any;
  value = new Subject<string>();

  constructor(private firestore: AngularFirestore, private fireauth: AngularFireAuth) {}

  async readTrans() {
    return this.firestore.collectionGroup('cl004belanja').valueChanges() as Observable<transactionContext[]>;
  }

  async getTrans() {
    return this.firestore
      .collectionGroup(`cl004belanja`)
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as any;
            const id = a.payload.doc.id;
            const idGood = a.payload.doc.ref.parent.parent.id;
            const meta = a.payload.doc.metadata;

            return { id, meta, idGood, ...data };
          })
        )
      );
  }

  async updateTrans(context: transactionContext) {
    let user = await this.fireauth.currentUser;
    console.log('get id goods ' + context.idGoods);
    console.log('get id transaksi ' + context.idx);

    if (user) {
      this.firestore.doc(`cl003merchan/${context.idGoods}`).collection('cl004belanja').doc(context.idx).update({
        dn305catatan: context.dn305catatan,
      });
    }
  }

  async updateStatus(context: transactionContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      this.firestore
        .doc(`cl003merchan/${context.idGoods}`)
        .collection('cl004belanja')
        .doc(context.idx)
        .update({
          dn312alterby: context.dn312alterby,
          dn309statudt: context.dn309statudt,
          dn310sstatus: context.dn310sstatus,
        });
    }
  }

  async deleteTrans(context: transactionContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      this.firestore.collection(routes.read()).doc(context.idx).delete();
    }
  }
}
