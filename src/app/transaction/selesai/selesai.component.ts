import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelesaiService } from './selesai.service';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-selesai',
  templateUrl: './selesai.component.html',
  styleUrls: ['./selesai.component.scss'],
  providers: [MessageService, DatePipe],
})
export class SelesaiComponent implements OnInit {
  isLoading = false;
  error: string | undefined;

  modalAdd: boolean = false;
  modalDetail: boolean = false;
  modalKirim: boolean = false;
  modalListAddress: boolean = false;
  modalPhoto: boolean = false;

  transactionForm!: FormGroup;
  idCustom: any | undefined;
  alterBy: any | undefined;

  idx: any | undefined;
  idGoods: any | undefined;
  readData: any | undefined;
  dataLength: any | undefined;
  searchValue: string | undefined;

  statusID: any | undefined;
  whatBox: any | undefined;

  get tf() {
    return this.transactionForm.controls;
  }

  submitted = false;

  noImage: any = 'assets/no-image.jpg';
  pictureFile: any = 'assets/no-image.jpg';

  _selectedColumns: any[];
  cols: any[];

  constructor(
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private fireauth: AngularFireAuth,
    private firestore: AngularFirestore,
    private service: SelesaiService,
    private messageService: MessageService,
    private datePipe: DatePipe
  ) {
    this.Form();
  }

  ngOnInit() {
    this.getTransaction();

    // set item column on p-table
    this.cols = [
      // { field: 'dn304paypric', header: 'Payment Price' },
      // { field: 'dn111fotobrg', header: 'Photo' },
      { field: 'dn107barcoid', header: 'BarcID' },
      { field: 'dn103handler', header: 'Baran' },
      { field: 'dn101bymaker', header: 'Mker' },
      { field: 'dn102bseries', header: 'Seri' },
      { field: 'dn302qantity', header: 'Q' },
      { field: 'dn000custoid', header: 'CustID' },
      { field: 'dn006fulname', header: 'Pmbeli' },
      { field: 'dn303dealprc', header: 'Sisa' },
      { field: 'dn200kemasid', header: 'KmasID' },
      { field: 'dn308whatbox', header: 'K' },
      { field: 'dn307expordt', header: 'DteX' },
      { field: 'dn305catatan', header: 'CatT' },
    ];

    this._selectedColumns = this.cols;
  }

  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col: any) => val.includes(col));
  }

  addModal() {
    this.modalAdd = true;
    this.renderer.addClass(document.body, 'modal-open');

    this.transactionForm.reset();
  }

  noKemas(event: any) {
    console.log('event ' + JSON.stringify(event));
  }

  removeModalTrans() {
    this.modalAdd = false;
    this.modalDetail = false;
    this.renderer.removeClass(document.body, 'modal-open');
  }

  private Form() {
    this.transactionForm = this.formBuilder.group({
      idx: [''],
      idGoods: [''],
      dn307expordt: [''],
      dn308whatbox: [''],
      dn302qantity: ['', [Validators.required]],
      dn300transid: [''],
      dn101bymaker: [''],
      dn102bseries: [''],
      dn103handler: [''],
      dn104release: [''],
      dn107barcoid: [''],
      dn111fotobrg: [''],
      dn000custoid: [''],
      dn006fulname: [''],
      dn200kemasid: [''],
      dn303dealprc: ['', [Validators.required]],
      dn304paypric: ['', [Validators.required]],
      dn305catatan: [''],
      dn312alterby: [''],
      dn309statudt: [''],
      dn310sstatus: [''],
    });
  }

  closed(event: any) {
    console.log('event ' + event.target.className);
    if(event.target.className === "modal fade show") {
      this.modalDetail = false;
      this.modalPhoto = false;
      this.renderer.removeClass(document.body, 'modal-open');
    } 
  }

  lulaPhoto(event: any) {
    if(event.dn111fotobrg == "" || event.dn111fotobrg == null) {
      this.pictureFile = 'assets/no-image.jpg';
    } else {
      this.pictureFile = event.dn111fotobrg;
    }

    this.modalPhoto = true;
    this.renderer.addClass(document.body, 'modal-open');

  }

  lulaId(event: any) {
    console.log('event ' + JSON.stringify(event));

    // this.getTransaction();

    // console.log('get id transaksi ' + event.id );
    this.idx = event.id;

    this.modalDetail = true;
    this.renderer.addClass(document.body, 'modal-open');

    const dateReleaseGoods = moment(new Date(event.dn104release?.seconds * 1000)).format('YYYY/MM/DD hh:mm:ss');
    const statusDate = moment(new Date(event.dn309statudt?.seconds * 1000)).format('MM/DD/YYYY hh:mm:ss A');  

    const exportDate = moment(new Date(event.dn307expordt?.seconds * 1000)).format('MM/DD/YYYY hh:mm:ss A');

    this.transactionForm.patchValue({
      idx: event.id,
      idGoods: event.idGood,
      dn000custoid: event.dn000custoid,
      dn006fulname: event.dn006fulname,
      dn307expordt: exportDate,
      dn308whatbox: event.dn308whatbox,
      dn302qantity: event.dn302qantity,
      dn200kemasid: event.dn200kemasid,
      dn103handler: event.dn103handler,
      dn107barcoid: event.dn107barcoid,
      dn101bymaker: event.dn101bymaker,
      dn102bseries: event.dn102bseries,
      dn300transid: event.dn300transid,
      dn104release: dateReleaseGoods,
      dn303dealprc: event.dn303dealprc,
      dn304paypric: event.dn304paypric,
      dn305catatan: event.dn305catatan,
      dn312alterby: event.dn312alterby,
      dn309statudt: statusDate,
      dn310sstatus: event.dn310sstatus,
      dn111fotobrg: event.dn111fotobrg
    });
  }

  async moveStatus(event: any) {
    const user = await this.fireauth.currentUser;

    const statusCode = event.dn310sstatus === 8;
    console.log('result ' + statusCode);

    if (statusCode) {
      this.lulaId(event);

      this.transactionForm.patchValue({
        dn312alterby: user.uid,
        dn309statudt: firebase.firestore.Timestamp.fromDate(new Date()),
        dn310sstatus: 9,
      });

      this.service.updateStatus(this.transactionForm.value).then((res: any) => {
        this.isLoading = false;

        if (res) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Can not move merchandise to shipment lunas!',
          });
        } else {
          this.messageService.add({
            key: 'success',
            severity: 'success',
            summary: 'Success Message',
            detail: 'Succesfully move merchandise to shipment lunas',
          });

          this.removeModalTrans();
        }
      });
    } else {
      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: 'Can not move to shipment lunas. Make sure payment be Lunas!',
      });
    }
  }

  async getTransaction() {
    this.isLoading = true;

    (await this.service.getTrans()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          this.readData = res.filter((data: any) => data.dn310sstatus == 8);

          // this.readData.forEach((value: any) => {
          //   if (value.dn308whatbox == 0) {
          //     this.whatBox = "Out";
          //   } else if (value.dn308whatbox == 1) {
          //     this.whatBox = "A";
          //   } else if (value.dn308whatbox == 2) {
          //     this.whatBox = "B";
          //   } else if (value.dn308whatbox == 3) {
          //     this.whatBox = "C";
          //   } else if (value.dn308whatbox == 4) {
          //     this.whatBox = "D";
          //   } 
          // });
        } else {
          this.readData = [];
        }
      } else {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach the server!',
        });
      }
    });
  }

  async updateTransaction() {
    this.isLoading = true;
    this.submitted = true;

    const user = await this.fireauth.currentUser;

    if (this.transactionForm.invalid) {
      this.isLoading = false;

      if (this.tf.dn302qantity.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Quantity can not empty!',
        });
      }

      if (this.tf.dn303dealprc.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Deal price can not empty!',
        });
      }

      if (this.tf.dn304paypric.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Payment price can not empty!',
        });
      }

      return;
    } else {
      this.submitted = false;
    }

    this.service.updateTrans(this.transactionForm.value).then((res: any) => {
      this.isLoading = false;

      if (res) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not update transaction!',
        });
      } else {
        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully update transaction',
        });

        this.removeModalTrans();
      }
    });
  }

  async deleteTransaction() {
    this.isLoading = true;

    this.service.deleteTrans(this.transactionForm.value).then((res: any) => {
      if (res) {
        console.log('Error delete data transaction');
      } else {
        console.log('Your transaction success delete');

        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully delete transaction',
        });

        this.removeModalTrans();
      }
    });
  }
}
