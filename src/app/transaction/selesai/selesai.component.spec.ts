import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelesaiComponent } from './selesai.component';

describe('SelesaiComponent', () => {
  let component: SelesaiComponent;
  let fixture: ComponentFixture<SelesaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SelesaiComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelesaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
