import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

const routes = {
  read: () => `/cl004belanja`,
  customer: () => '/cl001members',
};

const credentialsKey = 'credentials';

export interface dataContext {
  // The quote's category: 'dev', 'explicit'...
  category: string;
}

export interface idCount {
  idCount: number;
}

export interface goodsContext {
  idx: any;
  dm100merchid: string;
  dm106itemnam: any;
  dm101bymaker: string;
  dm102bseries: string;
  dm103handler: string;
  dm104release: string;
  dm105catatan: string;
  dm111mystatu: any;
  dm112alterby: string;
  dm109created: string;
  dm110noaktif: number;
}

export interface transactionContext {
  idx: any;
  idGoods: any;
  dn300transid: string;
  dn307expordt: string;
  dn308whatbox: number;
  dn302qantity: number;
  dn101bymaker: string;
  dn102bseries: string;
  dn103handler: string;
  dn104release: string;
  dn111fotobrg: string;
  dn107barcoid: string;
  dn000custoid: string;
  dn006fulname: string;
  dn200kemasid: string;
  dn303dealprc: number;
  dn304paypric: number;
  dn305catatan: string;
  dn312alterby: string;
  dn309statudt: string;
  dn310sstatus: number;
}

export interface memberContext {
  idCustomer: any;
  dk000custoid: string;
  dk001emaddre: string;
  dk006fulname: string;
  dk010country: number;
  dk011postnum: string;
  dk012prefect: number;
  dk013citynam: string;
  dk014lainnya: string;
  dk015telpnum: number;
  dk016geopoin: any;
  dk005catatan: string;
  dk007created: string;
}

export interface adminContext {
  di000adminid: string;
  di001dmymail: string;
  di003authori: string;
  di004admname: string;
  di005catatan: string;
}

@Injectable({
  providedIn: 'root',
})
export class StartService {
  idCustom: any;
  value = new Subject<string>();
  email: any | undefined;

  constructor(private firestore: AngularFirestore, private fireauth: AngularFireAuth) {}

  async readTrans() {
    return this.firestore.collectionGroup('cl004belanja').valueChanges() as Observable<transactionContext[]>;
  }

  async getAdmin() {
    let user = await this.fireauth.currentUser;
    let userEmail = await JSON.parse(localStorage.getItem(credentialsKey));

    if (userEmail) {
      return this.firestore
      .collectionGroup(`cl000adminis`, (ref) => ref.where('di001dmymail', '==', userEmail.email))
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as any;
            const id = a.payload.doc.id;
            const meta = a.payload.doc.metadata;

            return { id, meta, ...data };
          })
        )
      ) as Observable<adminContext[]>;
    }
  }

  async getTrans() {
    return this.firestore
      .collectionGroup(`cl004belanja`)
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as any;
            const id = a.payload.doc.id;
            const idGood = a.payload.doc.ref.parent.parent.id;
            const meta = a.payload.doc.metadata;

            return { id, meta, idGood, ...data };
          })
        )
      ) as Observable<transactionContext[]>;
  }

  async updateTrans(context: transactionContext) {
    let user = await this.fireauth.currentUser;
    console.log('get id goods ' + context.idGoods);
    console.log('get id transaksi ' + context.idx);

    if (user) {
      this.firestore
        .doc(`cl003merchan/${context.idGoods}`)
        .collection('cl004belanja')
        .doc(context.idx)
        .update({
          // dn307expordt: context.dn307expordt,
          dn308whatbox: context.dn308whatbox,
          dn302qantity: context.dn302qantity,
          // dn300transid: context.dn300transid,
          // dn106itemnam: [
          //   {
          //     dn101bymaker: context.dn101bymaker,
          //     dn102bseries: context.dn102bseries,
          //     dn103handler: context.dn103handler,
          //   }
          // ],
          // dn104release: context.dn104release,
          // dn105catatan: context.dn105catatan,
          dn000custoid: context.dn000custoid,
          dn006fulname: context.dn006fulname,
          dn200kemasid: context.dn200kemasid,
          dn303dealprc: context.dn303dealprc,
          dn304paypric: context.dn304paypric,
          dn305catatan: context.dn305catatan,
        }).catch((error: any) => {
          console.log('data ' + error);
        });
    }
  }

  async updateStatus(context: transactionContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      this.firestore
        .doc(`cl003merchan/${context.idGoods}`)
        .collection('cl004belanja')
        .doc(context.idx)
        .update({
          dn312alterby: context.dn312alterby,
          dn309statudt: context.dn309statudt,
          dn310sstatus: context.dn310sstatus,
        });
    }
  }

  async deleteTrans(context: transactionContext) {
    let user = await this.fireauth.currentUser;

    if (user) {
      this.firestore.doc(`cl003merchan/${context.idGoods}`).collection('cl004belanja').doc(context.idx).delete();
    }
  }

  async readCustomer() {
    return this.firestore.collection(routes.customer()).valueChanges() as Observable<memberContext[]>;
  }
}
