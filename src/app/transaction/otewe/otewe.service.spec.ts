import { TestBed } from '@angular/core/testing';

import { OteweService } from './otewe.service';

describe('OteweService', () => {
  let service: OteweService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OteweService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
