import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetokComponent } from './setok.component';

describe('SetokComponent', () => {
  let component: SetokComponent;
  let fixture: ComponentFixture<SetokComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SetokComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetokComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
