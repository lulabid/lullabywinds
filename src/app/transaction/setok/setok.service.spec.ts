import { TestBed } from '@angular/core/testing';

import { SetokService } from './setok.service';

describe('SetokService', () => {
  let service: SetokService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SetokService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
