import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampahComponent } from './sampah.component';

describe('SampahComponent', () => {
  let component: SampahComponent;
  let fixture: ComponentFixture<SampahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SampahComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
