import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { Shell } from '@app/shell/shell.service';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'about',
      loadChildren: () => import('./about/about.module').then((m) => m.AboutModule),
    },
    {
      path: 'customer-address/:id',
      loadChildren: () => import('./pages/address/address.module').then((m) => m.AddressModule),
    },
    {
      path: 'goods-active',
      loadChildren: () => import('./pages/barang/barang.module').then((m) => m.BarangModule),
    },
    {
      path: 'merchant-photo/:id',
      loadChildren: () => import('./pages/list-photo/list-photo.module').then((m) => m.ListPhotoModule),
    },
    {
      path: 'transaction/:id',
      loadChildren: () =>
        import('./pages/detail-transaction/detail-transaction.module').then((m) => m.DetailTransactionModule),
    },
    {
      path: 'transaction-shipment/:id',
      loadChildren: () =>
        import('./pages/transaction-shipment/transaction-shipment.module').then((m) => m.TransactionShipmentModule),
    },
    {
      path: 'transaction-start',
      loadChildren: () => import('./transaction/start/start.module').then((m) => m.StartModule),
    },
    {
      path: 'transaction-box',
      loadChildren: () => import('./transaction/kotak/kotak.module').then((m) => m.KotakModule),
    },
    {
      path: 'transaction-otewe',
      loadChildren: () => import('./transaction/otewe/otewe.module').then((m) => m.OteweModule),
    },
    {
      path: 'transaction-bill',
      loadChildren: () => import('./transaction/tagih/tagih.module').then((m) => m.TagihModule),
    },
    {
      path: 'transaction-setok',
      loadChildren: () => import('./transaction/setok/setok.module').then((m) => m.SetokModule),
    },
    {
      path: 'transaction-paid',
      loadChildren: () => import('./transaction/lunas/lunas.module').then((m) => m.LunasModule),
    },
    {
      path: 'transaction-finish',
      loadChildren: () => import('./transaction/selesai/selesai.module').then((m) => m.SelesaiModule),
    },
    {
      path: 'transaction-trash',
      loadChildren: () => import('./transaction/sampah/sampah.module').then((m) => m.SampahModule),
    },
    {
      path: 'container-box',
      loadChildren: () => import('./kontainer/kotak/kotak.module').then((m) => m.KotakModule),
    },
    {
      path: 'container-otewe',
      loadChildren: () => import('./kontainer/otewe/otewe.module').then((m) => m.OteweModule),
    },
    {
      path: 'shipment-paid',
      loadChildren: () => import('./shipment/lunas/lunas.module').then((m) => m.LunasModule),
    },
    {
      path: 'shipment-packet',
      loadChildren: () => import('./shipment/bungkus/bungkus.module').then((m) => m.BungkusModule),
    },
    {
      path: 'shipment-send',
      loadChildren: () => import('./shipment/kirim/kirim.module').then((m) => m.KirimModule),
    },
    {
      path: 'shipment-finish',
      loadChildren: () => import('./shipment/selesai/selesai.module').then((m) => m.SelesaiModule),
    },
    {
      path: 'shipment-trash',
      loadChildren: () => import('./shipment/sampah/sampah.module').then((m) => m.SampahModule),
    },
  ]),

  {
    path: 'print-invoice/:id',
    loadChildren: () => import('./pages/print-invoice/print-invoice.module').then((m) => m.PrintInvoiceModule),
  },

  // Fallback when no prior route is matched
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}
