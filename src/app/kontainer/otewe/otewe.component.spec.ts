import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OteweComponent } from './otewe.component';

describe('OteweComponent', () => {
  let component: OteweComponent;
  let fixture: ComponentFixture<OteweComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OteweComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OteweComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
