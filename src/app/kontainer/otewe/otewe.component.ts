import { DatePipe } from '@angular/common';
import { Component, OnInit, Renderer2 } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OteweService } from '../../transaction/otewe/otewe.service';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-otewe',
  templateUrl: './otewe.component.html',
  styleUrls: ['./otewe.component.scss'],
  providers: [MessageService, DatePipe],
})
export class OteweComponent implements OnInit {
  isLoading = false;
  error: string | undefined;

  modalAdd: boolean = false;
  modalDetail: boolean = false;
  modalKirim: boolean = false;
  modalListAddress: boolean = false;

  transactionForm!: FormGroup;
  idCustom: any | undefined;
  alterBy: any | undefined;

  idx: any | undefined;
  idGoods: any | undefined;
  readData: any | undefined;
  dataLength: any | undefined;

  statusID: any | undefined;

  get tf() {
    return this.transactionForm.controls;
  }

  submitted = false;

  constructor(
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private fireauth: AngularFireAuth,
    private firestore: AngularFirestore,
    private service: OteweService,
    private messageService: MessageService,
    private datePipe: DatePipe
  ) {
    this.Form();
  }

  ngOnInit() {
    this.getTransaction();
  }

  addModal() {
    this.modalAdd = true;
    this.renderer.addClass(document.body, 'modal-open');

    this.transactionForm.reset();
  }

  noKemas(event: any) {
    console.log('event ' + JSON.stringify(event));
  }

  removeModalTrans() {
    this.modalAdd = false;
    this.modalDetail = false;
    this.renderer.removeClass(document.body, 'modal-open');
  }

  private Form() {
    this.transactionForm = this.formBuilder.group({
      idx: [''],
      idGoods: [''],
      dn307expordt: [''],
      dn308whatbox: [''],
      dn302qantity: ['', [Validators.required]],
      dn100merchid: [''],
      dn101bymaker: [''],
      dn102bseries: [''],
      dn103handler: [''],
      dn104release: [''],
      dn105catatan: [''],
      dn000custoid: [''],
      dn006fulname: [''],
      dn005catatan: [''],
      dn200nokirim: [''],
      dn303dealprc: ['', [Validators.required]],
      dn304paypric: ['', [Validators.required]],
      dn305catatan: [''],
      dn312alterby: [''],
      dn309statudt: [''],
      dn310sstatus: [''],
    });
  }

  lulaId(event: any) {
    console.log('event ' + JSON.stringify(event));

    // this.getTransaction();

    // console.log('get id transaksi ' + event.id );
    this.idx = event.id;

    this.modalDetail = true;
    this.renderer.addClass(document.body, 'modal-open');

    const dateReleaseGoods = moment(new Date(event.dn104release?.seconds * 1000)).format('YYYY/MM/DD hh:mm:ss');
    const dateStatusUpdate = moment(new Date(event.dn311mystatu[0].dn309statudt?.seconds * 1000)).format(
      'YYYY/MM/DD hh:mm:ss'
    );

    this.transactionForm.patchValue({
      idx: this.idx,
      idGoods: event.idGood,
      dn307expordt: event.dn307expordt,
      dn308whatbox: event.dn308whatbox,
      dn302qantity: event.dn302qantity,
      dn200nokirim: event.dn200nokirim,
      dn000custoid: event.dn000custoid,
      dn005catatan: event.dn005catatan,
      dn006fulname: event.dn006fulname,
      dn103handler: event.dn106itemnam[0].dn103handler,
      dn101bymaker: event.dn106itemnam[0].dn101bymaker,
      dn102bseries: event.dn106itemnam[0].dn102bseries,
      dn100merchid: event.dn100merchid,
      dn105catatan: event.dn105catatan,
      dn104release: dateReleaseGoods,
      dn303dealprc: event.dn301payment[0].dn303dealprc,
      dn304paypric: event.dn301payment[0].dn304paypric,
      dn305catatan: event.dn305catatan,
      dn312alterby: event.dn311mystatu[0].dn312alterby,
      dn309statudt: dateStatusUpdate,
      dn310sstatus: event.dn311mystatu[0].dn310sstatus,
    });
  }

  async moveStatus(event: any) {
    const user = await this.fireauth.currentUser;

    if (event) {
      this.lulaId(event);

      this.transactionForm.patchValue({
        dn312alterby: user.uid,
        dn309statudt: firebase.firestore.Timestamp.fromDate(new Date()),
        dn310sstatus: 4,
      });

      this.service.updateStatus(this.transactionForm.value).then((res: any) => {
        this.isLoading = false;

        if (res) {
          this.messageService.add({
            key: 'error',
            severity: 'error',
            summary: 'Error Message',
            detail: 'Can not move merchandise to setok!',
          });
        } else {
          this.messageService.add({
            key: 'success',
            severity: 'success',
            summary: 'Success Message',
            detail: 'Succesfully move merchandise to setok',
          });

          this.removeModalTrans();
        }
      });
    } else {
      this.messageService.add({
        key: 'error',
        severity: 'error',
        summary: 'Error Message',
        detail: 'Can not move to setok!',
      });
    }
  }

  async getTransaction() {
    this.isLoading = true;

    (await this.service.getTrans()).subscribe((res: any) => {
      this.isLoading = false;

      if (res) {
        const status = res;
        if (status) {
          this.readData = res.filter((data: any) => data.dn311mystatu[0].dn310sstatus == 3);
        } else {
          this.readData = [];
        }

        // // Get all data address
        // this.readData = this.firestore
        // .collectionGroup(`cl004belanja`)
        // .snapshotChanges()
        // .pipe(
        //   map((actions) =>
        //     actions.map((a) => {
        //       const data = a.payload.doc.data() as any;
        //       const id = a.payload.doc.id;
        //       const idgood = a.payload.doc.ref.parent.parent.id;
        //       const meta = a.payload.doc.metadata;

        //       this.idGoods = idgood;

        //       this.statusID = data.dn311mystatu[0].dn310sstatus;

        //       // console.log('idtrans ' + JSON.stringify(id));
        //       // console.log('idgood ' + JSON.stringify(idgood));
        //       this.dataLength = data;

        //       return { id, meta, ...data };
        //     })
        //   )
        // );
      } else {
        this.readData = [];

        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not reach the server!',
        });
      }
    });
  }

  async updateTransaction() {
    this.isLoading = true;
    this.submitted = true;

    const user = await this.fireauth.currentUser;

    if (this.transactionForm.invalid) {
      this.isLoading = false;

      if (this.tf.dn302qantity.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Quantity can not empty!',
        });
      }

      if (this.tf.dn303dealprc.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Deal price can not empty!',
        });
      }

      if (this.tf.dn304paypric.errors) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Payment price can not empty!',
        });
      }

      return;
    } else {
      this.submitted = false;
    }

    this.service.updateTrans(this.transactionForm.value).then((res: any) => {
      this.isLoading = false;

      if (res) {
        this.messageService.add({
          key: 'error',
          severity: 'error',
          summary: 'Error Message',
          detail: 'Can not update transaction!',
        });
      } else {
        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully update transaction',
        });

        this.removeModalTrans();
      }
    });
  }

  async deleteTransaction() {
    this.isLoading = true;

    this.service.deleteTrans(this.transactionForm.value).then((res: any) => {
      if (res) {
        console.log('Error delete data transaction');
      } else {
        console.log('Your transaction success delete');

        this.messageService.add({
          key: 'success',
          severity: 'success',
          summary: 'Success Message',
          detail: 'Succesfully delete transaction',
        });

        this.removeModalTrans();
      }
    });
  }
}
